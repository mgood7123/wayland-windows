#include <gtest/gtest.h>
#include <wl_posix.h>

TEST(fd_allocator, create) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 0);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, allocate) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(fd, 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 1);
    EXPECT_EQ(wl_syscalls__fd_allocator__get_value_from_fd(a, fd), nullptr);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 0);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, data) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd = wl_syscalls__fd_allocator__allocate_fd(a, (void*)5, nullptr);
    EXPECT_EQ(wl_syscalls__fd_allocator__get_value_from_fd(a, fd), (void*)5);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 0);
    wl_syscalls__fd_allocator__destroy(a);
}

void des1(size_t index, void** data, bool in_destructor) {
    **((int**)data) = 1;
};

TEST(fd_allocator, callback) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int foo = 0;

    int fd = wl_syscalls__fd_allocator__allocate_fd(a, &foo, des1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd);
    EXPECT_EQ(foo, 1);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, allocate_2) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd0 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd1 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(fd0, 0);
    EXPECT_EQ(fd1, 1);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 2);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd0);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd1);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 0);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, allocate_3) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd0 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd1 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(fd0, 0);
    EXPECT_EQ(fd1, 1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd1);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd0);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 0);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, allocate_and_reuse) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd0 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd1 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(fd0, 0);
    EXPECT_EQ(fd1, 1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd0);
    fd0 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(fd0, 0);
    EXPECT_EQ(fd1, 1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd1);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd0);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 0);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, reclaim) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int foo0 = 0;
    int foo1 = 0;

    int fd0 = wl_syscalls__fd_allocator__allocate_fd(a, &foo0, des1);
    int fd1 = wl_syscalls__fd_allocator__allocate_fd(a, &foo1, des1);
    wl_syscalls__fd_allocator__destroy(a);
    EXPECT_EQ(foo0, 1);
    EXPECT_EQ(foo1, 1);
}

TEST(fd_allocator, allocate_4) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd0 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 1);
    int fd1 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 2);
    int fd2 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 6);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 3);
    EXPECT_EQ(fd0, 0);
    EXPECT_EQ(fd1, 1);
    EXPECT_EQ(fd2, 2);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd2);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 2);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd1);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd0);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 0);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, allocate_5) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd0 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 1);
    int fd1 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 2);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 2);
    int fd2 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 6);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 3);
    EXPECT_EQ(fd0, 0);
    EXPECT_EQ(fd1, 1);
    EXPECT_EQ(fd2, 2);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd0);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 6);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 2);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd1);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 4);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd2);
    EXPECT_EQ(wl_syscalls__fd_allocator__capacity(a), 0);
    EXPECT_EQ(wl_syscalls__fd_allocator__size(a), 0);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, allocate_6) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd0 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd1 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd2 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd3 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd4 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd5 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd6 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd7 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd8 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd9 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd10 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, 11);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd10);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd10);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd9);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd9);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd8);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd8);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd7);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd7);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd6);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd6);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd5);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd5);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd4);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd4);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd3);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd3);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd2);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd2);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd1);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd0);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd0);
    wl_syscalls__fd_allocator__destroy(a);
}

TEST(fd_allocator, allocate_7) {
    wl_syscalls__fd_allocator * a = wl_syscalls__fd_allocator__create();
    int fd0 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd1 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd2 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd3 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd4 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd5 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd6 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd7 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd8 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd9 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    int fd10 = wl_syscalls__fd_allocator__allocate_fd(a, nullptr, nullptr);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd0);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd0);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd1);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd1);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd2);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd2);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd3);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd3);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd4);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd4);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd5);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd5);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd6);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd6);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd7);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd7);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd8);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd8);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd9);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd9);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, fd10);
    ShrinkingVectorIndexAllocator__index_is_valid(a->used, 11);
    wl_syscalls__fd_allocator__deallocate_fd(a, fd10);
    wl_syscalls__fd_allocator__destroy(a);
}
