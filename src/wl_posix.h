#ifndef WL_POSIX_H
#define WL_POSIX_H

#include "wl_syscalls.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32


struct wl_posix_fd_linked_list {
	void * buffer;
	size_t buffer_len;
	struct wl_posix_fd_linked_list * next;
};

typedef struct wl_posix_fd_linked_list wl_posix_fd_linked_list;

typedef struct wl_posix_transfer {
	
	bool complete;
	int iovec_current_count;
	int iovec_count;
	bool have_count;

	// send
	bool sent_iovec;
	bool sent_iov_count;
	bool sent_iov_base;
	bool sent_count;
	wl_posix_fd_linked_list * current;

	// recv
	bool recv_iovec;
	bool recv_iov_count;
	bool recv_iov_base;
	bool recv_count;
	int count;
	wl_miniobj * buf;
	wl_miniobj * buf_start;

} wl_posix_transfer;

typedef struct wl_posix_fd_struct {
	wl_posix_fd_linked_list * first;
	wl_posix_fd_linked_list * last;
	wl_posix_transfer* data;
	int count;
} wl_posix_fd_struct;

wl_posix_fd_struct * wl_posix_fd_transfer__prepare(void);
void wl_posix_fd_transfer__add(wl_posix_fd_struct* s, wl_miniobj * obj);
int wl_posix_fd_transfer__send(wl_posix_fd_struct* s, int socket_fd, struct iovec* iov, int iov_len, size_t CLEN);
int wl_posix_fd_transfer__recv(wl_posix_fd_struct* s, int socket_fd, struct iovec* iov, int iov_len, size_t CLEN);
void wl_posix_fd_transfer__delete(wl_posix_fd_struct* s);

#ifdef _WIN32

#define WL_SEND_DATA(socket, data, size) \
do { \
	len = wl_syscall__send(socket, (char*)data, size, MSG_NOSIGNAL | MSG_DONTWAIT); \
} while (len == SOCKET_ERROR && WSAGetLastError() == WSAEINTR); \
if (len == SOCKET_ERROR) { \
	return -1; \
}

#define WL_RECV_DATA(socket, data, size) \
do { \
	len = wl_syscall__recv(socket, (char*)data, size, MSG_DONTWAIT); \
} while (len == SOCKET_ERROR && WSAGetLastError() == WSAEINTR); \
if (len == SOCKET_ERROR) { \
	return -1; \
}

#else

#define WL_SEND_DATA(fd, data, size) \
do { \
	len = send(fd, (char*)data, size, MSG_NOSIGNAL | MSG_DONTWAIT); \
} while (len == -1 && errno == EINTR); \
if (len == -1) { \
	return -1; \
}

#define WL_RECV_DATA(fd, data, size) \
do { \
	len = recv(fd, (char*)data, size, MSG_DONTWAIT); \
} while (len == -1 && errno == EINTR); \
if (len == -1) { \
	return -1; \
}

#endif

void* wl_posix_dupfd_for_process(int fd, int peer, int minfd);

#define WL_POSIX_FD_HANDLE HANDLE
#define WL_POSIX_FD_INVALID_HANDLE_VALUE INVALID_HANDLE_VALUE
#define WL_POSIX_FD_INVALID_SOCKET_VALUE INVALID_SOCKET

LP64__LONG random(void);

char * strndup (const char *s, size_t n);

#include <time.h>
#include <errno.h>

typedef enum clockid_t {
	CLOCK_MONOTONIC,
	CLOCK_REALTIME
} clockid_t;

int gettimeofday(struct timeval_64_t *spec);
int clock_gettime(clockid_t type, struct timespec_N_bit *tp);

#else
#define WL_POSIX_FD_HANDLE int
#define WL_POSIX_FD_INVALID_HANDLE_VALUE -1
#define WL_POSIX_FD_INVALID_SOCKET_HANDLE -1
#define timespec64bit timespec
#endif

#define WL_POSIX_FD_VERSION 1

/*
API spec

linux specifies the file object as

https://www.kernel.org/doc/html/latest/filesystems/vfs.html?highlight=file+descriptor+table#id2

the int fd is simply an index to a file struct inside an fd table

https://github.com/torvalds/linux/blob/c2bf05db6c78f53ca5cd4b48f3b9b71f78d215f1/include/linux/fdtable.h#L27

struct fdtable {
	unsigned int max_fds;
	struct file __rcu **fd;      // current fd array
	unsigned long *close_on_exec;
	unsigned long *open_fds;
	unsigned long *full_fds_bits;
	struct rcu_head rcu;
};

the file struct is defined here

https://github.com/torvalds/linux/blob/c2bf05db6c78f53ca5cd4b48f3b9b71f78d215f1/include/linux/fs.h#L940

and it is large

*/

// wl_event_loop_add_fd will duplicate it's passed descriptor
// 
// the problem with this is we need to implement our own
// eventfd object since since windows does not have equivilant
// behaviour
//
// this immediately breaks the descriptor duping since
// the eventfd object is NOT a windows kernel object
//

#define wl_posix_type_socket 1
#define wl_posix_type_fd 2
#define wl_posix_type_eventfd 3
#define wl_posix_type_temporary_file 4
#define wl_posix_type_epoll 5
#define wl_posix_type_timerfd 6

#if _WIN32
SOCKET
#else
int
#endif
wl_posix_get_socket(int fd);

#if _WIN32
HANDLE
#else
int
#endif
wl_posix_get_fd(int fd);

#if _WIN32
HANDLE
#else
int
#endif
wl_posix_get_epoll(int fd);

#if _WIN32
HANDLE
#else
int
#endif
wl_posix_get_eventfd(int fd);

#if _WIN32
HANDLE
#else
int
#endif
wl_posix_get_temporary_file(int fd);

#if _WIN32
HANDLE
#else
int
#endif
wl_posix_get_timerfd(int fd);

// eventfd and timerfd

#ifdef _WIN32

#define TFD_NONBLOCK 0x1
#define TFD_CLOEXEC 0x2
#define TFD_TIMER_ABSTIME 0x1
#define TFD_TIMER_CANCEL_ON_SET 0x2

#define EFD_CLOEXEC 0x1
#define EFD_NONBLOCK 0x2
#define EFD_SEMAPHORE 0x4

// epoll

enum EPOLL_EVENTS {
  EPOLLIN      = (int) (1U <<  0),
  EPOLLPRI     = (int) (1U <<  1),
  EPOLLOUT     = (int) (1U <<  2),
  EPOLLERR     = (int) (1U <<  3),
  EPOLLHUP     = (int) (1U <<  4),
  EPOLLRDNORM  = (int) (1U <<  6),
  EPOLLRDBAND  = (int) (1U <<  7),
  EPOLLWRNORM  = (int) (1U <<  8),
  EPOLLWRBAND  = (int) (1U <<  9),
  EPOLLMSG     = (int) (1U << 10), /* Never reported. */
  EPOLLRDHUP   = (int) (1U << 13),
  EPOLLONESHOT = (int) (1U << 31)
};

#define EPOLLIN      (1U <<  0)
#define EPOLLPRI     (1U <<  1)
#define EPOLLOUT     (1U <<  2)
#define EPOLLERR     (1U <<  3)
#define EPOLLHUP     (1U <<  4)
#define EPOLLRDNORM  (1U <<  6)
#define EPOLLRDBAND  (1U <<  7)
#define EPOLLWRNORM  (1U <<  8)
#define EPOLLWRBAND  (1U <<  9)
#define EPOLLMSG     (1U << 10)
#define EPOLLRDHUP   (1U << 13)
#define EPOLLONESHOT (1U << 31)

#define EPOLL_CTL_ADD 1
#define EPOLL_CTL_MOD 2
#define EPOLL_CTL_DEL 3

#define EPOLL_CLOEXEC 0x1

#endif

/*
 * creates a file descriptor object, the fd must be allocated manually by setting returned_value->data
 *
 * this can be any valid unix file descriptor (int), or windows handle (HANDLE), any other type is UB
*/
int wl_posix_create_fd(void);

int wl_posix_create_socket(int domain, int type, int protocol);

int wl_posix_socketpair(int domain, int type, int protocol, int sv[2]);

int wl_posix_create_eventfd(unsigned int initval, int flags);

int wl_posix_create_epoll(int flags);

int wl_posix_create_mkstemp(char *template_);

int wl_posix_create_timerfd(int clockid, int flags);


void wl_posix_register_syscalls(void);


#ifdef _WIN32

// https://github.com/tronkko/dirent/blob/master/include/dirent.h
// TODO: verify
#include "win32-dirent.h"

// https://github.com/blackav/ejudge/blob/master/win32/open_memstream.c
// TODO: actually implement correctly, this just creates a disk file thus incurs disk-IO
FILE * open_memstream(char **ptr, size_t *sizeloc);
void close_memstream(FILE *f);

#endif



// https://github.com/dsprenkels/randombytes/blob/master/randombytes.h
// we trust this implementation

#ifdef _WIN32
/* Load size_t on windows */
#include <crtdefs.h>
#else

// In the case that are compiling on linux, we need to define _GNU_SOURCE
// *before* randombytes.h is included. Otherwise SYS_getrandom will not be
// declared.
#if defined(__linux__) || defined(__GNU__)
# define _GNU_SOURCE
#endif /* defined(__linux__) || defined(__GNU__) */

#include <unistd.h>
#endif /* _WIN32 */

/*
 * Write `n` bytes of high quality random bytes to `buf`
 */
int randombytes(void *buf, size_t n);

#ifdef __cplusplus
}
#endif

#endif