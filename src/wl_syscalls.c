#include "wl_syscalls.h"

int8_t FLAG_IS_BEING_DESTROYED = 0x1;
int8_t FLAG_FD_IS_BEING_DESTROYED = 0x2;
int8_t FLAG_UNDERLYING_DATA_IS_REFERENCE_COUNTED = 0x4;
int8_t FLAG_FD_IS_BEING_DESTROYED_MANUALLY = 0x8;

/*
 * A mini object, used to implement a
 * kernel-like api for opaque objects
 *
 * private implementation
 */
typedef struct wl_miniobj_priv {
    wl_miniobj public_obj;
    uint16_t references;
    int8_t  flags;
} wl_miniobj_priv;

typedef struct wl_miniobj_registry_t {
    uint8_t registered; // true if taken, false if free
    uint16_t references; // reference count all allocated objects, it is illegal to unregister if reference is not 0
    uint16_t count;
    uint16_t * accepted_syscalls;
    WL_MINIOBJ_CALLBACK * callbacks;
} wl_miniobj_registry_t;

// this computes to 1 MB on a 64 bit system
// this is enough to hold the maximum number of objects that we can contain

// for performance, we avoid allocating an wl_miniobj_registry_t object and instead directly use our allocated memory

// to maximize performance, the registered type shall directly correspond to an array index in the registry

wl_miniobj_registry_t * wl_miniobj__registry = NULL;
uint16_t wl_miniobj__registry_count = 0; // we still need this to know how many objects there are

wl_syscalls__fd_allocator * wl_miniobj_fd_list = NULL;

bool wl_miniobj_log_allocation = false;

uint64_t wl_miniobj_total_allocations = 0;
uint64_t wl_miniobj_total_deallocations = 0;

uint64_t wl_miniobj__total_allocations(void) { return wl_miniobj_total_allocations; }
uint64_t wl_miniobj__total_deallocations(void) { return wl_miniobj_total_deallocations; }

static void wl_miniobj_init(void) {
    if (wl_miniobj__registry == NULL) {
        wl_miniobj__registry = (wl_miniobj_registry_t*) calloc(65535, sizeof(wl_miniobj_registry_t)); // zero out every field
        if (wl_miniobj__registry == NULL) {
            printf("WL MINIOBJ ERROR:       FAILED TO INITIALIZE REGISTRY\n");
        }
        wl_miniobj__registry_count = 0;
        wl_miniobj_total_allocations = 0;
        wl_miniobj_total_deallocations = 0;
        wl_miniobj_fd_list = wl_syscalls__fd_allocator__create();
        if (wl_miniobj_fd_list == NULL) {
            printf("WL MINIOBJ ERROR:       FAILED TO INITIALIZE FD ALLOCATOR\n");
            free(wl_miniobj__registry);
            wl_miniobj__registry = NULL;
        }
    }
}

void wl_miniobj_deinit(void) {
    if (wl_miniobj__registry != NULL) {
        size_t objcount = wl_syscalls__fd_allocator__size(wl_miniobj_fd_list);
        if (objcount != 0) {
            printf("WL MINIOBJ WARNING: there are %zu allocated objects still present, they will be destroyed\n", objcount);
        }
        wl_syscalls__fd_allocator__destroy(wl_miniobj_fd_list);
        for (uint16_t i = 0; i < 65535; i++) {
            if (wl_miniobj__registry[i].registered) {
                wl_miniobj_unreg(i);
            }
        }
        wl_miniobj__registry_count = 0;
        free(wl_miniobj__registry);
        wl_miniobj__registry = NULL;
    }
}

static wl_miniobj_priv *  wl_miniobj_get_priv(int fd) {
    if (fd == -1) {
        printf("WL MINIOBJ ERROR: fd is -1\n");
        return NULL;
    }
    if (!wl_syscalls__fd_allocator__fd_is_valid(wl_miniobj_fd_list, fd)) {
        printf("WL MINIOBJ ERROR: fd (%d) is invalid \n", fd);
        return NULL;
    }
    return (wl_miniobj_priv *) wl_syscalls__fd_allocator__get_value_from_fd(wl_miniobj_fd_list, fd);
}

wl_miniobj *  wl_miniobj_get(int fd) {
    wl_miniobj_priv * obj_ = wl_miniobj_get_priv(fd);
    if (obj_ != NULL) {
        if (WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_IS_BEING_DESTROYED)) {
            if (wl_miniobj_debug) printf("obj is being destroyed\n");
            return NULL;
        }
    }
    return (wl_miniobj*) obj_;
}

bool wl_miniobj_is_being_destroyed(int fd) {
    wl_miniobj_priv * obj_ = wl_miniobj_get_priv(fd);
    if (obj_ != NULL) {
        if (WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_IS_BEING_DESTROYED)) {
            return true;
        }
    }
    return false;
}

static void WL_MINIOBJ_DESTROY_CALLBACK(size_t fd, void** unused, bool in_destructor) {
    if (in_destructor) {
        wl_miniobj_priv * obj_ = wl_miniobj_get_priv(fd);
        if (obj_ != NULL) {
            WL_MINIOBJ_ADD_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED);
        }
    }
    wl_syscall__close(fd);
    *unused = NULL;
}

int wl_miniobj_create(uint16_t version, uint16_t type, bool underlying_data_is_reference_counted) {
    wl_miniobj_init();
    if (!wl_miniobj__registry[type].registered) {
        printf("WL MINIOBJ ERROR: attempted to create an unregistered object of type: %u\n", type);
        return -1;
    }
    if (wl_miniobj__registry[type].references == 65535) {
        printf("WL MINIOBJ ERROR: maximum number of objects of type %u have been created, please destroy an object of same type in order to make room for another object of same type to be created\n", type);
        return -1;
    }
    wl_miniobj * obj = (wl_miniobj*) malloc(sizeof(wl_miniobj_priv));
    if (obj == NULL) {
        return -1;
    }
    wl_miniobj_total_allocations++;
    obj->version = version;
    obj->type = type;
    obj->data = 0;
    wl_miniobj_priv * obj_ = (wl_miniobj_priv *) obj;
    obj_->flags = 0;
    WL_MINIOBJ_SET_FLAG(obj_->flags, FLAG_UNDERLYING_DATA_IS_REFERENCE_COUNTED, underlying_data_is_reference_counted);
    obj_->references = 1;
    wl_miniobj__registry[type].references++;

    if (wl_miniobj_log_allocation) printf("WL MINIOBJ WARNING: allocate (malloc) object: %p, with type: %u\n", obj, obj->type);
    
    return (int) wl_syscalls__fd_allocator__allocate_fd(wl_miniobj_fd_list, obj, WL_MINIOBJ_DESTROY_CALLBACK);
}

int wl_miniobj_duplicate(int fd) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    wl_miniobj_priv * obj_ = (wl_miniobj_priv *) obj;
    if (obj == NULL) {
        return -1;
    }
    if (!WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_UNDERLYING_DATA_IS_REFERENCE_COUNTED)) {
        // if the underlying data IS NOT reference counted
        // we must return the same object
        // this object itself is reference counted
        obj_->references++;
        return (int) wl_syscalls__fd_allocator__allocate_fd(wl_miniobj_fd_list, obj, WL_MINIOBJ_DESTROY_CALLBACK);
    } else {
        // if the underlying data IS reference counted
        // we must construct a new object
        // each duplicate holds its own data, however it is copied for convinience (new obj data = obj data)
        // this object itself is not reference counted
        int new_fd = wl_miniobj_create(obj->version, obj->type, WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_UNDERLYING_DATA_IS_REFERENCE_COUNTED));
        wl_miniobj * new_obj = wl_miniobj_get(new_fd);
        new_obj->data = obj->data;
        return new_fd;
    }
}

bool wl_miniobj_should_destroy(int fd) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return false;
    }
    if (!wl_miniobj__registry[obj->type].registered) {
        return false;
    }
    if (wl_miniobj__registry[obj->type].references == 0) {
        return false;
    }
    wl_miniobj_priv * obj_ = (wl_miniobj_priv *) obj;
    // each reference has its own fd
    uint16_t ref = obj_->references;
    if (WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED)) {
        ref--;
    }
    if (!WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED) && !WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED_MANUALLY)) {
        ref--;
    }
    if (ref == 0) {
        return true;
    }
    return false;
}

void wl_miniobj_destroy(int fd, wl_miniobj * obj) {
    if (fd == -1 && obj == NULL) {
        printf("WL MINIOBJ ERROR: cannot destroy NULL object\n");
        return;
    }
    if (fd != -1 && obj == NULL) {
        obj = wl_miniobj_get(fd);
        if (obj != NULL) {
            wl_miniobj_destroy(fd, obj);
        }
        return;
    }
    if (fd == -1 && obj != NULL) {
        printf("WL MINIOBJ ERROR: an fd is required to destroy object: %u\n", obj->type);
        return;
    }
    if (!wl_miniobj__registry[obj->type].registered) {
        printf("WL MINIOBJ ERROR: cannot destroy an unregistered object of type: %u\n", obj->type);
        return;
    }
    if (wl_miniobj__registry[obj->type].references == 0) {
        printf("WL MINIOBJ ERROR: type cannot be unregistered while there are references to it: %u\n", obj->type);
        return;
    }
    wl_miniobj_priv * obj_ = (wl_miniobj_priv *) obj;
    // each reference has its own fd
    if (WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED)) {
        obj_->references--;
    }
    if (!WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED) && !WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED_MANUALLY)) {
        obj_->references--;
        if (obj_->references == 0) {
            WL_MINIOBJ_ADD_FLAG(obj_->flags, FLAG_IS_BEING_DESTROYED);
        }
        WL_MINIOBJ_ADD_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED_MANUALLY);
        wl_syscalls__fd_allocator__deallocate_fd(wl_miniobj_fd_list, fd);
        WL_MINIOBJ_REMOVE_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED_MANUALLY);
    }
    if (obj_->references == 0) {
        wl_miniobj__registry[obj->type].references--;
        if (wl_miniobj_log_allocation) printf("WL MINIOBJ WARNING: deallocate (free) object: %p, with type: %u\n", obj, obj->type);
        free(obj);
        wl_miniobj_total_deallocations++;
    }
}

// OBJ, SYSCALL, CALLBACK, SYSCALL, CALLBACK, ...
void wl_miniobj_internal_reg(uint16_t obj_type, int argc, ...) {
    if (wl_miniobj__registry_count == 65535) {
        printf("WL MINIOBJ ERROR: maximum number of objects have been registered, please unregister an object in order to make room for another object to be registered\n");
        return;
    }

    if (argc % 2 != 0) {
        printf("WL MINIOBJ ERROR: argc must be a multiple of 2\n");
        return;
    }

    wl_miniobj_init();

    if (wl_miniobj__registry[obj_type].registered) {
        printf("WL MINIOBJ ERROR: type conflict: %u\n", obj_type);
        return;
    }

    wl_miniobj__registry[obj_type].registered = true;


    va_list ap;
    va_start(ap, argc);

    uint16_t ac = argc/2;
   
    wl_miniobj__registry[obj_type].references = 0;
    wl_miniobj__registry[obj_type].count = ac;
    wl_miniobj__registry[obj_type].accepted_syscalls = (uint16_t*) malloc(ac*sizeof(uint16_t));
    wl_miniobj__registry[obj_type].callbacks = (WL_MINIOBJ_CALLBACK*) malloc(ac*sizeof(WL_MINIOBJ_CALLBACK));
    
    for(int i = 0; i < ac; i++) {
        wl_miniobj__registry[obj_type].accepted_syscalls[i] = (uint16_t)va_arg(ap, int);
        wl_miniobj__registry[obj_type].callbacks[i] = va_arg(ap, WL_MINIOBJ_CALLBACK);
    }

    wl_miniobj__registry_count++;
}

void wl_miniobj_unreg(uint16_t obj_type) {
    if (wl_miniobj__registry_count == 0) {
        printf("WL MINIOBJ ERROR: no objects have been registered, cannot unregister type %u\n", obj_type);
        return;
    }
    if (wl_miniobj__registry[obj_type].registered) {
        if (wl_miniobj__registry[obj_type].references != 0) {
            printf("WL MINIOBJ ERROR: type cannot be unregistered while there are references to it: %u\n", obj_type);
            return;
        }
        free(wl_miniobj__registry[obj_type].callbacks);
        free(wl_miniobj__registry[obj_type].accepted_syscalls);
        wl_miniobj__registry[obj_type].callbacks = NULL;
        wl_miniobj__registry[obj_type].accepted_syscalls = NULL;
        wl_miniobj__registry[obj_type].count = 0;
        wl_miniobj__registry[obj_type].registered = false;
        wl_miniobj__registry_count--;
    } else {
        printf("WL MINIOBJ ERROR: type already unregistered: %u\n", obj_type);
    }
}

int WL_MINIOBJ__DO_SYSCALL(int fd, uint16_t syscall, int argc, ...) {
    if (wl_miniobj__registry_count == 0) {
        printf("WL MINIOBJ ERROR: no objects have been registered\n");
        return -1;
    }

    // we cannot use wl_miniobj_get here as it would ambiguate errors
    //
    wl_miniobj * obj = (wl_miniobj *) wl_miniobj_get_priv(fd);

    if (obj == NULL) {
        printf("WL MINIOBJ ERROR: NULL object was passed to syscall\n");
        return -1;
    }
    int data = 0;
    va_list ap;
    va_start(ap, argc);
    switch(syscall) {

        case 0: // wl_syscall__accept4
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__accept4' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 0) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__accept4' for object type %u\n", obj->type);
            }
            break;
        }

        case 1: // wl_syscall__close
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__close' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 1) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__close' for object type %u\n", obj->type);
            }
            break;
        }

        case 2: // wl_syscall__dupfd
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__dupfd' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 2) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__dupfd' for object type %u\n", obj->type);
            }
            break;
        }

        case 3: // wl_syscall__dupfd_2
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__dupfd_2' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 3) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__dupfd_2' for object type %u\n", obj->type);
            }
            break;
        }

        case 4: // wl_syscall__set_exec_or_close
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__set_exec_or_close' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 4) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__set_exec_or_close' for object type %u\n", obj->type);
            }
            break;
        }

        case 5: // wl_syscall__read
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__read' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 5) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__read' for object type %u\n", obj->type);
            }
            break;
        }

        case 6: // wl_syscall__write
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__write' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 6) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__write' for object type %u\n", obj->type);
            }
            break;
        }

        case 7: // wl_syscall__listen
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__listen' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 7) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__listen' for object type %u\n", obj->type);
            }
            break;
        }

        case 8: // wl_syscall__bind
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__bind' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 8) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__bind' for object type %u\n", obj->type);
            }
            break;
        }

        case 9: // wl_syscall__connect
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__connect' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 9) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__connect' for object type %u\n", obj->type);
            }
            break;
        }

        case 10: // wl_syscall__mkstemp
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__mkstemp' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 10) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__mkstemp' for object type %u\n", obj->type);
            }
            break;
        }

        case 11: // wl_syscall__send
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__send' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 11) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__send' for object type %u\n", obj->type);
            }
            break;
        }

        case 12: // wl_syscall__recv
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__recv' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 12) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__recv' for object type %u\n", obj->type);
            }
            break;
        }

        case 13: // wl_syscall__peercred
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__peercred' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 13) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__peercred' for object type %u\n", obj->type);
            }
            break;
        }

        case 18: // wl_syscall__epoll_ctl
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__epoll_ctl' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 18) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__epoll_ctl' for object type %u\n", obj->type);
            }
            break;
        }

        case 19: // wl_syscall__epoll_wait
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__epoll_wait' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 19) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__epoll_wait' for object type %u\n", obj->type);
            }
            break;
        }

        case 20: // wl_syscall__timerfd_settime
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__timerfd_settime' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 20) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__timerfd_settime' for object type %u\n", obj->type);
            }
            break;
        }

        case 21: // wl_syscall__timerfd_gettime
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__timerfd_gettime' has been passed an unregistered object type: %u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == 21) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__timerfd_gettime' for object type %u\n", obj->type);
            }
            break;
        }

        default:
        {
            printf("WL MINIOBJ ERROR: unknown syscall: %u\n", syscall);
            break;
        }
    }
    va_end(ap);
    return data;
}


const int WL_MINIOBJ_SYSCALL__NR__ACCEPT4 = 0;
const int WL_MINIOBJ_SYSCALL__NR__CLOSE = 1;
const int WL_MINIOBJ_SYSCALL__NR__DUPFD = 2;
const int WL_MINIOBJ_SYSCALL__NR__DUPFD_2 = 3;
const int WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE = 4;
const int WL_MINIOBJ_SYSCALL__NR__READ = 5;
const int WL_MINIOBJ_SYSCALL__NR__WRITE = 6;
const int WL_MINIOBJ_SYSCALL__NR__LISTEN = 7;
const int WL_MINIOBJ_SYSCALL__NR__BIND = 8;
const int WL_MINIOBJ_SYSCALL__NR__CONNECT = 9;
const int WL_MINIOBJ_SYSCALL__NR__MKSTEMP = 10;
const int WL_MINIOBJ_SYSCALL__NR__SEND = 11;
const int WL_MINIOBJ_SYSCALL__NR__RECV = 12;
const int WL_MINIOBJ_SYSCALL__NR__PEERCRED = 13;
const int WL_MINIOBJ_SYSCALL__NR__EPOLL_CTL = 18;
const int WL_MINIOBJ_SYSCALL__NR__EPOLL_WAIT = 19;
const int WL_MINIOBJ_SYSCALL__NR__TIMERFD_SETTIME = 20;
const int WL_MINIOBJ_SYSCALL__NR__TIMERFD_GETTIME = 21;
