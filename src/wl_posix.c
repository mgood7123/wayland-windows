#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "wl_posix.h"
#define NEW(type, name) type * name = (type *) malloc(sizeof(type))
#define NEW_ARRAY(type, name, count) type * name = (type *) malloc(sizeof(type)*count)
#define ZERO_NEW(type, name) type * name = (type *) calloc(1, sizeof(type))
#define ZERO_NEW_ARRAY(type, name, count) type * name = (type *) calloc(count, sizeof(type))

#ifdef _WIN32
#define WL_POSIX_SOCK_T SOCKET

errno_t err__map_win_error_to_errno(DWORD error);

static bool check_error(const char * name, int err) {
    if (err == -1) {
        DWORD err = GetLastError();
        int errwsa = WSAGetLastError();
        char err_s[4096];
        char errwsa_s[4096];
        strerror_s(err_s, 4096, err__map_win_error_to_errno(err));
        strerror_s(errwsa_s, 4096, err__map_win_error_to_errno(errwsa));
        printf("obtained an error for name: '%s'\n    GetLastError integer = %lu\n    GetLastError string = %s\n    WSAGetLastError integer = %d\n    WSAGetLastError string = %s\n", name, err, err_s, errwsa, errwsa_s);
        return true;
    }
    return false;
}

static bool check_error_socket(const char* name, SOCKET err) {
    return check_error(name, (err == INVALID_SOCKET || err == SOCKET_ERROR) ? -1 : 0);
}

#else

#define WL_POSIX_SOCK_T int

static bool check_error(const char * name, int fd) {
    if (fd > 0) {
        printf("obtained an error for name: '%s'\n  errno string = %s\n", name, strerror(errno));
        return true;
    }
    return false;
}

static bool check_error_socket(const char * name, int fd) {
    return check_error(name, fd);
}

#endif

#ifdef _WIN32
typedef struct WL_POSIX_SOCKET_T {
    SOCKET socket;
    HANDLE last_epoll;
    char *addr;
} WL_POSIX_SOCKET_T;
#endif

int wl_posix_create_fd(void) {
    return wl_miniobj_create(WL_POSIX_FD_VERSION, wl_posix_type_fd, true);
}

int wl_posix_create_socket(int domain, int type, int protocol) {
#if _WIN32
    SOCKET s = socket(domain, type, protocol);
    if (s <= 0) {
        return -1;
    }
    ZERO_NEW(WL_POSIX_SOCKET_T, st);
    if (st == NULL) {
        closesocket(s);
        errno = ENOMEM;
        return -1;
    }
    st->socket = s;
    WL_MINIOBJ_CREATE_WITH_OBJ(sockfd, sock, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
    sock->data = (uint64_t) st;
    if (WL_MINIOBJ_HAS_FLAG(type, SOCK_CLOEXEC)) {
        return wl_syscall__set_exec_or_close(sockfd);
    }
    return sockfd;
#else
        int s;
        if (WL_MINIOBJ_HAS_FLAG(type, SOCK_CLOEXEC)) {
            s = socket(domain, type, protocol);
            if (s >= 0) {
                WL_MINIOBJ_CREATE_WITH_OBJ(sockfd, sock, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
                sock->data = (uint64_t) s;
                return sockfd;
            }
            if (errno != EINVAL) {
                return -1;
            }
            WL_MINIOBJ_REMOVE_FLAG(type, SOCK_CLOEXEC);
        }
        s = socket(domain, type, protocol);
        if (s >= 0) {
            WL_MINIOBJ_CREATE_WITH_OBJ(sockfd, sock, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
            sock->data = (uint64_t) s;
            return wl_syscall__set_exec_or_close(sockfd);
        }
        return -1;
#endif
}

#ifdef _WIN32
HANDLE epoll_create(int size);
HANDLE epoll_create1(int flags);

int epoll_close(HANDLE ephnd);
int epoll_ctl(HANDLE ephnd, int op, SOCKET sock, struct epoll_event* events);
int epoll_wait(HANDLE ephnd, struct epoll_event* events, int maxevents, int timeout);

#include "wl_posix_epoll.c"
#endif


int wl_posix_create_epoll(int flags) {
#ifdef _WIN32
    if (WL_MINIOBJ_HAS_INVALID_FLAGS(flags, EPOLL_CLOEXEC)) {
        errno = EINVAL;
        return -1;
    }
    HANDLE h = epoll_create(1);
    if (h == INVALID_HANDLE_VALUE) {
        errno = EINVAL;
        return -1;
    }
    WL_MINIOBJ_CREATE_WITH_OBJ(epoll_fd, epoll, WL_POSIX_FD_VERSION, wl_posix_type_epoll, false);
    epoll->data = (uint64_t) h;
    if (WL_MINIOBJ_HAS_FLAG(flags, EPOLL_CLOEXEC)) {
        return wl_syscall__set_exec_or_close(epoll_fd);
    }
    return epoll_fd;
#else
    int fd = epoll_create1(flags);
    if (fd >= 0) {
        WL_MINIOBJ_CREATE_WITH_OBJ(epoll_fd, epoll, WL_POSIX_FD_VERSION, wl_posix_type_epoll, true);
        epoll->data = = (uint64_t) fd;
        return epoll_fd;
    }
    if (errno != EINVAL) {
        int saved = errno;
        errno = saved;
        return -1;
    }
    fd = epoll_create(1);
    if (fd >= 0) {
        WL_MINIOBJ_CREATE_WITH_OBJ(epoll_fd, epoll, WL_POSIX_FD_VERSION, wl_posix_type_epoll, true);
        epoll->data = (uint64_t) fd;
        if (WL_MINIOBJ_HAS_FLAG(flags, EPOLL_CLOEXEC)) {
            return (wl_miniobj*) wl_syscall__set_exec_or_close(epoll_fd);
        }
        return epoll_fd;
    }
    return -1;
#endif
}

#ifdef _WIN32
typedef struct WL_POSIX_EVENTFD_T {
    HANDLE fd;
    BOOL nonblock;
    BOOL semaphore;
    uint64_t counter;
    CRITICAL_SECTION criticalSection;
} WL_POSIX_EVENTFD_T;
#endif

int wl_posix_create_eventfd(unsigned int initval, int flags) {
#ifdef _WIN32
    if (WL_MINIOBJ_HAS_INVALID_FLAGS(flags, EFD_CLOEXEC | EFD_NONBLOCK | EFD_SEMAPHORE)) {
        errno = EINVAL;
        return -1;
    }

    HANDLE h = CreateEventW(NULL, TRUE, TRUE, NULL);
    if (h == WL_POSIX_FD_INVALID_HANDLE_VALUE) {
        errno = EINVAL;
        return -1;
    }

    NEW(WL_POSIX_EVENTFD_T, fd);

    fd->counter = initval;
    fd->nonblock = WL_MINIOBJ_HAS_FLAG(flags, EFD_NONBLOCK);
    fd->semaphore = WL_MINIOBJ_HAS_FLAG(flags, EFD_SEMAPHORE);

    fd->fd = h;

    InitializeCriticalSectionAndSpinCount(&fd->criticalSection, 4000);

    WL_MINIOBJ_CREATE_WITH_OBJ(eventfd_fd, eventfd, WL_POSIX_FD_VERSION, wl_posix_type_eventfd, false);
    eventfd->data = (uint64_t) fd;
    if (WL_MINIOBJ_HAS_FLAG(flags, EFD_CLOEXEC))
        return wl_syscall__set_exec_or_close(eventfd_fd);
    return eventfd_fd;
#else
    int fd = eventfd(initval, flags);
    if (fd >= 0) {
        WL_MINIOBJ_CREATE_WITH_OBJ(eventfd, WL_POSIX_FD_VERSION, wl_posix_type_eventfd, true);
        eventfd->data = (uint64_t) fd;
        return eventfd;
    }
    return -1;
#endif
}

// https://github.com/dsprenkels/randombytes/blob/master/randombytes.c

// In the case that are compiling on linux, we need to define _GNU_SOURCE
// *before* randombytes.h is included. Otherwise SYS_getrandom will not be
// declared.
#if defined(__linux__) || defined(__GNU__)
# define _GNU_SOURCE
#endif /* defined(__linux__) || defined(__GNU__) */

#if defined(_WIN32)
/* Windows */
# include <windows.h>
# include <wincrypt.h> /* CryptAcquireContext, CryptGenRandom */
#endif /* defined(_WIN32) */

/* wasi */
#if defined(__wasi__)
#include <stdlib.h>
#endif

/* kFreeBSD */
#if defined(__FreeBSD_kernel__) && defined(__GLIBC__)
# define GNU_KFREEBSD
#endif

#if defined(__linux__) || defined(__GNU__) || defined(GNU_KFREEBSD)
/* Linux */
// We would need to include <linux/random.h>, but not every target has access
// to the linux headers. We only need RNDGETENTCNT, so we instead inline it.
// RNDGETENTCNT is originally defined in `include/uapi/linux/random.h` in the
// linux repo.
# define RNDGETENTCNT 0x80045200

# include <assert.h>
# include <errno.h>
# include <fcntl.h>
# include <poll.h>
# include <stdint.h>
# include <stdio.h>
# include <sys/ioctl.h>
# if (defined(__linux__) || defined(__GNU__)) && defined(__GLIBC__) && ((__GLIBC__ > 2) || (__GLIBC_MINOR__ > 24))
#  define USE_GLIBC
#  include <sys/random.h>
# endif /* (defined(__linux__) || defined(__GNU__)) && defined(__GLIBC__) && ((__GLIBC__ > 2) || (__GLIBC_MINOR__ > 24)) */
# include <sys/stat.h>
# include <sys/syscall.h>
# include <sys/types.h>
# include <unistd.h>

// We need SSIZE_MAX as the maximum read len from /dev/urandom
# if !defined(SSIZE_MAX)
#  define SSIZE_MAX (SIZE_MAX / 2 - 1)
# endif /* defined(SSIZE_MAX) */

#endif /* defined(__linux__) || defined(__GNU__) || defined(GNU_KFREEBSD) */


#if defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
/* Dragonfly, FreeBSD, NetBSD, OpenBSD (has arc4random) */
# include <sys/param.h>
# if defined(BSD)
#  include <stdlib.h>
# endif
/* GNU/Hurd defines BSD in sys/param.h which causes problems later */
# if defined(__GNU__)
#  undef BSD
# endif
#endif

#if defined(__EMSCRIPTEN__)
# include <assert.h>
# include <emscripten.h>
# include <errno.h>
# include <stdbool.h>
#endif /* defined(__EMSCRIPTEN__) */


#if defined(_WIN32)
static int randombytes_win32_randombytes(void* buf, size_t n)
{
	HCRYPTPROV ctx;
	BOOL tmp;
	DWORD to_read = 0;
	const size_t MAX_DWORD = 0xFFFFFFFF;

	tmp = CryptAcquireContext(&ctx, NULL, NULL, PROV_RSA_FULL,
	                          CRYPT_VERIFYCONTEXT);
	if (tmp == FALSE) return -1;

	while (n > 0) {
		to_read = (DWORD)(n < MAX_DWORD ? n : MAX_DWORD);
		tmp = CryptGenRandom(ctx, to_read, (BYTE*) buf);
		if (tmp == FALSE) return -1;
		buf = ((char*)buf) + to_read;
		n -= to_read;
	}

	tmp = CryptReleaseContext(ctx, 0);
	if (tmp == FALSE) return -1;

	return 0;
}
#endif /* defined(_WIN32) */

#if defined(__wasi__)
static int randombytes_wasi_randombytes(void *buf, size_t n) {
	arc4random_buf(buf, n);
	return 0;
}
#endif /* defined(__wasi__) */

#if (defined(__linux__) || defined(__GNU__)) && (defined(USE_GLIBC) || defined(SYS_getrandom))
# if defined(USE_GLIBC)
// getrandom is declared in glibc.
# elif defined(SYS_getrandom)
static ssize_t getrandom(void *buf, size_t buflen, unsigned int flags) {
	return syscall(SYS_getrandom, buf, buflen, flags);
}
# endif

static int randombytes_linux_randombytes_getrandom(void *buf, size_t n)
{
	/* I have thought about using a separate PRF, seeded by getrandom, but
	 * it turns out that the performance of getrandom is good enough
	 * (250 MB/s on my laptop).
	 */
	size_t offset = 0, chunk;
	int ret;
	while (n > 0) {
		/* getrandom does not allow chunks larger than 33554431 */
		chunk = n <= 33554431 ? n : 33554431;
		do {
			ret = getrandom((char *)buf + offset, chunk, 0);
		} while (ret == -1 && errno == EINTR);
		if (ret < 0) return ret;
		offset += ret;
		n -= ret;
	}
	assert(n == 0);
	return 0;
}
#endif /* (defined(__linux__) || defined(__GNU__)) && (defined(USE_GLIBC) || defined(SYS_getrandom)) */

#if (defined(__linux__) || defined(GNU_KFREEBSD)) && !defined(SYS_getrandom)

# if defined(__linux__)
static int randombytes_linux_read_entropy_ioctl(int device, int *entropy)
{
	return ioctl(device, RNDGETENTCNT, entropy);
}

static int randombytes_linux_read_entropy_proc(FILE *stream, int *entropy)
{
	int retcode;
	do {
		rewind(stream);
		retcode = fscanf(stream, "%d", entropy);
	} while (retcode != 1 && errno == EINTR);
	if (retcode != 1) {
		return -1;
	}
	return 0;
}

static int randombytes_linux_wait_for_entropy(int device)
{
	/* We will block on /dev/random, because any increase in the OS' entropy
	 * level will unblock the request. I use poll here (as does libsodium),
	 * because we don't *actually* want to read from the device. */
	enum { IOCTL, PROC } strategy = IOCTL;
	const int bits = 128;
	struct pollfd pfd;
	int fd;
	FILE *proc_file;
	int retcode, retcode_error = 0; // Used as return codes throughout this function
	int entropy = 0;

	/* If the device has enough entropy already, we will want to return early */
	retcode = randombytes_linux_read_entropy_ioctl(device, &entropy);
	// printf("errno: %d (%s)\n", errno, strerror(errno));
	if (retcode != 0 && (errno == ENOTTY || errno == ENOSYS)) {
		// The ioctl call on /dev/urandom has failed due to a
		//   - ENOTTY (unsupported action), or
		//   - ENOSYS (invalid ioctl; this happens on MIPS, see #22).
		//
		// We will fall back to reading from
		// `/proc/sys/kernel/random/entropy_avail`.  This less ideal,
		// because it allocates a file descriptor, and it may not work
		// in a chroot.  But at this point it seems we have no better
		// options left.
		strategy = PROC;
		// Open the entropy count file
		proc_file = fopen("/proc/sys/kernel/random/entropy_avail", "r");
		if (proc_file == NULL) {
			return -1;
		}
	} else if (retcode != 0) {
		// Unrecoverable ioctl error
		return -1;
	}
	if (entropy >= bits) {
		return 0;
	}

	do {
		fd = open("/dev/random", O_RDONLY);
	} while (fd == -1 && errno == EINTR); /* EAGAIN will not occur */
	if (fd == -1) {
		/* Unrecoverable IO error */
		return -1;
	}

	pfd.fd = fd;
	pfd.events = POLLIN;
	for (;;) {
		retcode = poll(&pfd, 1, -1);
		if (retcode == -1 && (errno == EINTR || errno == EAGAIN)) {
			continue;
		} else if (retcode == 1) {
			if (strategy == IOCTL) {
				retcode = randombytes_linux_read_entropy_ioctl(device, &entropy);
			} else if (strategy == PROC) {
				retcode = randombytes_linux_read_entropy_proc(proc_file, &entropy);
			} else {
				return -1; // Unreachable
			}

			if (retcode != 0) {
				// Unrecoverable I/O error
				retcode_error = retcode;
				break;
			}
			if (entropy >= bits) {
				break;
			}
		} else {
			// Unreachable: poll() should only return -1 or 1
			retcode_error = -1;
			break;
		}
	}
	do {
		retcode = close(fd);
	} while (retcode == -1 && errno == EINTR);
	if (strategy == PROC) {
		do {
			retcode = fclose(proc_file);
		} while (retcode == -1 && errno == EINTR);
	}
	if (retcode_error != 0) {
		return retcode_error;
	}
	return retcode;
}
# endif /* defined(__linux__) */


static int randombytes_linux_randombytes_urandom(void *buf, size_t n)
{
	int fd;
	size_t offset = 0, count;
	ssize_t tmp;
	do {
		fd = open("/dev/urandom", O_RDONLY);
	} while (fd == -1 && errno == EINTR);
	if (fd == -1) return -1;
# if defined(__linux__)
	if (randombytes_linux_wait_for_entropy(fd) == -1) return -1;
# endif

	while (n > 0) {
		count = n <= SSIZE_MAX ? n : SSIZE_MAX;
		tmp = read(fd, (char *)buf + offset, count);
		if (tmp == -1 && (errno == EAGAIN || errno == EINTR)) {
			continue;
		}
		if (tmp == -1) return -1; /* Unrecoverable IO error */
		offset += tmp;
		n -= tmp;
	}
	close(fd);
	assert(n == 0);
	return 0;
}
#endif /* defined(__linux__) && !defined(SYS_getrandom) */


#if defined(BSD)
static int randombytes_bsd_randombytes(void *buf, size_t n)
{
	arc4random_buf(buf, n);
	return 0;
}
#endif /* defined(BSD) */


#if defined(__EMSCRIPTEN__)
static int randombytes_js_randombytes_nodejs(void *buf, size_t n) {
	const int ret = EM_ASM_INT({
		var crypto;
		try {
			crypto = require('crypto');
		} catch (error) {
			return -2;
		}
		try {
			writeArrayToMemory(crypto.randomBytes($1), $0);
			return 0;
		} catch (error) {
			return -1;
		}
	}, buf, n);
	switch (ret) {
	case 0:
		return 0;
	case -1:
		errno = EINVAL;
		return -1;
	case -2:
		errno = ENOSYS;
		return -1;
	}
	assert(false); // Unreachable
}
#endif /* defined(__EMSCRIPTEN__) */


int randombytes(void *buf, size_t n)
{
#if defined(__EMSCRIPTEN__)
	return randombytes_js_randombytes_nodejs(buf, n);
#elif defined(__linux__) || defined(__GNU__) || defined(GNU_KFREEBSD)
# if defined(USE_GLIBC)
	/* Use getrandom system call */
	return randombytes_linux_randombytes_getrandom(buf, n);
# elif defined(SYS_getrandom)
	/* Use getrandom system call */
	return randombytes_linux_randombytes_getrandom(buf, n);
# else
	/* When we have enough entropy, we can read from /dev/urandom */
	return randombytes_linux_randombytes_urandom(buf, n);
# endif
#elif defined(BSD)
	/* Use arc4random system call */
	return randombytes_bsd_randombytes(buf, n);
#elif defined(_WIN32)
	/* Use windows API */
	return randombytes_win32_randombytes(buf, n);
#elif defined(__wasi__)
	/* Use WASI */
	return randombytes_wasi_randombytes(buf, n);
#else
# error "randombytes(...) is not supported on this platform"
#endif
}


#ifdef _WIN32

#include <time.h>
#include <errno.h>

/* These are the characters used in temporary filenames.  */
static const char letters[] =
"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
#define NUM_LETTERS (62)

int gettimeofday(struct timeval_64_t *spec)
{
    __int64 wintime;
    GetSystemTimeAsFileTime((FILETIME*)&wintime);
    wintime      -=116444736000000000i64;  //1jan1601 to 1jan1970
    spec->tv_sec  =wintime / 10000000i64;  //seconds
    spec->tv_usec =wintime % 10000000i64;  //milli-seconds
    return 0;
}

static void brain_damaged_fillrand(unsigned char *buf, unsigned int len)
{
    unsigned int i, k;
    struct timeval_64_t tv;
    uint32_t high, low, rh;
    static uint64_t value;
    gettimeofday(&tv);
    value += ((uint64_t) tv.tv_usec << 16) ^ tv.tv_sec ^ GetCurrentProcessId();
    low = value & UINT32_MAX;
    high = value >> 32;
    for (i = 0; i < len; ++i) {
        rh = high % NUM_LETTERS;
        high /= NUM_LETTERS;
#define _L ((UINT32_MAX % NUM_LETTERS + 1) % NUM_LETTERS)
        k = (low % NUM_LETTERS) + (_L * rh);
#undef _L
#define _H ((UINT32_MAX / NUM_LETTERS) + ((UINT32_MAX % NUM_LETTERS + 1) / NUM_LETTERS))
        low = (low / NUM_LETTERS) + (_H * rh) + (k / NUM_LETTERS);
#undef _H
        k %= NUM_LETTERS;
        buf[i] = letters[k];
    }
}

typedef struct WL_POSIX_TMP_FILE_T {
    HANDLE fd;
    char path[MAX_PATH];
} WL_POSIX_TMP_FILE_T;

LP64__LONG random(void) {
    LP64__LONG randomness[1];
    if (randombytes((unsigned char*)randomness, sizeof(LP64__LONG)) != sizeof(LP64__LONG)) {
        /* if random device nodes failed us, lets use the braindamaged ver */
        brain_damaged_fillrand((unsigned char*)randomness, sizeof(LP64__LONG));
    }
    return randomness[0];
}

#define MS_PER_SEC      1000ULL     // MS = milliseconds
#define US_PER_MS       1000ULL     // US = microseconds
#define HNS_PER_US      10ULL       // HNS = hundred-nanoseconds (e.g., 1 hns = 100 ns)
#define NS_PER_US       1000ULL

#define HNS_PER_SEC     (MS_PER_SEC * US_PER_MS * HNS_PER_US)
#define NS_PER_HNS      (100ULL)    // NS = nanoseconds
#define NS_PER_SEC      (MS_PER_SEC * US_PER_MS * NS_PER_US)

static int clock_gettime_monotonic(struct timespec_N_bit *tv)
{
    static LARGE_INTEGER ticksPerSec;
    LARGE_INTEGER ticks;
    double seconds;

    if (!ticksPerSec.QuadPart) {
        QueryPerformanceFrequency(&ticksPerSec);
        if (!ticksPerSec.QuadPart) {
            errno = ENOTSUP;
            return -1;
        }
    }

    QueryPerformanceCounter(&ticks);

    seconds = (double) ticks.QuadPart / (double) ticksPerSec.QuadPart;
    tv->tv_sec = (time_64_t) seconds;
    tv->tv_nsec = (LP64__LONG)((ULONGLONG)(seconds * NS_PER_SEC) % NS_PER_SEC);

    return 0;
}

static int clock_gettime_realtime(struct timespec_N_bit *tv)
{
    FILETIME ft;
    ULARGE_INTEGER hnsTime;

    GetSystemTimeAsFileTime(&ft);

    hnsTime.LowPart = ft.dwLowDateTime;
    hnsTime.HighPart = ft.dwHighDateTime;

    // To get POSIX Epoch as baseline, subtract the number of hns intervals from Jan 1, 1601 to Jan 1, 1970.
    hnsTime.QuadPart -= (11644473600ULL * HNS_PER_SEC);

    // modulus by hns intervals per second first, then convert to ns, as not to lose resolution
    tv->tv_nsec = (LP64__LONG) ((hnsTime.QuadPart % HNS_PER_SEC) * NS_PER_HNS);
    tv->tv_sec = (LP64__LONG) (hnsTime.QuadPart / HNS_PER_SEC);

    return 0;
}

int clock_gettime(clockid_t type, struct timespec_N_bit *tp)
{
    if (type == CLOCK_MONOTONIC)
    {
        return clock_gettime_monotonic(tp);
    }
    else if (type == CLOCK_REALTIME)
    {
        return clock_gettime_realtime(tp);
    }

    errno = ENOTSUP;
    return -1;
}

char * strndup (const char *s, size_t n)
{
    char *result;
    size_t len = strnlen (s, n);

    result = (char *) malloc (len + 1);
    if (!result)
    return 0;

    result[len] = '\0';
    return (char *) memcpy (result, s, len);
}

#endif

// based on
// https://github.com/wbx-github/uclibc-ng/blob/master/libc/misc/internals/tempname.c#L166

/* Generate a temporary file name based on TMPL. TMPL must match the
   rules for mk[s]temp[s] (i.e. end in "prefixXXXXXXsuffix"). The name
   constructed does not exist at the time of the call to __gen_tempname.
   TMPL is overwritten with the result.
   KIND may be one of:
   __GT_NOCREATE:       simply verify that the name does not exist
                        at the time of the call. mode argument is ignored.
   __GT_FILE:           create the file using open(O_CREAT|O_EXCL)
                        and return a read-write fd with given mode.
   __GT_BIGFILE:        same as __GT_FILE but use open64().
   __GT_DIR:            create a directory with given mode.
*/

int wl_posix_create_mkstemp(char *template) {
#ifdef _WIN32

    char *XXXXXX;
    unsigned int i;
    int save_errno = errno;
    unsigned char randomness[6];
    size_t len;
    int suffixlen = 0; // always zero

    len = strlen (template);
    /* This is where the Xs start.  */
    XXXXXX = template + len - 6 - suffixlen;
    if (len < 6 || suffixlen < 0 || suffixlen > len - 6
        || strncmp (XXXXXX, "XXXXXX", 6))
    {
        errno = EINVAL;
        return -1;
    }
    
#ifndef TMP_MAX
#define TMP_MAX 238328
#endif

    NEW(WL_POSIX_TMP_FILE_T, tmp);
    if (tmp == NULL) {
        return -1;
    }

    for (i = 0; i < TMP_MAX; ++i) {
        unsigned char j;
        /* Get some random data.  */
        if (randombytes(randomness, sizeof(randomness)) != sizeof(randomness)) {
            /* if random device nodes failed us, lets use the braindamaged ver */
            brain_damaged_fillrand(randomness, sizeof(randomness));
        }
        for (j = 0; j < sizeof(randomness); ++j)
            XXXXXX[j] = letters[randomness[j] % NUM_LETTERS];
        
        char tmp_path[MAX_PATH];
        DWORD rp = GetTempPathA(MAX_PATH, tmp_path);
        if (!(rp > MAX_PATH || rp == 0)) {
            snprintf(tmp->path, MAX_PATH, "%s/%s", tmp_path, template);

            tmp->fd = CreateFile (
                tmp->path,
                GENERIC_READ | GENERIC_WRITE,
                0,
                NULL,
                OPEN_ALWAYS,
                FILE_ATTRIBUTE_TEMPORARY,
                NULL
            );

            if (tmp->fd == INVALID_HANDLE_VALUE) {
                if (GetLastError() != ERROR_ALREADY_EXISTS) {
                    /* Any other error will apply also to other names we might
                    try, and there are 2^32 or so of them, so give up now. */
                    free(tmp);
                    return -1;
                }
                continue;
            }
            // we succeeded in creating the file
            WL_MINIOBJ_CREATE_WITH_OBJ(tmp_fd, tmp_obj, WL_POSIX_FD_VERSION, wl_posix_type_temporary_file, false);
            tmp_obj->data = (uint64_t) tmp;
            return tmp_fd;
        }
        // we failed to get a temporary path
        errno = save_errno;
        return -1;
    }

    /* We got out of the loop because we ran out of combinations to try.  */
    errno = EEXIST;
    return -1;
#else
    int fd = mkstemp(template);
    if (fd >= 0) {
        WL_MINIOBJ_CREATE_WITH_OBJ(tmp, tmp_obj, WL_POSIX_FD_VERSION, wl_posix_type_temporary_file, true);
        tmp_obj->data = (uint64_t) fd;
        return tmp;
    }
    return -1;
#endif
}

#ifdef _WIN32

struct SOCKETPAIRACCEPTINFO {
    SOCKET source;
    struct sockaddr_un target;
};

static DWORD WINAPI acceptThread(PVOID p)
{
    struct SOCKETPAIRACCEPTINFO * sock = (struct SOCKETPAIRACCEPTINFO*) p;
    puts("CONNECT THREAD");
    int error = connect(sock->source, (struct sockaddr*)&(sock->target), sizeof(sock->target));
    if (check_error("connect 0", error)) {
        return -1;
    }
    puts("CONNECTED THREAD");
    return 0;
}

static int internal_socketpair(int domain, int type, int protocol, SOCKET sv[2], char * path_108) {
    if (!(domain == AF_INET || domain == AF_UNIX)) {
        errno = EINVAL;
        return -1;
    }

    if (domain == AF_UNIX && type != SOCK_STREAM) {
        errno = EINVAL;
        return -1;
    }

    SOCKET pair[2] = { INVALID_SOCKET, INVALID_SOCKET};
    int32_t error = 0;

    pair[0] = socket(domain, type, protocol);
    if (check_error_socket("socket 0", pair[0])) {
        return -1;
    }
    pair[1] = socket(domain, type, protocol);
    if (check_error_socket("socket 1", pair[1])) {
        closesocket(pair[0]);
        return -1;
    }

    if (domain == AF_INET) {
        struct sockaddr_in address[2];
        memset(&(address[0]), 0, sizeof(struct sockaddr_in));
        memset(&(address[1]), 0, sizeof(struct sockaddr_in));
        address[0].sin_family = AF_INET;
        address[1].sin_family = AF_INET;
        address[0].sin_addr.s_addr = htonl(INADDR_LOOPBACK);
        address[1].sin_addr.s_addr = htonl(INADDR_LOOPBACK);

        int namelen;

        memset(&(address[0]), 0, sizeof(struct sockaddr_in));
        address[0].sin_family = AF_INET;
        address[0].sin_addr.s_addr = htonl(INADDR_LOOPBACK);
        error = bind(pair[0], (struct sockaddr*)&(address[0]), sizeof(struct sockaddr_in));
        if (check_error("bind 0", error)) {
            closesocket(pair[0]);
            closesocket(pair[1]);
            return -1;
        }
        error = getsockname(pair[0], (struct sockaddr*)&(address[0]), &namelen);
        if (check_error("getsockname 0", error)) {
            closesocket(pair[0]);
            closesocket(pair[1]);
            return -1;
        }
        if (sizeof(struct sockaddr_in) != namelen) {
            printf("socket name does not match legnth of struct sockaddr_in\n");
            closesocket(pair[0]);
            closesocket(pair[1]);
            return -1;
        }

        error = bind(pair[1], (struct sockaddr*)&(address[1]), sizeof(struct sockaddr_in));
        if (check_error("bind 1", error)) {
            closesocket(pair[0]);
            closesocket(pair[1]);
            return -1;
        }
        error = getsockname(pair[1], (struct sockaddr*)&(address[1]), &namelen);
        if (check_error("getsockname 1", error)) {
            closesocket(pair[0]);
            closesocket(pair[1]);
            return -1;
        }
        if (sizeof(struct sockaddr_in) != namelen) {
            printf("socket name does not match legnth of struct sockaddr_in\n");
            closesocket(pair[0]);
            closesocket(pair[1]);
            return -1;
        }

        error = connect(pair[0], (struct sockaddr*)&(address[1]), namelen);
        if (check_error("connect", error)) {
            closesocket(pair[0]);
            closesocket(pair[1]);
            return -1;
        }
        error = connect(pair[1], (struct sockaddr*)&(address[0]), namelen);
        if (check_error("connect", error)) {
            closesocket(pair[0]);
            closesocket(pair[1]);
            return -1;
        }
    } else {
        // AF_UNIX
        char AF_UNIX_PATH_1[4096];
        char AF_UNIX_PATH_2[4096];
        snprintf(AF_UNIX_PATH_1, 4096, "%s", "WL_POSIX__SOCKPAIR_0__XXXXXX");
        snprintf(AF_UNIX_PATH_2, 4096, "%s", "WL_POSIX__SOCKPAIR_1__XXXXXX");

        struct sockaddr_un address[2];
        memset(&(address[0]), 0, sizeof(struct sockaddr_un));
        memset(&(address[1]), 0, sizeof(struct sockaddr_un));
        address[0].sun_family = AF_UNIX;
        address[1].sun_family = AF_UNIX;

        {
            char* template_ = AF_UNIX_PATH_1;
            char* XXXXXX;
            unsigned int i;
            int save_errno = errno;
            unsigned char randomness[6];
            size_t len;
            int suffixlen = 0; // always zero

            len = strlen(template_);
            /* This is where the Xs start.  */
            XXXXXX = template_ + len - 6 - suffixlen;
            if (len < 6 || suffixlen < 0 || suffixlen > len - 6
                || strncmp(XXXXXX, "XXXXXX", 6))
            {
                closesocket(pair[0]);
                closesocket(pair[1]);
                errno = EINVAL;
                return -1;
            }

#ifndef TMP_MAX
#define TMP_MAX 238328
#endif
            bool found = false;
            for (i = 0; i < TMP_MAX; ++i) {
                unsigned char j;
                /* Get some random data.  */
                int rb = randombytes(randomness, sizeof(randomness));
                if (rb != sizeof(randomness)) {
                    /* if random device nodes failed us, lets use the braindamaged ver */
                    brain_damaged_fillrand(randomness, sizeof(randomness));
                }
                for (j = 0; j < sizeof(randomness); ++j)
                    XXXXXX[j] = letters[randomness[j] % NUM_LETTERS];

                char tmp_path[MAX_PATH];
                DWORD rp = GetTempPathA(MAX_PATH, tmp_path);
                if (!(rp > MAX_PATH || rp == 0)) {
                    snprintf(address[0].sun_path, sizeof(address[0].sun_path), "%s%s", tmp_path, template_);

                    error = bind(pair[0], (struct sockaddr*)&(address[0]), sizeof(address[0]));
                    bool r = check_error("bind 0", error);
                    if (r) {
                        if (WSAGetLastError() != WS_E_ADDRESS_IN_USE) {
                            /* Any other error will apply also to other names we might
                            try, and there are 2^32 or so of them, so give up now. */
                            closesocket(pair[0]);
                            closesocket(pair[1]);
                            errno = save_errno;
                            return -1;
                        }
                        continue;
                    }
                    // we succeeded in binding the socket
                    found = true;
                    break;
                }
                // we failed to get a temporary path
                closesocket(pair[0]);
                closesocket(pair[1]);
                errno = save_errno;
                return -1;
            }
            if (!found) {
                /* We got out of the loop because we ran out of combinations to try.  */
                closesocket(pair[0]);
                closesocket(pair[1]);
                errno = EEXIST;
                return -1;
            }
        }

        {
            char* template_ = AF_UNIX_PATH_2;
            char* XXXXXX;
            unsigned int i;
            int save_errno = errno;
            unsigned char randomness[6];
            size_t len;
            int suffixlen = 0; // always zero

            len = strlen(template_);
            /* This is where the Xs start.  */
            XXXXXX = template_ + len - 6 - suffixlen;
            if (len < 6 || suffixlen < 0 || suffixlen > len - 6
                || strncmp(XXXXXX, "XXXXXX", 6))
            {
                closesocket(pair[0]);
                closesocket(pair[1]);
                DeleteFileA(address[0].sun_path);
                errno = EINVAL;
                return -1;
            }

#ifndef TMP_MAX
#define TMP_MAX 238328
#endif

            bool found = false;
            for (i = 0; i < TMP_MAX; ++i) {
                unsigned char j;
                /* Get some random data.  */
                int rb = randombytes(randomness, sizeof(randomness));
                if (rb != sizeof(randomness)) {
                    /* if random device nodes failed us, lets use the braindamaged ver */
                    brain_damaged_fillrand(randomness, sizeof(randomness));
                }
                for (j = 0; j < sizeof(randomness); ++j)
                    XXXXXX[j] = letters[randomness[j] % NUM_LETTERS];

                char tmp_path[MAX_PATH];
                DWORD rp = GetTempPathA(MAX_PATH, tmp_path);
                if (!(rp > MAX_PATH || rp == 0)) {
                    snprintf(address[1].sun_path, sizeof(address[1].sun_path), "%s%s", tmp_path, template_);
                    memcpy(path_108, address[1].sun_path, sizeof(char)*108);

                    error = bind(pair[1], (struct sockaddr*)&(address[1]), sizeof(address[1]));
                    bool r = check_error("bind 1", error);
                    if (r) {
                        if (WSAGetLastError() != WS_E_ADDRESS_IN_USE) {
                            /* Any other error will apply also to other names we might
                            try, and there are 2^32 or so of them, so give up now. */
                            closesocket(pair[0]);
                            closesocket(pair[1]);
                            DeleteFileA(address[0].sun_path);
                            errno = save_errno;
                            return -1;
                        }
                        continue;
                    }
                    // we succeeded in binding the socket
                    found = true;
                    break;
                }
                // we failed to get a temporary path
                closesocket(pair[0]);
                closesocket(pair[1]);
                DeleteFileA(address[0].sun_path);
                errno = save_errno;
                return -1;
            }
            if (!found) {
                /* We got out of the loop because we ran out of combinations to try.  */
                closesocket(pair[0]);
                closesocket(pair[1]);
                DeleteFileA(address[0].sun_path);
                errno = EEXIST;
                return -1;
            }
        }

        error = listen(pair[0], 1);
        if (check_error("listen 0", error)) {
            closesocket(pair[0]);
            closesocket(pair[1]);
            DeleteFileA(address[0].sun_path);
            DeleteFileA(address[1].sun_path);
            return -1;
        }

        struct SOCKETPAIRACCEPTINFO a = {pair[1], address[0]};

        HANDLE t = CreateThread(NULL, 0, acceptThread, &a, 0, NULL);

        struct sockaddr t_addr;
        int t_addrlen = sizeof(t_addr);

        SOCKET tmp = accept(pair[0], &t_addr, &t_addrlen);
        if (check_error_socket("accept", tmp)) {
            closesocket(pair[0]);
            closesocket(pair[1]);
            DeleteFileA(address[0].sun_path);
            DeleteFileA(address[1].sun_path);
            return -1;
        }

        WaitForSingleObject(t, INFINITE);
        TerminateThread(t, 0);
        CloseHandle(t);
        
        closesocket(pair[0]);
        DeleteFileA(address[0].sun_path);
        pair[0] = tmp;
    }

    sv[0] = pair[0];
    sv[1] = pair[1];

    return 0;
}
#else
static int internal_socketpair(int domain, int type, int protocol, int sv[2]) {
    int pair[2] = { -1, -1 };
    if (socketpair(domain, type, protocol, pair) == 0) {
        sv[0] = pair[0];
        sv[1] = pair[1];
        return 0;
    }
    return -1;
}
#endif

int wl_posix_socketpair(int domain, int type, int protocol, int sv[2]) {
#if _WIN32
    ZERO_NEW(WL_POSIX_SOCKET_T, s1);
    if (s1 == NULL) {
        errno = ENOMEM;
        return -1;
    }
    ZERO_NEW(WL_POSIX_SOCKET_T, s2);
    if (s2 == NULL) {
        free(s1);
        errno = ENOMEM;
        return -1;
    }
    NEW_ARRAY(char, a, 108);
    s1->addr = a;
    SOCKET s[2];
    int ret = internal_socketpair(domain, type, protocol, s, s1->addr);
    if (ret == -1) {
        free(s1);
        free(s2);
        free(a);
        return -1;
    }
    s1->socket = s[0];
    s2->socket = s[1];

    WL_MINIOBJ_CREATE_WITH_OBJ(sockfd1, sock1, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
    WL_MINIOBJ_CREATE_WITH_OBJ(sockfd2, sock2, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
    sock1->data = (uint64_t) s1;
    sock2->data = (uint64_t) s2;
    if (WL_MINIOBJ_HAS_FLAG(type, SOCK_CLOEXEC)) {
        sockfd1 = wl_syscall__set_exec_or_close(sockfd1);
        sockfd2 = wl_syscall__set_exec_or_close(sockfd2);
    }
    sv[0] = sockfd1;
    sv[1] = sockfd2;
    return 0;
#else
    WL_MINIOBJ_CREATE_WITH_OBJ(sockfd1, sock1, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
    WL_MINIOBJ_CREATE_WITH_OBJ(sockfd2, sock2, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
    int s[2];
    int r = internal_socketpair(domain, type, protocol, s);
    sock1->data = (uint64_t) s[0];
    sock2->data = (uint64_t) s[1];
    sv[0] = sockfd1;
    sv[1] = sockfd2;
    return r;
#endif
}

#ifdef _WIN32

typedef enum TIMER_FD__STATE {
    TIMER_FD__RUNNING,
    TIMER_FD__SET_TIME,
    TIMER_FD__STOPPED
} TIMER_FD__STATE;

typedef struct TIMER_FD {
    int fd[2];
    BOOL has_data;

    // timer spec, used for obtaining current time
    struct itimerspec timer_spec;

    // current timer
    HANDLE timer;

    // current timer thread
    HANDLE thread;
    
    BOOL nonblock;
    
    uint64_t expirations;
    BOOL canceled;

    // timer data needed for thread
    LARGE_INTEGER dueTime;
    LP64__LONG period;

    // thread state
    TIMER_FD__STATE state;

    // synchronization
    CRITICAL_SECTION criticalSection;

    HANDLE state_event;
    HANDLE response_event;
    BOOL response;
} TIMER_FD;

#endif

#define WL_POSIX_GET_FUNC(type_, RET) \
wl_posix_get_##type_(int fd) { \
    wl_miniobj* obj = wl_miniobj_get(fd); \
    if ( \
        obj == NULL || \
        obj->version < WL_POSIX_FD_VERSION || \
        obj->type != wl_posix_type_##type_ \
    ) { \
        /* do nothing */ \
        errno = EINVAL; \
        return RET; \
    } \
    return wl_posix_getfast_##type_(obj); \
}


#if _WIN32
#define wl_posix_getfast_fd(obj) (HANDLE) obj->data
HANDLE
#else
#define wl_posix_getfast_fd(obj) (int) obj->data
int
#endif
WL_POSIX_GET_FUNC(fd, WL_POSIX_FD_INVALID_HANDLE_VALUE)

#if _WIN32
#define wl_posix_getfast_epoll(obj) (HANDLE) obj->data
HANDLE
#else
#define wl_posix_getfast_epoll(obj) (int) obj->data
int
#endif
WL_POSIX_GET_FUNC(epoll, WL_POSIX_FD_INVALID_HANDLE_VALUE)

#if _WIN32
#define wl_posix_getfast_socket(obj) (SOCKET) ((WL_POSIX_SOCKET_T*)obj->data)->socket
SOCKET
#else
#define wl_posix_getfast_socket(obj) (int) obj->data
int
#endif
WL_POSIX_GET_FUNC(socket, WL_POSIX_FD_INVALID_SOCKET_VALUE)

#if _WIN32
#define wl_posix_getfast_eventfd(obj) (HANDLE) ((WL_POSIX_EVENTFD_T*)obj->data)->fd
HANDLE
#else
#define wl_posix_getfast_eventfd(obj) (int) obj->data
int
#endif
WL_POSIX_GET_FUNC(eventfd, WL_POSIX_FD_INVALID_HANDLE_VALUE)

#if _WIN32
#define wl_posix_getfast_temporary_file(obj) (HANDLE) ((WL_POSIX_TMP_FILE_T*)obj->data)->fd
HANDLE
#else
#define wl_posix_getfast_temporary_file(obj) (int) obj->data
int
#endif
WL_POSIX_GET_FUNC(temporary_file, WL_POSIX_FD_INVALID_HANDLE_VALUE)

#if _WIN32
#define wl_posix_getfast_timerfd(obj) (HANDLE) ((TIMER_FD*) obj->data)->timer
HANDLE
#else
#define wl_posix_getfast_timerfd(obj) (int) obj->data
int
#endif
WL_POSIX_GET_FUNC(timerfd, WL_POSIX_FD_INVALID_HANDLE_VALUE)

#define wl_posix_switchget(type) case wl_posix_type_##type: { wl_posix_getfast_##type(obj); break; }

static
#if _WIN32
HANDLE
#else
int
#endif
wl_posix_get_handle(wl_miniobj* obj) {
    switch(obj->type) {
        wl_posix_switchget(socket)
        wl_posix_switchget(eventfd)
        wl_posix_switchget(fd)
        wl_posix_switchget(temporary_file)
        wl_posix_switchget(epoll)
        wl_posix_switchget(timerfd)
    };
    errno = EINVAL;
    return WL_POSIX_FD_INVALID_HANDLE_VALUE;
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_set_exec_or_close, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        !(
            obj->type == wl_posix_type_socket
            || obj->type == wl_posix_type_eventfd
            || obj->type == wl_posix_type_fd
            || obj->type == wl_posix_type_temporary_file
            || obj->type == wl_posix_type_epoll
            || obj->type == wl_posix_type_timerfd
        )
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

#if _WIN32
    HANDLE h = wl_posix_get_handle(obj);
    if (obj->type == wl_posix_type_socket ? (SOCKET) h == WL_POSIX_FD_INVALID_SOCKET_VALUE : h == WL_POSIX_FD_INVALID_HANDLE_VALUE)
        return fd;

    DWORD flags;
    if (!GetHandleInformation(h, &flags))
        goto err;
    
    if (!SetHandleInformation(h, flags | HANDLE_FLAG_INHERIT, 0))
        goto err;
#else
    int fd_ = wl_posix_getfast_fd(obj);
    if (fd_ == -1)
        return fd;
    
    long flags;

    flags = fcntl(fd_, F_GETFD);
    if (flags == -1)
        goto err;

    if (fcntl(fd_, F_SETFD, flags | FD_CLOEXEC) == -1)
        goto err;
#endif
    return fd;

err:
    wl_syscall__close(fd);
    return -1;
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_socket_accept4, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_socket
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    WL_POSIX_SOCK_T s = wl_posix_getfast_socket(obj);
    if (s == WL_POSIX_FD_INVALID_SOCKET_VALUE)
        return fd;

    if (argc != 3) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(struct sockaddr *, addr, argv);
    WL_ARGV(socklen_t *, addrlen, argv);
    WL_ARGV(int, flags, argv);

    // if flags are zero, this is equivilant to accept()
    
#ifdef _WIN32
    if (WL_MINIOBJ_HAS_INVALID_FLAGS(flags, SOCK_NONBLOCK | SOCK_CLOEXEC)) {
        errno = EINVAL;
        return -1;
    }

    SOCKET hs = accept(s, addr, addrlen);
    if (hs == INVALID_SOCKET) {
        return -1;
    }

    if (flags != 0) {
        u_long mode = WL_MINIOBJ_HAS_FLAG(flags, SOCK_NONBLOCK);
        ioctlsocket(hs, FIONBIO, &mode);
    }

    ZERO_NEW(WL_POSIX_SOCKET_T, st);

    if (st == NULL) {
        closesocket(hs);
        return -1;
    }
    
    WL_MINIOBJ_CREATE_WITH_OBJ(sockfd, sock, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
    sock->data = (uint64_t) st;
    if (WL_MINIOBJ_HAS_FLAG(flags, SOCK_CLOEXEC)) {
        return wl_syscall__set_exec_or_close(sockfd);
    }
    return sockfd;
#else
    int fd;
#ifdef HAVE_ACCEPT4
    fd = accept4(s, addr, addrlen, flags);
    if (fd >= 0) {
        WL_MINIOBJ_CREATE_WITH_OBJ(sockfd, sock, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
        sock->data = (uint64_t) fd;
        return sockfd;
    }
    if (errno != ENOSYS) {
        return -1;
    }
#endif
    fd = accept(s, addr, addrlen);
    if (fd >= 0) {
        WL_MINIOBJ_CREATE_WITH_OBJ(sockfd, sock, WL_POSIX_FD_VERSION, wl_posix_type_socket, true);
        sock->data = (uint64_t) fd;
        if (WL_MINIOBJ_HAS_FLAG(flags, SOCK_CLOEXEC)) {
            return wl_syscall__set_exec_or_close(sockfd);
        }
        return sockfd;
    }
    return -1;
#endif
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_socket_connect, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_socket
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    WL_POSIX_SOCK_T s = wl_posix_getfast_socket(obj);
    if (s == WL_POSIX_FD_INVALID_SOCKET_VALUE)
        return fd;

    if (argc != 2) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(struct sockaddr *, addr, argv);
    WL_ARGV(socklen_t, addrlen, argv);

    return connect(s, addr, addrlen);
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_socket_bind, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_socket
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    WL_POSIX_SOCKET_T* s = (WL_POSIX_SOCKET_T*)obj->data;
    if (s->socket == WL_POSIX_FD_INVALID_SOCKET_VALUE)
        return fd;

    if (argc != 2) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(struct sockaddr *, addr, argv);
    WL_ARGV(socklen_t, addrlen, argv);

    /*
    // do not auto unlink for now

    if (addr->sa_family == AF_UNIX) {
        char * p = ((struct sockaddr_un*)addr)->sun_path;
        if (p[0] != '\0') {
            if (s->addr == NULL) {
                NEW_ARRAY(char, a, 108);
                s->addr = a;
            }
            memcpy(s->addr, p, sizeof(char)*108);
        }
    }
    */

    return bind(s->socket, addr, addrlen);
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_socket_listen, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_socket
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    WL_POSIX_SOCK_T s = wl_posix_getfast_socket(obj);
    if (s == WL_POSIX_FD_INVALID_SOCKET_VALUE)
        return fd;

    if (argc != 1) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(int, backlog, argv);

    return listen(s, backlog);
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_socket_send, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_socket
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    WL_POSIX_SOCK_T s = wl_posix_getfast_socket(obj);
    if (s == WL_POSIX_FD_INVALID_SOCKET_VALUE)
        return fd;

    if (argc != 3) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(const void*, buf, argv);
    WL_ARGV(size_t, len, argv);
    WL_ARGV(int, flags, argv);

#ifdef _WIN32
    if (flags == 0) return send(s, buf, len, 0);
    u_long mode = WL_MINIOBJ_HAS_FLAG(flags, MSG_DONTWAIT);
    ioctlsocket(s, FIONBIO, &mode);
    int len_ = send(s, buf, len, 0);
    if (WL_MINIOBJ_HAS_FLAG(flags, MSG_NOSIGNAL)) {
        if (GetLastError() == ERROR_BROKEN_PIPE) {
            errno = EPIPE;
        }
    }
    return len_;
#else
    return send(s, buf, len, flags);
#endif
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_socket_recv, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_socket
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    WL_POSIX_SOCK_T s = wl_posix_getfast_socket(obj);
    if (s == WL_POSIX_FD_INVALID_SOCKET_VALUE)
        return fd;

    if (argc != 3) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(void*, buf, argv);
    WL_ARGV(size_t, len, argv);
    WL_ARGV(int, flags, argv);

#ifdef _WIN32
    if (flags == 0) return recv(s, buf, len, 0);
    u_long mode = WL_MINIOBJ_HAS_FLAG(flags, MSG_DONTWAIT);
    ioctlsocket(s, FIONBIO, &mode);
    int len_ = recv(s, buf, len, 0);
    if (WL_MINIOBJ_HAS_FLAG(flags, MSG_NOSIGNAL)) {
        if (GetLastError() == ERROR_BROKEN_PIPE) {
            errno = EPIPE;
        }
    }
    return len_;
#else
    return recv(s, buf, len, flags);
#endif
}

#define COPY_TO_BUF(buf, obj_type, obj) memcpy(buf, obj, sizeof(obj_type)); buf += sizeof(obj_type)
#define COPY_FROM_BUF(buf, obj_type, obj) memcpy(obj, buf, sizeof(obj_type)); buf += sizeof(obj_type)

#ifdef _WIN32
static void store_buffer_socket(void** buffer, size_t * buffer_len, wl_miniobj * obj) {
    *buffer_len = sizeof(uint16_t)*2;
    *buffer_len += sizeof(WSAPROTOCOL_INFO);
    *buffer = malloc(*buffer_len);
    char * tmp = *buffer;
    COPY_TO_BUF(tmp, uint16_t, &obj->type);
    COPY_TO_BUF(tmp, uint16_t, &obj->version);
    COPY_TO_BUF(tmp, WSAPROTOCOL_INFO, (WSAPROTOCOL_INFO*)obj->data);
}

static void store_buffer_handle(void** buffer, size_t * buffer_len, wl_miniobj * obj) {
    *buffer_len = sizeof(uint16_t)*2;
    *buffer_len += sizeof(HANDLE);
    *buffer = malloc(*buffer_len);
    char * tmp = *buffer;
    COPY_TO_BUF(tmp, uint16_t, &obj->type);
    COPY_TO_BUF(tmp, uint16_t, &obj->version);
    COPY_TO_BUF(tmp, HANDLE, (HANDLE*)obj->data);
}
#else
static void store_buffer_fd(void** buffer, size_t * buffer_len, wl_miniobj * obj) {
    *buffer_len = sizeof(uint16_t)*2;
    *buffer_len += sizeof(uint32_t);
    *buffer = malloc(*buffer_len);
    char * tmp = *buffer;
    COPY_TO_BUF(tmp, uint16_t, &obj->type);
    COPY_TO_BUF(tmp, uint16_t, &obj->version);
    COPY_TO_BUF(tmp, uint32_t, &obj->data);
}
#endif

void wl_posix_fd_transfer__add(wl_posix_fd_struct* s, wl_miniobj * obj) {
    if (s->first == NULL) {
        s->first = calloc(1, sizeof(wl_posix_fd_linked_list));
        s->last = s->first;
    } else {
        s->last->next = calloc(1, sizeof(wl_posix_fd_linked_list));
        s->last = s->last->next;
    }
#ifdef _WIN32
    if (obj->type == wl_posix_type_socket) {
        store_buffer_socket(&s->last->buffer, &s->last->buffer_len, obj);
    } else {
        store_buffer_handle(&s->last->buffer, &s->last->buffer_len, obj);
    }
#else
    store_buffer_fd(&s->last->buffer, &s->last->buffer_len, obj);
#endif
    s->count++;
}

wl_posix_fd_struct * wl_posix_fd_transfer__prepare(void) {
    wl_posix_fd_struct * data = (wl_posix_fd_struct *) calloc(1, sizeof(wl_posix_fd_struct));
    data->data = (wl_posix_transfer*) calloc(1, sizeof(wl_posix_transfer));

    //wl_posix_transfer* transfer = (wl_posix_transfer*) data->data;
    return data;
}

#ifndef _WIN32

#define SEND_MSG(fd, msg, data) \
do { \
    len = sendmsg(fd, msg, MSG_NOSIGNAL | MSG_DONTWAIT); \
} while (len == -1 && errno == EINTR); \
free(data); \
if (len == -1) { \
    return -1; \
}

#define RECV_MSG(fd, msg) \
do { \
    len = recvmsg(fd, msg, MSG_NOSIGNAL | MSG_DONTWAIT); \
} while (len == -1 && errno == EINTR); \
if (len == -1) { \
    return -1; \
}



#define SEND_FD(fd, fd_, iov, iov_len, CLEN) \
{ \
    struct msghdr msg = {0}; \
    size_t clen; \
    NEW_ARRAY(char, data, CLEN); \
    struct cmsghdr *cmsg = (struct cmsghdr *) data; \
    cmsg->cmsg_level = SOL_SOCKET; \
    cmsg->cmsg_type = SCM_RIGHTS; \
    memcpy(CMSG_DATA(cmsg), fd_, sizeof(uint32_t)); \
    cmsg->cmsg_len = CMSG_LEN(1); \
    clen = cmsg->cmsg_len; \
    msg.msg_iov = iov; \
    msg.msg_iovlen = iov_len; \
    msg.msg_control = (clen > 0) ? cmsg : NULL; \
    msg.msg_controllen = clen; \
    SEND_MSG(fd, msg, data); \
}


#define RECV_FD(fd, fd_) \
{
    struct msghdr msg = {0}; \
    RECV_MSG(fd, &msg); \
    struct cmsghdr *cmsg; \
    size_t size; \
    for (cmsg = CMSG_FIRSTHDR(msg); cmsg != NULL; \
         cmsg = CMSG_NXTHDR(msg, cmsg)) { \
        if (cmsg->cmsg_level != SOL_SOCKET || \
            cmsg->cmsg_type != SCM_RIGHTS) \
            continue; \
        size = cmsg->cmsg_len - CMSG_LEN(0); \
        memcpy(fd_, CMSG_DATA(cmsg), sizeof(uint32_t)); \
        break; /* we only get 1 fd */
    }
}

#endif

int wl_posix_fd_transfer__send(wl_posix_fd_struct* s, int socket_fd, struct iovec* iov, int iov_len, size_t CLEN) {
    int len = 0;
    wl_posix_transfer* transfer = s->data;

#ifdef _WIN32
    if (!transfer->sent_iovec) {
        if (!transfer->have_count) {
            transfer->iovec_count = iov_len;
            transfer->iovec_current_count = iov_len;
            transfer->have_count = true;
        }
        if (!transfer->sent_iov_count) {
            WL_SEND_DATA(socket_fd, &iov_len, sizeof(int));
            transfer->sent_iov_count = true;
        }
        while(transfer->iovec_current_count != 0) {
            if (!transfer->sent_iov_base) {
                WL_SEND_DATA(socket_fd, iov[transfer->iovec_current_count].iov_base, iov[transfer->iovec_current_count].iov_len);
                transfer->sent_iov_base = true;
            }
            transfer->iovec_current_count--;
            transfer->sent_iov_base = false;
        }
        transfer->sent_iovec = true;
    }
    if (!transfer->sent_count) {
        WL_SEND_DATA(socket_fd, &s->count, sizeof(int));
        transfer->sent_count = true;
        transfer->current = s->first;
    }
    WL_SEND_DATA(socket_fd, transfer->current->buffer, transfer->current->buffer_len);
#else
    if (!transfer->sent_count) {
        WL_SEND_DATA(socket_fd, &s->count, sizeof(int));
        transfer->sent_count = true;
        transfer->current = s->first;
    }

    uint16_t type, version;
    uint32_t handle;
    
    COPY_FROM_BUF(transfer->current->buffer, uint16_t, &type);
    COPY_FROM_BUF(transfer->current->buffer, uint16_t, &version);
    COPY_FROM_BUF(transfer->current->buffer, uint32_t, handle);

    WL_SEND_DATA(socket_fd, &type, sizeof(uint16_t));
    WL_SEND_DATA(socket_fd, &version, sizeof(uint16_t));
    SEND_FD(socket_fd, handle, iov, iov_len, CLEN);
#endif
    transfer->current = transfer->current->next;
    transfer->complete = transfer->current == NULL;
    return len;
}

int wl_posix_fd_transfer__recv(wl_posix_fd_struct* s, int socket_fd, struct iovec* iov, int iov_len, size_t CLEN) {
    int len = 0;
    wl_posix_transfer* transfer = s->data;
#ifdef _WIN32
    if (!transfer->recv_iovec) {
        if (!transfer->recv_iov_count) {
            WL_RECV_DATA(socket_fd, &transfer->iovec_count, sizeof(int));
            transfer->iovec_current_count = transfer->iovec_count;
            transfer->recv_iov_count = true;
            if (iov_len != transfer->iovec_count) {
                printf("iovec obtained possible incorrect len: given to function = %d, obtained from socket: %d\n", iov_len, transfer->iovec_count);
            }
        }
        while(transfer->iovec_current_count != 0) {
            if (!transfer->recv_iov_base) {
                WL_RECV_DATA(socket_fd, iov[transfer->iovec_current_count].iov_base, iov[transfer->iovec_current_count].iov_len);
                transfer->recv_iov_base = true;
            }
            transfer->iovec_current_count--;
            transfer->recv_iov_base = false;
        }
        transfer->recv_iovec = true;
    }

    if (!transfer->recv_count) {
        WL_RECV_DATA(socket_fd, &transfer->count, sizeof(int));
        transfer->recv_count = true;
        transfer->buf = malloc(sizeof(wl_miniobj)*transfer->count);
        transfer->buf_start = transfer->buf;
    }

    
    wl_miniobj obj;

    WL_RECV_DATA(socket_fd, &obj.type, sizeof(uint16_t));
    WL_RECV_DATA(socket_fd, &obj.version, sizeof(uint16_t));

    if (obj.type == wl_posix_type_socket) {
        WSAPROTOCOL_INFO p;
        WL_RECV_DATA(socket_fd, &p, sizeof(WSAPROTOCOL_INFO));
        obj.data = (SOCKET) WSASocket(
            FROM_PROTOCOL_INFO, FROM_PROTOCOL_INFO,
            FROM_PROTOCOL_INFO, &p, 0, 0
        );
    } else {
        HANDLE h = NULL;
        WL_RECV_DATA(socket_fd, &h, sizeof(HANDLE));
        obj.data = (uint64_t) h;
    }

    COPY_TO_BUF(transfer->buf, wl_miniobj, &obj);

    transfer->count--;
    transfer->complete = transfer->count <= 0;
#else
    if (!transfer->recv_count) {
        WL_RECV_DATA(socket_fd, &transfer->count, sizeof(int));
        transfer->recv_count = true;
        transfer->buf = malloc(sizeof(wl_miniobj)*transfer->count);
        transfer->buf_start = transfer->buf;
    }

    wl_miniobj obj;

    WL_RECV_DATA(socket_fd, &obj.type, sizeof(uint16_t));
    WL_RECV_DATA(socket_fd, &obj.version, sizeof(uint16_t));

    uint32_t h;
    RECV_FD(socket_fd, &h);
    obj.data = (uint64_t) h;

    COPY_TO_BUF(transfer->buf, wl_miniobj, &obj);

    transfer->count--;
    transfer->complete = transfer->count <= 0;
#endif
    return 0;
}

void wl_posix_fd_transfer__delete(wl_posix_fd_struct* s) {
    while (s->first != NULL) {
        s->last = s->first->next;
        free(s->first->buffer);
        free(s->first);
        s->first = s->last;
    }
    wl_posix_transfer* transfer = s->data;
    free(transfer->buf);
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_socket_peercred, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_socket
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    WL_POSIX_SOCK_T s = wl_posix_getfast_socket(obj);
    if (s == WL_POSIX_FD_INVALID_SOCKET_VALUE)
        return fd;

    if (argc != 3) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(uid_t*, uid, argv);
    WL_ARGV(gid_t*, gid, argv);
    WL_ARGV(pid_t*, pid, argv);

#if defined(__FreeBSD__)
    socklen_t len;
    struct xucred ucred;

    len = sizeof(ucred);
    if (getsockopt(s, SOL_LOCAL, LOCAL_PEERCRED, &ucred, &len) < 0 ||
        ucred.cr_version != XUCRED_VERSION)
        return -1;
    if (uid != NULL)
        *uid = ucred.cr_uid;
    if (gid != NULL)
    *gid = ucred.cr_gid;
#if HAVE_XUCRED_CR_PID
    /* Since https://cgit.freebsd.org/src/commit/?id=c5afec6e895a */
    if (pid != NULL)
        *pid = ucred.cr_pid;
#else
    if (pid != NULL)
        *pid = 0;
#endif
    return 0;
#elif defined(SO_PEERCRED)
    socklen_t len;
    struct ucred ucred;

    len = sizeof(ucred);
    if (getsockopt(s, SOL_SOCKET, SO_PEERCRED, &ucred, &len) < 0)
        return -1;
    if (uid != NULL)
        *uid = ucred.uid;
    if (gid != NULL)
        *gid = ucred.gid;
    if (pid != NULL)
        *pid = ucred.pid;
    return 0;
#elif defined(_WIN32)
    SOCKADDR_IN ServerAddr = {0};
    int ServerAddrSize = sizeof(ServerAddr);

    SOCKADDR_IN ClientAddr = {0};
    int ClientAddrSize = sizeof(ClientAddr);

    if ((getsockname(s, (struct sockaddr*)&ServerAddr, &ServerAddrSize) == 0) &&
        (getpeername(s, (struct sockaddr*)&ClientAddr, &ClientAddrSize) == 0))
    {
        PMIB_TCPTABLE2 TcpTable = NULL;
        ULONG TcpTableSize = 0;
        ULONG result;

        do
        {
            result = GetTcpTable2(TcpTable, &TcpTableSize, TRUE);
            if (result != ERROR_INSUFFICIENT_BUFFER)
                break;

            LocalFree(TcpTable);
            TcpTable = (PMIB_TCPTABLE2) LocalAlloc(LMEM_FIXED, TcpTableSize);
        }
        while (TcpTable != NULL);

        if (result == NO_ERROR)
        {
            for (DWORD dw = 0; dw < TcpTable->dwNumEntries; ++dw)
            {
                PMIB_TCPROW2 row = &(TcpTable->table[dw]);

                if ((row->dwState == MIB_TCP_STATE_ESTAB) &&
                    (row->dwLocalAddr == ClientAddr.sin_addr.s_addr) &&
                    ((row->dwLocalPort & 0xFFFF) == ClientAddr.sin_port) &&
                    (row->dwRemoteAddr == ServerAddr.sin_addr.s_addr) &&
                    ((row->dwRemotePort & 0xFFFF) == ServerAddr.sin_port))
                {
                    if (pid != NULL)
                        *pid = row->dwOwningPid;
                    if (uid != NULL || gid != NULL) {
                        HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, *pid);
                        PSECURITY_DESCRIPTOR pSD;
                        uid_t a;
                        gid_t b;
                        GetSecurityInfo(
                            hProcess,
                            SE_KERNEL_OBJECT,
                            OWNER_SECURITY_INFORMATION,
                            &a, &b,
                            NULL, NULL, &pSD
                        );
                        if (uid != NULL)
                            *uid = a;
                        if (gid != NULL)
                            *gid = b;
                        LocalFree(pSD);
                        CloseHandle(hProcess);
                    }
                    LocalFree(TcpTable);
                    return 0;
                }
            }
        }
        LocalFree(TcpTable);
    }
    return -1;
#else
#error "Don't know how to read ucred on this platform"
#endif
}


// if peer is -1 then a new fd is returned
// otherwise an object is returned
// passing 0 for minfd will cause it to have lowest descriptor returned
void* wl_posix_dupfd_for_process(int fd, int peer, int minfd) {

    wl_miniobj * obj = wl_miniobj_get(fd);

    if (obj == NULL) {
        return NULL;
    }

    if (
        obj->version < WL_POSIX_FD_VERSION ||
        !(
            obj->type == wl_posix_type_socket
            || obj->type == wl_posix_type_fd
            || obj->type == wl_posix_type_temporary_file
            || obj->type == wl_posix_type_eventfd
        )
    ) {
        // do nothing
        errno = EINVAL;
        return NULL;
    }

    // if peer is -1 then duplicate for current process

    wl_miniobj * peer_socket = NULL;

    if (peer != -1) {
        peer_socket = wl_miniobj_get(peer);
        if (peer_socket == NULL) {
            return NULL;
        }
        if (
            peer_socket->version < WL_POSIX_FD_VERSION ||
            !(
                peer_socket->type == wl_posix_type_socket
            )
        ) {
            // do nothing
            errno = EINVAL;
            return NULL;
        }
    }

#ifdef _WIN32

    pid_t pid;
    if (peer == -1) {
        pid = GetCurrentProcessId();
    } else {
        wl_syscall__peercred_pid(peer, &pid);
    }

    if (obj->type == wl_posix_type_socket) {
        SOCKET s = wl_posix_getfast_socket(obj);
        if (s == INVALID_SOCKET)
            return NULL;
        
        if (peer == -1) {
            // duplicate for current process
            WSAPROTOCOL_INFO out;
            if (WSADuplicateSocket(s, pid, &out) == SOCKET_ERROR) {
                printf("WSADuplicateSocket(): failed. Error = %d\n", WSAGetLastError());
                return NULL;
            }
            ZERO_NEW(WL_POSIX_SOCKET_T, st);
            if (st == NULL) {
                return NULL;
            }
            st->socket = WSASocket(FROM_PROTOCOL_INFO, FROM_PROTOCOL_INFO, FROM_PROTOCOL_INFO, &out, 0, 0);
            if (st->socket == INVALID_SOCKET) {
                free(st);
                return NULL;
            }
            WL_MINIOBJ_DUPLICATE_WITH_OBJ(sockfd, sock, fd);
            sock->data = (uint64_t) st;
            return (void*)(uint64_t) sockfd;
        } else {
            // duplicate for target process
            NEW(WSAPROTOCOL_INFO, out);
            if (WSADuplicateSocket(s, pid, out) == SOCKET_ERROR) {
                printf("WSADuplicateSocket(): failed. Error = %d\n", WSAGetLastError());
                free(out);
                return NULL;
            }
            return out;
        }
    } else {
        HANDLE h = NULL;
        if (obj->type == wl_posix_type_temporary_file) {
            h = wl_posix_getfast_temporary_file(obj);
        } else {
            h = wl_posix_getfast_fd(obj);
        }
        if (h == INVALID_HANDLE_VALUE)
            return NULL;

        if (peer == -1) {
            // duplicate for current process
            if (obj->type == wl_posix_type_eventfd) {
                return (void*)(uint64_t) wl_miniobj_duplicate(fd);
            } else {
                HANDLE out;
                HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
                if (!DuplicateHandle(
                    GetCurrentProcess(),
                    h,
                    hProcess,
                    &out,
                    DUPLICATE_SAME_ACCESS,
                    FALSE,
                    DUPLICATE_SAME_ACCESS
                )) {
                    printf("DuplicateHandle(): failed. Error = %lu\n", GetLastError());
                    CloseHandle(hProcess);
                    return NULL;
                }
                CloseHandle(hProcess);
                if (obj->type == wl_posix_type_temporary_file) {
                    NEW(WL_POSIX_TMP_FILE_T, tmp);
                    if (tmp == NULL) {
                        CloseHandle(out);
                        return NULL;
                    }

                    WL_POSIX_TMP_FILE_T * tmp_obj = (WL_POSIX_TMP_FILE_T *) obj->data;
                    memcpy(tmp_obj->path, tmp->path, MAX_PATH);
                    tmp_obj->fd = out;

                    WL_MINIOBJ_DUPLICATE_WITH_OBJ(out_fd, out_obj, fd);
                    out_obj->data = (uint64_t) tmp_obj;
                    return (void*)(uint64_t) out_fd;
                } else {
                    WL_MINIOBJ_DUPLICATE_WITH_OBJ(out_fd, out_obj, fd);
                    out_obj->data = (uint64_t) out;
                    return (void*)(uint64_t) out_fd;
                }
            }
        } else {
            // duplicate for target process
            if (obj->type == wl_posix_type_eventfd) {
                    printf("eventfd not supported\n");
                    return NULL;
            }
            NEW(HANDLE, out);
            HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
            if (!DuplicateHandle(
                GetCurrentProcess(),
                h,
                hProcess,
                out,
                DUPLICATE_SAME_ACCESS,
                FALSE,
                DUPLICATE_SAME_ACCESS
            )) {
                printf("DuplicateHandle(): failed. Error = %lu\n", GetLastError());
                free(out);
                CloseHandle(hProcess);
                return NULL;
            }
            CloseHandle(hProcess);
            return out;
        }
    }
#else
    int f = wl_posix_getfast_socket(fd);
    int newfd = fcntl(f, F_DUPFD_CLOEXEC, minfd);
    if (newfd < 0) {
        if (errno != EINVAL) {
            return NULL;
        } else {
            newfd = fcntl(fd, F_DUPFD, minfd);
        }


        // set close exec flag on newfd

        long flags;

        flags = fcntl(f, F_GETFD);
        if (flags == -1) {
            close(newfd);
            return (void*)(uint64_t) -1;
        }

        if (fcntl(f, F_SETFD, flags | FD_CLOEXEC) == -1) {
            close(newfd);
            return (void*)(uint64_t) -1;
        }
    }
    if (peer == -1) {
        // duplicate for current process
        WL_MINIOBJ_DUPLICATE_WITH_OBJ(out_fd, out_obj, fd);
        out_obj->data = (uint64_t) new_fd;
        return (void*)(uint64_t) out_fd;
    } else {
        // duplicate for target process
        return (void*)(uint64_t) new_fd;
    }
#endif
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_dupfd, fd, argc, argv) {
    return (int)(uint64_t) wl_posix_dupfd_for_process(fd, -1, 0);
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_dupfd2, fd, argc, argv) {
    if (argc != 1) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(int, minfd, argv);

    return (int)(uint64_t) wl_posix_dupfd_for_process(fd, -1, minfd);
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_read, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        !(
            obj->type == wl_posix_type_socket
            || obj->type == wl_posix_type_eventfd
            || obj->type == wl_posix_type_fd
            || obj->type == wl_posix_type_temporary_file
            || obj->type == wl_posix_type_timerfd
        )
    ) {
        // do nothing
        errno = EINVAL;
        return (uint64_t) NULL;
    }

    if (argc != 2) {
        errno = EINVAL;
        return (uint64_t) NULL;
    }

    WL_ARGV(void*, buf, argv);
    WL_ARGV(size_t, count, argv);

#ifdef _WIN32
    if (obj->type == wl_posix_type_eventfd) {
        WL_POSIX_EVENTFD_T * fd = (WL_POSIX_EVENTFD_T *)obj->data;
        if (fd->fd == WL_POSIX_FD_INVALID_HANDLE_VALUE) {
            errno = EINVAL;
            return -1;
        }
        if (count < sizeof(uint64_t)) {
            errno = EINVAL;
            return -1;
        }
        EnterCriticalSection(&fd->criticalSection);
        if (fd->counter == 0) {
            if (fd->nonblock) {
                errno = EAGAIN;
                LeaveCriticalSection(&fd->criticalSection);
                return -1;
            }
            WaitForSingleObject(fd->fd, INFINITE);
        }
        if (fd->semaphore) {
            fd->counter--;
            SetEvent(fd->fd); // signal decrement
            LeaveCriticalSection(&fd->criticalSection);
            ((uint64_t*)buf)[0] = 1;
            return 1;
        } else {
            uint64_t ret = fd->counter;
            fd->counter = 0;
            SetEvent(fd->fd); // signal decrement
            LeaveCriticalSection(&fd->criticalSection);
            ((uint64_t*)buf)[0] = ret;
            return ret;
        }
    } else if (obj->type == wl_posix_type_timerfd) {
        TIMER_FD * timer = (TIMER_FD *)obj->data;
        if (timer->timer == WL_POSIX_FD_INVALID_HANDLE_VALUE) {
            errno = EINVAL;
            return -1;
        }
        
        if (count < sizeof(uint64_t)) {
            errno = EINVAL;
            return -1;
        }

        // the man page does not specify wether this is an atomic operation or not
        // we will make this atomic for reliability
        EnterCriticalSection(&timer->criticalSection);
        if (timer->canceled) {
            timer->canceled = FALSE;
            LeaveCriticalSection(&timer->criticalSection);
            errno = ECANCELED;
            return -1;
        }
        
        SOCKET read = wl_posix_getfast_socket(wl_miniobj_get(timer->fd[0]));
        if (timer->nonblock) {
            if (timer->has_data) {
                // we have data, eject it
                u_long mode = 0; // blocking
                ioctlsocket(read, FIONBIO, &mode);

                uint64_t value;

                int len = 0;
                do {
                    len = recv(read, (char*)&value, sizeof(uint64_t), 0);
                } while (len == SOCKET_ERROR && WSAGetLastError() == WSAEINTR);

                if (len == SOCKET_ERROR) {
                    // we failed to eject data
                    LeaveCriticalSection(&timer->criticalSection);
                    return -1;
                }

                // the next read will fail until we have data
                timer->expirations = 0;
                timer->has_data = FALSE;
                LeaveCriticalSection(&timer->criticalSection);
                return value;
            } else {
                // we have no data
                LeaveCriticalSection(&timer->criticalSection);
                errno = EAGAIN;
                return -1;
            }
        } else {
            // wait for data and then eject it

            u_long mode = 0; // blocking
            ioctlsocket(read, FIONBIO, &mode);

            uint64_t value;

            int len = 0;
            do {
                len = recv(read, (char*)&value, sizeof(uint64_t), 0);
            } while (len == SOCKET_ERROR && WSAGetLastError() == WSAEINTR);

            if (len == SOCKET_ERROR) {
                // we failed to eject data
                LeaveCriticalSection(&timer->criticalSection);
                return -1;
            }

            // the next read will fail until we have data
            timer->expirations = 0;
            timer->has_data = FALSE;
            LeaveCriticalSection(&timer->criticalSection);
            return value;
        }
    } else {
        DWORD ret = 0;
        HANDLE h = NULL;
        if (obj->type == wl_posix_type_temporary_file) {
            WL_POSIX_TMP_FILE_T * tmp = (WL_POSIX_TMP_FILE_T *)obj->data;
            h = tmp->fd;
        } else if (obj->type == wl_posix_type_socket) {
            WL_POSIX_SOCKET_T * s = (WL_POSIX_SOCKET_T*) obj->data;
            h = (HANDLE) s->socket;
        } else {
            h = (HANDLE) obj->data;
        }
        // ReadFile specifies it can read any handle passed to it
        if (ReadFile(h, buf, count, &ret, NULL)) {
            return (uint64_t) ret;
        }
        // we got an error, return 0 bytes read
        return 0;
    }
#else
    return read(obj->data, buf, count);
#endif
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_write, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        !(
            obj->type == wl_posix_type_socket
            || obj->type == wl_posix_type_eventfd
            || obj->type == wl_posix_type_fd
            || obj->type == wl_posix_type_temporary_file
        )
    ) {
        // do nothing
        errno = EINVAL;
        return (uint64_t) NULL;
    }

    if (argc != 2) {
        errno = EINVAL;
        return (uint64_t) NULL;
    }

    WL_ARGV(void*, buf, argv);
    WL_ARGV(size_t, count, argv);

#ifdef _WIN32
    if (obj->type == wl_posix_type_eventfd) {
        WL_POSIX_EVENTFD_T * fd = (WL_POSIX_EVENTFD_T *) obj->data;
        if (fd->fd == WL_POSIX_FD_INVALID_HANDLE_VALUE) {
            errno = EINVAL;
            return -1;
        }
        if (count < sizeof(uint64_t)) {
            errno = EINVAL;
            return -1;
        }
        uint64_t val = ((uint64_t*)buf)[0];
        if (val == UINT64_MAX) {
            errno = EINVAL;
            return -1;
        }
        EnterCriticalSection(&fd->criticalSection);
        if ((UINT64_MAX-1) - fd->counter < val) {
            if (fd->nonblock) {
                errno = EAGAIN;
                LeaveCriticalSection(&fd->criticalSection);
                return -1;
            }
            WaitForSingleObject(fd->fd, INFINITE);
        }
        fd->counter += val;
        SetEvent(fd->fd); // signal increment
        LeaveCriticalSection(&fd->criticalSection);
        return 1;
    } else {
        DWORD ret = 0;
        HANDLE h = NULL;
        if (obj->type == wl_posix_type_temporary_file) {
            WL_POSIX_TMP_FILE_T * tmp = (WL_POSIX_TMP_FILE_T *)obj->data;
            h = tmp->fd;
        } else if (obj->type == wl_posix_type_socket) {
            WL_POSIX_SOCKET_T * s = (WL_POSIX_SOCKET_T*) obj->data;
            h = (HANDLE) s->socket;
        } else {
            h = (HANDLE) obj->data;
        }
        // WriteFile specifies it can write to any handle passed to it
        if (WriteFile(h, buf, count, &ret, NULL)) {
            return ret;
        }
        // we got an error, return 0 bytes written
        return 0;
    }
#else
    return write(obj->data, buf, count);
#endif
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_close, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        !(
            obj->type == wl_posix_type_socket
            || obj->type == wl_posix_type_eventfd
            || obj->type == wl_posix_type_fd
            || obj->type == wl_posix_type_temporary_file
            || obj->type == wl_posix_type_epoll
            || obj->type == wl_posix_type_timerfd
        )
    ) {
        // do nothing
        errno = EINVAL;
        return (uint64_t) NULL;
    }
    if (wl_miniobj_should_destroy(fd)) {
#ifdef _WIN32
        if (obj->type == wl_posix_type_temporary_file) {
            WL_POSIX_TMP_FILE_T * tmp = (WL_POSIX_TMP_FILE_T *)obj->data;
            if (tmp->fd != WL_POSIX_FD_INVALID_HANDLE_VALUE) {
                CloseHandle(tmp->fd);
                DeleteFile(tmp->path);
                free(tmp);
            }
        } else if (obj->type == wl_posix_type_socket) {
            WL_POSIX_SOCKET_T * s = (WL_POSIX_SOCKET_T*) obj->data;
            if (s->socket != (uint64_t) WL_POSIX_FD_INVALID_HANDLE_VALUE) {
                if (s->last_epoll != NULL) {
                    epoll_ctl(s->last_epoll, EPOLL_CTL_DEL, s->socket, NULL);
                }
                closesocket(s->socket);

                if (s->addr != NULL) {
                    if (s->addr[0] != '\0') {
                        _unlink(s->addr);
                    }
                    free(s->addr);
                }
                free(s);
            }
        } else if (obj->type == wl_posix_type_eventfd) {
            WL_POSIX_EVENTFD_T * fd = (WL_POSIX_EVENTFD_T *) obj->data;
            DeleteCriticalSection(&fd->criticalSection);
            if (fd->fd != WL_POSIX_FD_INVALID_HANDLE_VALUE) {
                CloseHandle(fd->fd);
            }
            free(fd);
        } else if (obj->type == wl_posix_type_epoll) {
            if (obj->data != (uint64_t) WL_POSIX_FD_INVALID_HANDLE_VALUE) {
                epoll_close((HANDLE)obj->data);
            }
        } else if (obj->type == wl_posix_type_timerfd) {
            TIMER_FD* timer = (TIMER_FD*)obj->data;
            TerminateThread(timer->thread, 0);
            DeleteCriticalSection(&timer->criticalSection);
            CloseHandle(timer->state_event);
            CloseHandle(timer->response_event);
            wl_syscall__close(timer->fd[0]);
            wl_syscall__close(timer->fd[1]);
            if (timer->timer != WL_POSIX_FD_INVALID_HANDLE_VALUE) {
                CloseHandle(timer->timer);
            }
            free(timer);
        } else {
            if (obj->data != (uint64_t) WL_POSIX_FD_INVALID_HANDLE_VALUE) {
                CloseHandle((HANDLE)obj->data);
            }
        }
#else
    if (obj->type == wl_posix_type_temporary_file) {
        delete(obj->data);
    }
    if (obj->data != WL_POSIX_FD_INVALID_HANDLE_VALUE) {
        close(obj->data);
    }
#endif
    }
    wl_miniobj_destroy(fd, obj);
    return -1;
}




WL_MINIOBJ__DECL_CALLBACK(_wl_posix_epoll_ctl, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_epoll
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    if (obj->data == (uint64_t) WL_POSIX_FD_INVALID_HANDLE_VALUE)
        return -1;

    if (argc != 3) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(int, op, argv);
    WL_ARGV(int, socket, argv);
    WL_ARGV(struct epoll_event*, events, argv);

    wl_miniobj * socket_ = wl_miniobj_get(socket);
    if (
        socket_->version < WL_POSIX_FD_VERSION ||
        socket_->type != wl_posix_type_socket
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

#ifdef _WIN32
    WL_POSIX_SOCKET_T * s = (WL_POSIX_SOCKET_T*) socket_->data;
    if (op == EPOLL_CTL_DEL) {
        s->last_epoll = NULL;
    } else {
        s->last_epoll = obj;
    }
    return epoll_ctl((HANDLE)obj->data, op, s->socket, events);
#else
    return epoll_ctl(obj->data, op, wl_posix_getfast_socket(socket), events);
#endif
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_epoll_wait, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_epoll
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    if (obj->data == (uint64_t) WL_POSIX_FD_INVALID_HANDLE_VALUE)
        return -1;

    if (argc != 3) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(struct epoll_event*, events, argv);
    WL_ARGV(int, maxevents, argv);
    WL_ARGV(int, timeout, argv);

    return epoll_wait((HANDLE)obj->data, events, maxevents, timeout);
}

#ifdef _WIN32

static VOID CALLBACK TimerAPCProc(
   LPVOID lpArg,               // Data value
   DWORD dwTimerLowValue,      // Timer low value
   DWORD dwTimerHighValue )    // Timer high value

{
    // Formal parameters not used in this example.
    UNREFERENCED_PARAMETER(dwTimerLowValue);
    UNREFERENCED_PARAMETER(dwTimerHighValue);

    TIMER_FD* timer = (TIMER_FD*) lpArg;

    // the man page does not specify wether this is an atomic operation or not
    // we will make this atomic for reliability
    EnterCriticalSection(&timer->criticalSection);
    timer->expirations++;

    if (timer->has_data) {
        // we have data, eject it
        SOCKET read = wl_posix_getfast_socket(wl_miniobj_get(timer->fd[0]));
        u_long mode = 0; // blocking
        ioctlsocket(read, FIONBIO, &mode);

        uint64_t value;

        int len = 0;
        do {
            len = recv(read, (char*)&value, sizeof(uint64_t), 0);
        } while (len == SOCKET_ERROR && WSAGetLastError() == WSAEINTR);

        if (len == SOCKET_ERROR) {
            // we failed to eject data
            LeaveCriticalSection(&timer->criticalSection);
            return;
        }
    }

    // we have no data, inject it
    SOCKET write = wl_posix_getfast_socket(wl_miniobj_get(timer->fd[1]));
    u_long mode = 0; // blocking
    ioctlsocket(write, FIONBIO, &mode);

    int len = 0;
    do {
        len = send(write, (char*)&timer->expirations, sizeof(uint64_t), 0);
    } while (len == SOCKET_ERROR && WSAGetLastError() == WSAEINTR);

    if (len == SOCKET_ERROR) {
        // we failed to inject data
        LeaveCriticalSection(&timer->criticalSection);
        return;
    }

    timer->has_data = TRUE;

    LeaveCriticalSection(&timer->criticalSection);
}

static DWORD WINAPI ConsumerThreadProc (PVOID p)
{
    TIMER_FD* timer = (TIMER_FD*) p;

    SetEvent(timer->response_event);
    
    while(timer->state == TIMER_FD__RUNNING)
    {
        // block calling thread to prevent it modifying data while we are reading
        EnterCriticalSection(&timer->criticalSection);
        if (timer->state == TIMER_FD__SET_TIME) {

            /*
                When you are using a waitable timer with an APC, the thread that sets the timer should not wait on the handle of the timer.
                By doing so, you would cause the thread to wake up as a result of the timer becoming signaled rather than as the result of an entry being added to the APC queue.
                As a result, the thread is no longer in an alertable state and the completion routine is not called.
            */
            /*
                Timers are initially inactive.
                To activate a timer, call SetWaitableTimer.
                If the timer is already active when you call SetWaitableTimer, the timer is stopped, then it is reactivated.
                Stopping the timer in this manner does not set the timer state to signaled, so threads blocked in a wait operation on the timer remain blocked.
                However, it does cancel any pending completion routines.

                The thread that set the timer calls the completion routine when it enters an alertable wait state.
                If the timer is set before the thread enters an alertable wait state, the APC is canceled.

                If the system time is adjusted, the due time of any outstanding absolute timers is adjusted.

                APIs that deal with timers use various different hardware clocks.
                These clocks may have resolutions significantly different from what you expect:
                  some may be measured in milliseconds (for those that use an RTC-based timer chip), to those measured in nanoseconds (for those that use ACPI or TSC counters).

                You can change the resolution of your API with a call to the timeBeginPeriod and timeEndPeriod functions.
                How precise you can change the resolution depends on which hardware clock the particular API uses.
                For more information, check your hardware documentation.
            */
            timer->response = SetWaitableTimer(timer->timer, &timer->dueTime, timer->period, TimerAPCProc, timer, FALSE);
            timer->state = TIMER_FD__RUNNING;
            SetEvent(timer->response_event);
        }
        LeaveCriticalSection(&timer->criticalSection);
        WaitForSingleObjectEx(timer->state_event, INFINITE, TRUE);
    }
    return 0;
}

#endif

// a timerfd object MUST be selectable and pollable
//
// thus it MUST be backed by a socket
//
// a timerfd also ONLY supports read()
//
// passing to write would assumable generate EINVAL
//
int wl_posix_create_timerfd(int clockid, int flags) {
#ifdef _WIN32
    if (WL_MINIOBJ_HAS_INVALID_FLAGS(flags, TFD_NONBLOCK | TFD_CLOEXEC)) {
        errno = EINVAL;
        return -1;
    }

    HANDLE timer = CreateWaitableTimer(
               // If lpTimerAttributes is NULL, the timer object gets a default security descriptor and the handle cannot be inherited
            //
             // A process created by the CreateProcess function can inherit a handle to a timer object
            //  if the lpTimerAttributes parameter of CreateWaitableTimer enables inheritance.
            //
            NULL,                   // Default security attributes
            FALSE,                  // Create auto-reset timer

            //If lpTimerName is NULL, the timer object is created without a name.
            //
            //If lpTimerName matches the name of an existing event, semaphore, mutex, job, or file-mapping object,
            //  the function fails and GetLastError returns ERROR_INVALID_HANDLE.
            //  This occurs because these objects share the same namespace.
            //
            NULL
    );

    // If the function succeeds, the return value is a handle to the timer object.
    //
    // If the named timer object exists before the function call
    //   the function returns a handle to the existing object and GetLastError returns ERROR_ALREADY_EXISTS.
    //
    // If the function fails, the return value is NULL. To get extended error information, call GetLastError.
    //
    if (timer == NULL) {
        return -1;
    }

    if (GetLastError() == ERROR_ALREADY_EXISTS) {
        errno = EEXIST;
        return -1;
    }

    ZERO_NEW(TIMER_FD, timer_);

    if (timer_ == NULL) {
        errno = ENOMEM;
        return -1;
    }

    if (wl_posix_socketpair(AF_LOCAL, SOCK_STREAM, 0, timer_->fd) == -1) {
        return -1;
    }

    timer_->timer = timer;
    InitializeCriticalSectionAndSpinCount(&timer_->criticalSection, 4000);

    timer_->state_event = CreateEventW(NULL, TRUE, TRUE, NULL);
    if (timer_->state_event == WL_POSIX_FD_INVALID_HANDLE_VALUE) {
        DeleteCriticalSection(&timer_->criticalSection);
        free(timer_);
        errno = EINVAL;
        return -1;
    }
    timer_->response_event = CreateEventW(NULL, TRUE, TRUE, NULL);
    if (timer_->response_event == WL_POSIX_FD_INVALID_HANDLE_VALUE) {
        DeleteCriticalSection(&timer_->criticalSection);
        CloseHandle(timer_->state_event);
        free(timer_);
        errno = EINVAL;
        return -1;
    }
    timer_->state = TIMER_FD__RUNNING;
    timer_->nonblock = WL_MINIOBJ_HAS_FLAG(flags, TFD_NONBLOCK);

    timer_->thread = CreateThread(NULL, 0, ConsumerThreadProc, timer_, 0, NULL);
    if (timer_->thread == NULL) {
        DeleteCriticalSection(&timer_->criticalSection);
        CloseHandle(timer_->state_event);
        CloseHandle(timer_->response_event);
        free(timer_);
        errno = ENOMEM;
        return -1;
    }

    WaitForSingleObject(timer_->response_event, INFINITE);

    WL_MINIOBJ_CREATE_WITH_OBJ(timerfd, timer_obj, WL_POSIX_FD_VERSION, wl_posix_type_timerfd, false);
    timer_obj->data = (uint64_t) timer_;
    if (WL_MINIOBJ_HAS_FLAG(flags, TFD_CLOEXEC)) {
        return wl_syscall__set_exec_or_close(timerfd);
    }
    return timerfd;
#else
    int fd = timerfd_create(clockid, flags);
    if (fd >= 0) {
        WL_MINIOBJ_CREATE_WITH_OBJ(timerfd, timer_obj, WL_POSIX_FD_VERSION, wl_posix_type_timerfd, true);
        timer_obj->data = (uint64_t) fd;
        return timerfd;
    }
#endif
    return -1;
}

WL_MINIOBJ__DECL_CALLBACK(_wl_posix_timerfd_settime, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_timerfd
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    if (argc != 3) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(int, flags, argv);
    WL_ARGV(const struct itimerspec*, newvalue, argv);
    WL_ARGV(struct itimerspec*, oldvalue, argv);


#if _WIN32
    if (WL_MINIOBJ_HAS_INVALID_FLAGS(flags, TFD_TIMER_ABSTIME | TFD_TIMER_CANCEL_ON_SET) || newvalue == NULL) {
        errno = EINVAL;
        return -1;
    }

    TIMER_FD* timer = (TIMER_FD*)obj->data;

    // windows duetime format:
    // sec , microsec , millisec , nanosec (100th)
    //
    // the standards seem to define nanosec precision (struct) as 1 (1000 nanoseconds == 1 microsecond)
    // windows defines it (struct) for timers as 100 (10 nanoseconds == 1 microsecond)
    //
    // to remain compatible with posix, we shall assume 1 nanosec precision storage but truncate to 100 precision when passing functions that expect 100 nenosec precision such as windows timer api's
    //

    LP64__LONG initial_nanosec = timer->timer_spec.it_value.tv_nsec;
    time_64_t initial_sec = timer->timer_spec.it_value.tv_sec;

    LP64__LONG period_nanosec = timer->timer_spec.it_interval.tv_nsec;
    time_64_t period_sec = timer->timer_spec.it_interval.tv_sec;

    int64_t qwDueTime;
    LP64__LONG period_milliseconds;

    // add sec to nanosec
    qwDueTime = initial_sec * 10000000; // sec -> sec,mill_mill_mill,micr_micr_micr,ns

    // truncate to 100th precision for windows
    // 0 + 50 > 50 / 100 > 0
    // 25 + 50 > 75 / 100 > 0
    // 50 + 50 > 100 / 100 > 1
    // 75 + 50 > 125 / 100 > 1
    // 100 + 50 > 150 / 100 > 1
    // 125 + 50 > 175 / 100 > 1
    // 150 + 50 > 200 / 100 > 2
    // 175 + 50 > 225 / 100 > 2
    // 200 + 50 > 250 / 100 > 2
    // 225 + 50 > 275 / 100 > 2
    initial_nanosec += 50;
    initial_nanosec /= 100;
    qwDueTime += initial_nanosec;

    // add sec to millisecond
    period_milliseconds = period_sec * 1000; // sec -> sec,mill_mill_mill

    // truncate to millisecond precision for windows period
    // for each X we add 0 to add and divide
    // mill_mill_mill,micr_micr_micr,ns_ns_ns -> mill_mill_mill (truncate by 6, add 500000 then divide 1000000) // 1th nano -> 1th millisecond
    period_nanosec += 500000;
    period_nanosec /= 1000000;
    period_milliseconds += period_nanosec;

    // use negative duetime to specify relative
    bool abstime = WL_MINIOBJ_HAS_FLAG(flags, TFD_TIMER_ABSTIME);
    if (!abstime) {
        qwDueTime = -qwDueTime; // flip from positive to negative
    }
    
    // Copy the relative time into a LARGE_INTEGER.
    LARGE_INTEGER   liDueTime;
    liDueTime.LowPart  = (DWORD) ( qwDueTime & 0xFFFFFFFF );
    liDueTime.HighPart = (LONG)  ( qwDueTime >> 32 );

    // thread may have woken up again
    EnterCriticalSection(&timer->criticalSection);
    timer->canceled = abstime && WL_MINIOBJ_HAS_FLAG(flags, TFD_TIMER_CANCEL_ON_SET);
    if (oldvalue != NULL) *oldvalue = timer->timer_spec;
    timer->timer_spec = *newvalue;
    timer->dueTime = liDueTime;
    timer->period = period_milliseconds;
    timer->state = TIMER_FD__SET_TIME;
    LeaveCriticalSection(&timer->criticalSection);

    SetEvent(timer->state_event);
    WaitForSingleObject(timer->response_event, INFINITE);

    // thread may have woken up again
    EnterCriticalSection(&timer->criticalSection);
    if (timer->response) {
        // set time successfully
        LeaveCriticalSection(&timer->criticalSection);
        return 0;
    }
    // error setting time
    LeaveCriticalSection(&timer->criticalSection);
    return -1;
#else
    return timerfd_settime(flags, newvalue, oldvalue);
#endif
}


WL_MINIOBJ__DECL_CALLBACK(_wl_posix_timerfd_gettime, fd, argc, argv) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return -1;
    }
    if (
        obj->version < WL_POSIX_FD_VERSION ||
        obj->type != wl_posix_type_timerfd
    ) {
        // do nothing
        errno = EINVAL;
        return -1;
    }

    if (argc != 3) {
        errno = EINVAL;
        return -1;
    }

    WL_ARGV(struct itimerspec*, currvalue, argv);

#if _WIN32
    // TODO: find out how to obtain remaining time for Event object expiration date
    //   the value stored in currvalue must ALWAYS be relative
    (void)currvalue;
    errno = ENOSYS;
    return -1;
#else
    return timerfd_gettime(currvalue);
#endif
}




#ifdef _WIN32

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <process.h>
#include <io.h>

static void addONode(int o_stream_number, FILE *file, char **buf, size_t *length);
static void delONode(FILE *file);
static int get_o_stream_number(void);
static void setODirName(char *str);
static void setOFileName(char *str, int stream_number);

struct oListNode
{
  int o_stream_number;
  FILE *file;
  char **buf;
  size_t *length;
  struct oListNode *pnext;
};

static struct oListNode *oList = NULL;

static void addONode(
        int o_stream_number,
        FILE *file,
        char **buf,
        size_t *length)
{
  struct oListNode **pcur = &oList;
  struct oListNode *node = (struct oListNode *)calloc(1, sizeof(struct oListNode));

  if(node == NULL)
    abort();

  while((*pcur) && (*pcur)->o_stream_number < o_stream_number)
    pcur = &((*pcur)->pnext);

  node->pnext = *pcur;
  node->o_stream_number = o_stream_number;
  node->file = file;
  node->buf = buf;
  node->length = length;
  (*pcur) = node;
}

static void delONode(FILE *file)
{
  struct oListNode **pcur = &oList;
  struct oListNode *todel;
  char file_name[30];

  while((*pcur) && (*pcur)->file != file)
    pcur = &((*pcur)->pnext);

  todel = (*pcur);
  if(todel == NULL){ //not found
    // WARNING(("Trying to close a simple FILE* with close_memstream()"));
  } else {
    if(EOF == fflush(file))
      abort();
    if((*(todel->length) = ftell(file)) == -1)
      abort();
    if((*(todel->buf) = (char*)calloc(1, *(todel->length) + 1)) == NULL)
      abort();
    if(EOF == fseek(file, 0, SEEK_SET))
      abort();
    fread(*(todel->buf), 1, *(todel->length), file);

    fclose(file);
    setOFileName(file_name,todel->o_stream_number);
    if(-1 == remove(file_name))
      abort();

    (*pcur) = todel->pnext;
    free(todel);
  }
}


static int get_o_stream_number(void)
{
  int o_stream_number = 1;
  struct oListNode *cur = oList;

  while(cur && o_stream_number >= cur->o_stream_number){
    o_stream_number++;
        cur = cur->pnext;
  }
  return o_stream_number;
}

static void setODirName(char *str)
{
  sprintf(str, "ostr_job_%d", _getpid());
}

static void setOFileName(char *str, int stream_number)
{
  setODirName(str);
  char fname[30];
  sprintf(fname,"/o_stream_%d",stream_number);
  strcat(str,fname);
}

FILE *
open_memstream(char **ptr, size_t *sizeloc)
{
  FILE *f;
  char file_name[30];
  int o_stream_number;

  if(oList == NULL){
    setODirName(file_name);
    mkdir(file_name);
  }

  o_stream_number = get_o_stream_number();
  setOFileName(file_name,o_stream_number);
  f = fopen(file_name,"w+");

  if(!f)
    return NULL;

  addONode(o_stream_number, f, ptr, sizeloc);

  return f;
}


void
close_memstream(FILE *f)
{
  char file_name[30];
  delONode(f);

  if(oList == NULL){
    setODirName(file_name);
    rmdir(file_name);
  }
}


// we hope this does not get invoked
// in a multi-threaded implementation

static BOOLEAN WAYLAND_WINSOCK__init_done = FALSE;
static WSADATA WAYLAND_WINSOCK__init_data;

#define INIT_WINSOCK_IF_NEEDED \
if(WAYLAND_WINSOCK__init_done == FALSE) { \
	WSAStartup(MAKEWORD(2,0), &WAYLAND_WINSOCK__init_data); \
    WAYLAND_WINSOCK__init_done = TRUE; \
}

#else
#define INIT_WINSOCK_IF_NEEDED
#endif

void wl_posix_register_syscalls(void) {
    INIT_WINSOCK_IF_NEEDED

    wl_miniobj_reg(
        wl_posix_type_socket,
        WL_MINIOBJ_SYSCALL__NR__ACCEPT4, _wl_posix_socket_accept4,
        WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE, _wl_posix_set_exec_or_close,
        WL_MINIOBJ_SYSCALL__NR__DUPFD, _wl_posix_dupfd,
        WL_MINIOBJ_SYSCALL__NR__DUPFD_2, _wl_posix_dupfd2,
        WL_MINIOBJ_SYSCALL__NR__READ, _wl_posix_read,
        WL_MINIOBJ_SYSCALL__NR__WRITE, _wl_posix_write,
        WL_MINIOBJ_SYSCALL__NR__CLOSE, _wl_posix_close,
        WL_MINIOBJ_SYSCALL__NR__LISTEN, _wl_posix_socket_listen,
        WL_MINIOBJ_SYSCALL__NR__BIND, _wl_posix_socket_bind,
        WL_MINIOBJ_SYSCALL__NR__CONNECT, _wl_posix_socket_connect,
        WL_MINIOBJ_SYSCALL__NR__SEND, _wl_posix_socket_send,
        WL_MINIOBJ_SYSCALL__NR__RECV, _wl_posix_socket_recv,
        WL_MINIOBJ_SYSCALL__NR__PEERCRED, _wl_posix_socket_peercred
    );

    wl_miniobj_reg(
        wl_posix_type_fd,
        WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE, _wl_posix_set_exec_or_close,
        WL_MINIOBJ_SYSCALL__NR__DUPFD, _wl_posix_dupfd,
        WL_MINIOBJ_SYSCALL__NR__DUPFD_2, _wl_posix_dupfd2,
        WL_MINIOBJ_SYSCALL__NR__READ, _wl_posix_read,
        WL_MINIOBJ_SYSCALL__NR__WRITE, _wl_posix_write,
        WL_MINIOBJ_SYSCALL__NR__CLOSE, _wl_posix_close
    );
    
    wl_miniobj_reg(
        wl_posix_type_eventfd,
        WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE, _wl_posix_set_exec_or_close,
        WL_MINIOBJ_SYSCALL__NR__DUPFD, _wl_posix_dupfd,
        WL_MINIOBJ_SYSCALL__NR__DUPFD_2, _wl_posix_dupfd2,
        WL_MINIOBJ_SYSCALL__NR__READ, _wl_posix_read,
        WL_MINIOBJ_SYSCALL__NR__WRITE, _wl_posix_write,
        WL_MINIOBJ_SYSCALL__NR__CLOSE, _wl_posix_close
    );

    wl_miniobj_reg(
        wl_posix_type_temporary_file,
        WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE, _wl_posix_set_exec_or_close,
        WL_MINIOBJ_SYSCALL__NR__DUPFD, _wl_posix_dupfd,
        WL_MINIOBJ_SYSCALL__NR__DUPFD_2, _wl_posix_dupfd2,
        WL_MINIOBJ_SYSCALL__NR__READ, _wl_posix_read,
        WL_MINIOBJ_SYSCALL__NR__WRITE, _wl_posix_write,
        WL_MINIOBJ_SYSCALL__NR__CLOSE, _wl_posix_close
    );

    wl_miniobj_reg(
        wl_posix_type_epoll,
        WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE, _wl_posix_set_exec_or_close,
        WL_MINIOBJ_SYSCALL__NR__READ, _wl_posix_read,
        WL_MINIOBJ_SYSCALL__NR__WRITE, _wl_posix_write,
        WL_MINIOBJ_SYSCALL__NR__EPOLL_CTL, _wl_posix_epoll_ctl,
        WL_MINIOBJ_SYSCALL__NR__EPOLL_WAIT, _wl_posix_epoll_wait,
        WL_MINIOBJ_SYSCALL__NR__CLOSE, _wl_posix_close
    );

    wl_miniobj_reg(
        wl_posix_type_timerfd,
        WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE, _wl_posix_set_exec_or_close,
        WL_MINIOBJ_SYSCALL__NR__READ, _wl_posix_read,
        WL_MINIOBJ_SYSCALL__NR__TIMERFD_SETTIME, _wl_posix_timerfd_settime,
        WL_MINIOBJ_SYSCALL__NR__TIMERFD_GETTIME, _wl_posix_timerfd_gettime,
        WL_MINIOBJ_SYSCALL__NR__CLOSE, _wl_posix_close
    );
}
