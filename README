What is Wayland?

Wayland is a project to define a protocol for a compositor to talk to
its clients as well as a library implementation of the protocol.  The
compositor can be a standalone display server running on Linux kernel
modesetting and evdev input devices, an X application, or a wayland
client itself.  The clients can be traditional applications, X servers
(rootless or fullscreen) or other display servers.

The wayland protocol is essentially only about input handling and
buffer management.  The compositor receives input events and forwards
them to the relevant client.  The clients creates buffers and renders
into them and notifies the compositor when it needs to redraw.  The
protocol also handles drag and drop, selections, window management and
other interactions that must go through the compositor.  However, the
protocol does not handle rendering, which is one of the features that
makes wayland so simple.  All clients are expected to handle rendering
themselves, typically through cairo or OpenGL.

The weston compositor is a reference implementation of a wayland
compositor and the weston repository also includes a few example
clients.


Building the wayland libraries is fairly simple, aside from libffi,
they don't have many dependencies:

    $ git clone https://github.com/mgood7123/wayland-windows-binary-build-deps

install vcpkg, and meson

extract glib, sed, and pkg-config to C:\vcpkg\installed\x64-windows

copy intl.dll to C:\vcpkg\installed\x64-windows\bin

next, go to system environmental variables

    set VCPKG_ROOT to C:\vcpkg
    add C:\vcpkg\installed\x64-windows\bin to PATH
    set PKG_CONFIG_PATH to C:\vcpkg\installed\x64-windows\lib\pkgconfig

next

    $ vcpkg integrate install
    log out and back in

this will allow compilers to use vcpkg

and this will allow C:\vcpkg\installed\x64-windows\bin\pkg-config.EXE to be found in PATH

next, create C:\vcpkg\installed\x64-windows\lib\pkgconfig\getopt.pc

add the following contents

```
prefix=${pcfiledir}/../..

exec_prefix=${prefix}
libdir=${prefix}/lib
sharedlibdir=${prefix}/lib
includedir=${prefix}/include

Name: getopt
Description: getopt library
Version: 1.0

Requires:
Libs: -L"${libdir}" -L"${sharedlibdir}" -lgetopt
Cflags: -I"${includedir}"
```

next continue with the install

    $ vcpkg install gettext:x64-windows
    $ vcpkg install libffi:x64-windows
    $ vcpkg install expat:x64-windows
    $ vcpkg install libxml2:x64-windows
    $ vcpkg install getopt:x64-windows
    $ git clone https://gitlab.freedesktop.org/mgood7123/wayland-windows
    $ cd wayland-windows
    $ cd src/QParse
    $ meson setup build --reconfigure --wipe --native-file=../../clang.txt --backend=ninja
    $ meson compile -C build
    $ cd ../..
    $ meson setup build --reconfigure --wipe --prefix=PREFIX  --native-file=clang.txt --backend=ninja
    $ meson compile -C build


testing wl_posix:

    wayland for windows comes with its own posix syscall implementation along with a subset of the posix API
    
    to test:
    
        $ cd src/QParse
        $ meson setup build --native-file=../../clang.txt --backend=ninja
        $ meson compile -C build
        $ cd ../../tests/wl_posix_tests
        $ meson wrap install gtest
        $ meson setup build --native-file=../../clang.txt --backend=ninja -Db_vscrt=mt
        // or
        $ meson setup --reconfigure --wipe build --native-file=../../clang.txt --backend=ninja -Db_vscrt=mt
        $ cls ; meson compile -C build

where PREFIX is where you want to install the libraries.  See
https://wayland.freedesktop.org for more complete build instructions
for wayland, weston, xwayland and various toolkits.
