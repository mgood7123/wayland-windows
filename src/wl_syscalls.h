#ifndef WL_SYSCALLS_H
#define WL_SYSCALLS_H

#include <stdio.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h> // CHAR_BIT

// sanity check
#if CHAR_BIT != 8
#error system does not support 8 bit addressing
#endif

#ifdef __cplusplus
#define restrict
#endif

#include "wl_syscalls__fd_allocator.h"

#ifdef _WIN32
// winsock2 includes time.h
#include <winsock2.h>
#include <Windows.h>
#include <synchapi.h>
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;

#include <aclapi.h>
#include <afunix.h>

#include <tcpmib.h>
#include <iphlpapi.h>

typedef PSID uid_t;
typedef PSID gid_t;
typedef DWORD pid_t;
typedef int socklen_t;

struct iovec {
	void  *iov_base;    /* Starting address */
	size_t iov_len;     /* Number of bytes to transfer */
};

#define AF_LOCAL AF_UNIX
#define PF_LOCAL PF_UNIX

#define	SOCK_CLOEXEC		0x8000	/* set FD_CLOEXEC */
#define	SOCK_NONBLOCK		0x4000	/* set O_NONBLOCK */
#define MSG_NOSIGNAL        0x1000  /* dont generate SIGPIPE */
#define MSG_DONTWAIT        0x2000  /* dont block */

typedef union epoll_data {
  void* ptr;
  int fd;
  uint32_t u32;
  uint64_t u64;
  SOCKET sock; /* Windows specific */
  HANDLE hnd;  /* Windows specific */
} epoll_data_t;

struct epoll_event {
  uint32_t events;   /* Epoll events and flags */
  epoll_data_t data; /* User data variable */
};

#define CLANG__ALWAYS_INLINE __attribute((always_inline))

/*
 * Add the pseudo keyword 'fallthrough' so case statement blocks
 * must end with any of these keywords:
 *   break;
 *   CLANG__FALLTHROUGH;
 *   continue;
 *   goto <label>;
 *   return [expression];
 *
 *  gcc: https://gcc.gnu.org/onlinedocs/gcc/Statement-Attributes.html#Statement-Attributes
 */
#if __has_attribute(__fallthrough__)
# define CLANG__FALLTHROUGH                    __attribute__((__fallthrough__))
#else
# define CLANG__FALLTHROUGH                    do {} while (0)  /* fallthrough */
#endif

#ifdef _WIN64

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
typedef long long LP64__LONG;

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
typedef unsigned long long LP64__ULONG;

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__LONG_MIN LLONG_MIN

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__LONG_MAX LLONG_MAX

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__ULONG_MIN ULLONG_MIN

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__ULONG_MAX ULLONG_MAX

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__1L 1LL

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__1UL 1ULL

#else
#ifdef _WIN32

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
typedef long LP64__LONG;

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
typedef unsigned long LP64__ULONG;

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__LONG_MIN LONG_MIN

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__LONG_MAX LONG_MAX

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__ULONG_MIN ULONG_MIN

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__ULONG_MAX ULONG_MAX

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__1L 1L

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__1UL 1UL

#endif
#endif

// time64_t is undefined on windows (and __time64_t is undefined in 32-bit)
// alpine specifies time_t as int64
typedef int64_t time_64_t;

// suseconds_t is undefined on windows
// alpine specifies suseconds_t as int64
typedef int64_t suseconds_t;

// windows defined timespec, we need to rename
//
// alpine specifies time_t as int64
// alpine specifies suseconds_t as int64
//
struct timeval_64_t { time_64_t tv_sec; suseconds_t tv_usec; };

// windows defined timespec, we need to rename
//
// alpine pads the timespec to 64-bits
//   "the kernel interfaces use a timespec layout padded to match the representation of a pair of 64-bit values, which requires endian-specific padding."
//
// alpine specifies time_t as int64
//
// UNIX long is LP64, 32-bit systems long == 32 bits, 64-bit systems long == 64 bits
//

// clang defines __BYTE_ORDER__

struct timespec_N_bit {
	time_64_t tv_sec;
	int :8*(sizeof(time_t)-sizeof(LP64__LONG))*(__BYTE_ORDER__==4321);
	LP64__LONG tv_nsec;
	int :8*(sizeof(time_t)-sizeof(LP64__LONG))*(__BYTE_ORDER__!=4321);
};

struct itimerspec {
	struct timespec_N_bit it_interval;  /* Interval for periodic timer */
	struct timespec_N_bit it_value;     /* Initial expiration */
};

#else

// windows includes time.h so include it for linux as well
#include <time.h>
#include <sys/socket.h>
#include <sys/eventfd.h>
#ifdef HAVE_SYS_UCRED_H
#include <sys/ucred.h>
#endif

#endif

typedef struct wl_peercred_t {
    pid_t pid;
    uid_t uid;
    gid_t gid;
} wl_peercred_t;

#define WL_MINIOBJ_MACRO____PP_128TH_ARG( _1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,_31,_32,_33,_34,_35,_36,_37,_38,_39,_40,_41,_42,_43,_44,_45,_46,_47,_48,_49,_50,_51,_52,_53,_54,_55,_56,_57,_58,_59,_60,_61,_62,_63,_64,_65,_66,_67,_68,_69,_70,_71,_72,_73,_74,_75,_76,_77,_78,_79,_80,_81,_82,_83,_84,_85,_86,_87,_88,_89,_90,_91,_92,_93,_94,_95,_96,_97,_98,_99,_100,_101,_102,_103,_104,_105,_106,_107,_108,_109,_110,_111,_112,_113,_114,_115,_116,_117,_118,_119,_120,_121,_122,_123,_124,_125,N,...) N

#define WL_MINIOBJ_MACRO____PP_RSEQ_N() 125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0

#define WL_MINIOBJ_MACRO____PP_NARG(N, ...) WL_MINIOBJ_MACRO____PP_NARG_(__VA_ARGS__, WL_MINIOBJ_MACRO____PP_RSEQ_N())

#define WL_MINIOBJ_MACRO____PP_NARG_(...) WL_MINIOBJ_MACRO____PP_128TH_ARG(__VA_ARGS__)

#ifdef __cplusplus
extern "C" {
#endif

// deinitializes the global miniobj system
//
// the global miniobj system will be re-initialized automatically when needed
void wl_miniobj_deinit(void);

/*
 * A mini object, used to implement a
 * kernel-like api for opaque objects
 * 
 * this MUST NOT be passed to free() since it maintains a reference count
 */
typedef struct wl_miniobj {
    uint16_t version;
    uint16_t type;
    uint64_t data;
} wl_miniobj;

typedef int (*WL_MINIOBJ_CALLBACK)(int fd, int argc, va_list args);

// creates a new miniobj object wrapped in an integer identifier
//
//   PERF RECOMMENDATION:
//     it is recommended to delay the creation of the miniobj itself until all data has been set-up
//         this results in fast failure and minimizes possible data races
//
//       instead of doing
//
//         WL_MINIOBJ_CREATE_WITH_OBJ(obj, fd, ver, type, true);
//         obj->data = allocate_ref_counted_data();
//         if (obj->data != nullptr) {
//             ((REF*)obj->data)->ref = create();
//             if (((REF*)obj->data)->ref == INVALID_HANDLE) {
//                 deallocate_ref_counted_data(obj->data);
//                 wl_miniobj_destroy(obj_fd, obj);
//
//                 // return error
//                 errno = EINVAL;
//                 return -1;
//             }
//         }
//         if (obj->data == nullptr) {
//             wl_miniobj_destroy(obj_fd, obj);
//
//             // return error
//             errno = EINVAL;
//             return -1;
//         }
//         return fd;
//
//       we could instead do
//
//         HANDLE h = create();
//         if (h == INVALID_HANDLE) {
//             errno = EINVAL;
//             return -1;
//         }
//
//         REF * data = allocate_ref_counted_data();
//         if (data == nullptr) {
//             destroy(h);
//             errno = EINVAL;
//             return -1;
//         }
//
//         data->ref = h;
//
//         WL_MINIOBJ_CREATE_WITH_OBJ(obj, fd, ver, type, true);
//         obj->data = data;
//         return fd;
//
//
// ERROR HANDLING: in events of error handling, call wl_miniobj_destroy first
//   it is common to do the following:
//
//       WL_MINIOBJ_CREATE_WITH_OBJ(
//           obj, obj_fd,                             // obj
//           obj_version, obj_type,                   // info
//           obj_underlying_data_is_reference_counted // ref count
//       );
//
//       int some_error = setup_obj(obj_fd, obj);
//
//       if (some_error != 0) {
//
//           // only if there is data to clean up
//           // wl_syscall__close(obj_fd);
//
//           // otherwise call this as it is cheaper
//           wl_miniobj_destroy(obj_fd, obj);
//
//           // return error
//           errno = EINVAL;
//           return -1;
//       }
//
// 
// the miniobj can be obtained by passing the returned fd into wl_miniobj_get
//
// WL_MINIOBJ_CREATE_WITH_OBJ can be used instead to automatically obtain the miniobj
//
// WL_MINIOBJ_CREATE(fd, ...);
// wl_miniobj * obj = wl_miniobj_get(fd);
// // or
// WL_MINIOBJ_CREATE_WITH_OBJ(obj, fd, ...);
//
// param: underlying_data_is_reference_counted
//    true:  this is not internally reference counted by the miniobj registry
//    false: this is internally reference counted by the miniobj registry
//
// the following objects are known to be reference counted:
//    windows HANDLE
//    unix file descriptors (int fd)
//
// this object should be destroyed by passing it to wl_miniobj_destroy
//
// returns name with a value of -1 if the type has not registered
// returns name with a value of -1 if malloc failed
//
#define WL_MINIOBJ_CREATE(name, obj_version, obj_type, underlying_data_is_reference_counted) int name = wl_miniobj_create(obj_version, obj_type, underlying_data_is_reference_counted)

// same as WL_MINIOBJ_CREATE but also extracts the miniobj from the returned fd
//
// WL_MINIOBJ_CREATE_WITH_OBJ(obj, fd, ...);
//
//
// this object should be destroyed by passing it to wl_miniobj_destroy
//
// returns fd_name with a value of -1 if the type has not registered
// returns fd_name with a value of -1 if malloc failed
//
#define WL_MINIOBJ_CREATE_WITH_OBJ(fd_name, obj_name, obj_version, obj_type, underlying_data_is_reference_counted) WL_MINIOBJ_CREATE(fd_name, obj_version, obj_type, underlying_data_is_reference_counted); wl_miniobj * obj_name = wl_miniobj_get(fd_name)

// obtains the miniobj referenced by the given fd
//
// returns NULL if the fd is invalid
// returns NULL if the fd is currently being deallocated (to avoid recursively deallocating the fd)
//
wl_miniobj *  wl_miniobj_get(int fd);

// returns true if the miniobj is currently being destroyed
bool wl_miniobj_is_being_destroyed(int fd);

// duplicates a miniobj object and returns it wrapped in a new fd
//
// if the underlying data IS reference counted
// we must construct a new object
// each duplicate holds its own data
// this object itself is not reference counted
//
// if the underlying data IS NOT reference counted
// we must return the same object
// this object itself is reference counted
//
#define WL_MINIOBJ_DUPLICATE(out_fd, in_fd) int out_fd = wl_miniobj_duplicate(in_fd)

// same as WL_MINIOBJ_DUPLICATE but also extracts the miniobj from the returned fd
#define WL_MINIOBJ_DUPLICATE_WITH_OBJ(out_fd, out_obj, in_fd) int out_fd = wl_miniobj_duplicate(in_fd); wl_miniobj * out_obj = wl_miniobj_get(out_fd)

// returns false if `obj == NULL || obj->version < _version || obj->type < _type`
//
#define WL_MINIOBJ_IS_NOT_VALID(obj, _version, _type) (obj == NULL || obj->version < _version || obj->type < _type)

#define WL_MINIOBJ_HAS_FLAG(flags, flag) (((flags) & (flag)) != 0)
#define WL_MINIOBJ_ADD_FLAG(flags, flag) flags |= (flag)
#define WL_MINIOBJ_REMOVE_FLAG(flags, flag) flags &= ~(flag)
#define WL_MINIOBJ_SET_FLAG(flags, flag, boolean_value) { if (boolean_value) { WL_MINIOBJ_ADD_FLAG(flags, flag); } else { WL_MINIOBJ_REMOVE_FLAG(flags, flag); } }
#define WL_MINIOBJ_HAS_INVALID_FLAGS(flags, all_supported_flags) (((flags) & ~ (all_supported_flags)) != 0)

#define WL_ARGV(type, name, argv) type name = va_arg(argv, type)

// set this to true to log miniobj malloc/free calls
extern bool wl_miniobj_log_allocation;
uint64_t wl_miniobj__total_allocations(void);
uint64_t wl_miniobj__total_deallocations(void);

// creates a new miniobj object wrapped in an integer identifier
// 
// the miniobj can be obtained by passing the returned fd into wl_miniobj_get
//
// int fd = wl_miniobj_create(...);
// wl_miniobj * obj = wl_miniobj_get(fd);
//
// param: underlying_data_is_reference_counted
//    true:  this is internally reference counted by the miniobj registry
//    false: this is not internally reference counted by the miniobj registry
//
// the following objects are known to be reference counted:
//    windows HANDLE
//    unix file descriptors (int fd)
//
// this object should be destroyed by passing it to wl_miniobj_destroy
//
// returns NULL if the type has not registered
//
int wl_miniobj_create(uint16_t version, uint16_t type, bool underlying_data_is_reference_counted);

// returns true if a call to wl_miniobj_destroy would destroy this object
//
// it is recommended to call wl_miniobj_destroy regardless of the return value
//
// usage:
//        if (wl_miniobj_should_destroy(fd)) {
//            cleanup(fd);
//        }
//        wl_miniobj_destroy(fd, NULL);
//
bool wl_miniobj_should_destroy(int fd);

// duplicates a miniobj object and returns it wrapped in a new fd
//
// if the underlying data IS reference counted
// we must construct a new object
// each duplicate holds its own data
// this object itself is not reference counted
//
// if the underlying data IS NOT reference counted
// we must return the same object
// this object itself is reference counted
//
int wl_miniobj_duplicate(int fd);

// destroys an allocated miniobj object
//
// it is UB to manually free() this object as the reference count WILL NOT decrease nor will it's wrapped fd be deallocated
// 
// it is valid to pass the fd to wl_syscalls__fd_allocator__deallocate_fd however this requires a valid fd allocator instance
//   and MUST NOT be done from a close syscall callback (as it will recurse infinitely)
//
// if the object is internally reference counted by the miniobj registry then this function also decrements it after freeing
//
// this WILL call the close() SYSCALL ( WL_MINIOBJ_SYSCALL__NR__CLOSE ) on the object due to the fd being deallocated
// HOWEVER wl_miniobj_get will detect this and return NULL
//
// it is SAFE to pass a NULL object to this function
//
void wl_miniobj_destroy(int fd, wl_miniobj * obj);


void wl_miniobj_internal_reg(uint16_t obj_type, int argc, ...);

// registers a new wl_miniobj type
//
// this takes [ obj_type, syscall, callback, syscall, callback, ... ]
//
// a syscall should ALWAYS have a matching callback
//
// care should be taken when passing an obj_type since this
// will be a direct index into the registery
//
// this will return NULL if the type already exists
// and print an error to the console specifying the type id that was given
//
//
// it is recommended for a registered type to have a registered close callback
// this is to clean up anything done when creating the object
//
// this may include:
// allocations, mutexes, open files/handles/descriptors, ect
// and others that must be cleaned up before deleting the object
//
// // declare a callback
// WL_MINIOBJ__DECL_CALLBACK(my_close, fd, argc, argv) {
//     if (wl_miniobj_should_destroy(fd)) {
//         // handle object destruction here
//     }
//     // optionally freeing with wl_miniobj_destroy(fd, NULL);
// }
//
// // pass this callback to wl_miniobj_reg
// wl_miniobj_reg(obj_type, WL_MINIOBJ_SYSCALL__NR__CLOSE, my_close);
//
#define wl_miniobj_reg(obj_type, ...) wl_miniobj_internal_reg(obj_type, WL_MINIOBJ_MACRO____PP_NARG(0, 0, ## __VA_ARGS__) - 1, ## __VA_ARGS__)

// unregisters a registered wl_miniobj type
// 
// does nothing if already unregistered
//
// a type CANNOT be unregistered if there are allocated instances of it
//
void wl_miniobj_unreg(uint16_t obj_type);

// extracts obj pointed to by fd and invokes a syscall on it
//
// does nothing if obj is NULL
// does nothing if obj is unregistered
// does nothing if obj is not interested in desired syscall
//
int WL_MINIOBJ__DO_SYSCALL(int fd, uint16_t syscall, int argc, ...);

// a macro to be used for declaring syscall callbacks
//
// it is recommended for a registered type to have a registered close callback
// this is to clean up anything done when creating the object
//
// this may include:
// allocations, mutexes, open files/handles/descriptors, ect
// and others that must be cleaned up before deleting the object
//
// // declare a callback
// WL_MINIOBJ__DECL_CALLBACK(my_close, fd, argc, argv) {
//     if (wl_miniobj_should_destroy(fd)) {
//         // handle object destruction here
//     }
//     // optionally freeing with wl_miniobj_destroy(fd, NULL);
// }
//
// // pass this callback to wl_miniobj_reg
// wl_miniobj_reg(obj_type, WL_MINIOBJ_SYSCALL__NR__CLOSE, my_close);
//
#define WL_MINIOBJ__DECL_CALLBACK(name, fd, argc, argv) static int name (int fd, const int argc, va_list argv)

// SYSCALLS START
//     accept4
//     close
//     dupfd
//     dupfd_2
//     set_exec_or_close
//     read
//     write
//     listen
//     bind
//     connect
//     mkstemp
//     send
//     recv
//     peercred
//     peercred_struct
//     peercred_uid
//     peercred_gid
//     peercred_pid
//     epoll_ctl
//     epoll_wait
//     timerfd_settime
//     timerfd_gettime
// SYSCALLS END

//        The  accept() system call is used with connection-based socket types (SOCK_STREAM, SOCK_SEQPACKET).  It extracts
//        the first connection request on the queue of pending connections for the listening socket, sockfd, creates a new
//        connected  socket,  and returns a new file descriptor referring to that socket.  The newly created socket is not
//        in the listening state.  The original socket sockfd is unaffected by this call.
// 
//        The argument sockfd is a socket that has been created with socket(2), bound to a local address with bind(2), and
//        is listening for connections after a listen(2).
// 
//        The  argument  addr  is  a pointer to a sockaddr structure.  This structure is filled in with the address of the
//        peer socket, as known to the communications layer.  The exact format of the address returned addr is  determined
//        by  the  socket's  address  family  (see  socket(2)  and the respective protocol man pages).  When addr is NULL,
//        nothing is filled in; in this case, addrlen is not used, and should also be NULL.
extern const int WL_MINIOBJ_SYSCALL__NR__ACCEPT4;

//        The  accept() system call is used with connection-based socket types (SOCK_STREAM, SOCK_SEQPACKET).  It extracts
//        the first connection request on the queue of pending connections for the listening socket, sockfd, creates a new
//        connected  socket,  and returns a new file descriptor referring to that socket.  The newly created socket is not
//        in the listening state.  The original socket sockfd is unaffected by this call.
// 
//        The argument sockfd is a socket that has been created with socket(2), bound to a local address with bind(2), and
//        is listening for connections after a listen(2).
// 
//        The  argument  addr  is  a pointer to a sockaddr structure.  This structure is filled in with the address of the
//        peer socket, as known to the communications layer.  The exact format of the address returned addr is  determined
//        by  the  socket's  address  family  (see  socket(2)  and the respective protocol man pages).  When addr is NULL,
//        nothing is filled in; in this case, addrlen is not used, and should also be NULL.
static inline int wl_syscall__accept4(int fd, struct sockaddr *restrict addr, socklen_t *restrict addrlen, int flags) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__ACCEPT4, 3, addr, addrlen, flags);
}



// close is expected to clean up
// and then free the passed object via wl_miniobj_destroy
extern const int WL_MINIOBJ_SYSCALL__NR__CLOSE;

// close is expected to clean up
// and then free the passed object via wl_miniobj_destroy
static inline int wl_syscall__close(int fd) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__CLOSE, 0);
}



//        The  dup()  system  call  allocates a new file descriptor that refers to the same open file description as the
//        descriptor oldfd.  (For an explanation of open file descriptions,  see  open(2).)   The  new  file  descriptor
//        number is guaranteed to be the lowest-numbered file descriptor that was unused in the calling process.
// 
//        After  a  successful return, the old and new file descriptors may be used interchangeably.  Since the two file
//        descriptors refer to the same open file description, they  share  file  offset  and  file  status  flags;  for
//        example,  if  the file offset is modified by using lseek(2) on one of the file descriptors, the offset is also
//        changed for the other file descriptor.
// 
//        The two file descriptors do not share file descriptor flags (the close-on-exec flag).  The close-on-exec  flag
//        (FD_CLOEXEC; see fcntl(2)) for the duplicate descriptor is off.
extern const int WL_MINIOBJ_SYSCALL__NR__DUPFD;

//        The  dup()  system  call  allocates a new file descriptor that refers to the same open file description as the
//        descriptor oldfd.  (For an explanation of open file descriptions,  see  open(2).)   The  new  file  descriptor
//        number is guaranteed to be the lowest-numbered file descriptor that was unused in the calling process.
// 
//        After  a  successful return, the old and new file descriptors may be used interchangeably.  Since the two file
//        descriptors refer to the same open file description, they  share  file  offset  and  file  status  flags;  for
//        example,  if  the file offset is modified by using lseek(2) on one of the file descriptors, the offset is also
//        changed for the other file descriptor.
// 
//        The two file descriptors do not share file descriptor flags (the close-on-exec flag).  The close-on-exec  flag
//        (FD_CLOEXEC; see fcntl(2)) for the duplicate descriptor is off.
static inline int wl_syscall__dupfd(int fd) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__DUPFD, 0);
}



// WINDOWS ONLY: minfd is ignored (but still checked for) since it operates on HANDLE pointers instead of file descriptor integers
// 
//        The  dup2()  system call performs the same task as dup(), but instead of using the lowest-numbered unused file
//        descriptor, it uses the file descriptor number specified in newfd.  In other words, the file descriptor  newfd
//        is adjusted so that it now refers to the same open file description as oldfd.
// 
//        If  the  file  descriptor  newfd was previously open, it is closed before being reused; the close is performed
//        silently (i.e., any errors during the close are not reported by dup2()).
// 
//        The steps of closing and reusing the file descriptor newfd  are  performed  atomically.   This  is  important,
//        because  trying  to  implement  equivalent  functionality  using  close(2)  and dup() would be subject to race
//        conditions, whereby newfd might be reused between the two steps.  Such reuse could  happen  because  the  main
//        program  is  interrupted  by  a  signal handler that allocates a file descriptor, or because a parallel thread
//        allocates a file descriptor.
// 
//        Note the following points:
// 
//        *  If oldfd is not a valid file descriptor, then the call fails, and newfd is not closed.
// 
//        *  If oldfd is a valid file descriptor, and newfd has the same value as oldfd, then dup2() does  nothing,  and
//           returns newfd.
extern const int WL_MINIOBJ_SYSCALL__NR__DUPFD_2;

// WINDOWS ONLY: minfd is ignored (but still checked for) since it operates on HANDLE pointers instead of file descriptor integers
// 
//        The  dup2()  system call performs the same task as dup(), but instead of using the lowest-numbered unused file
//        descriptor, it uses the file descriptor number specified in newfd.  In other words, the file descriptor  newfd
//        is adjusted so that it now refers to the same open file description as oldfd.
// 
//        If  the  file  descriptor  newfd was previously open, it is closed before being reused; the close is performed
//        silently (i.e., any errors during the close are not reported by dup2()).
// 
//        The steps of closing and reusing the file descriptor newfd  are  performed  atomically.   This  is  important,
//        because  trying  to  implement  equivalent  functionality  using  close(2)  and dup() would be subject to race
//        conditions, whereby newfd might be reused between the two steps.  Such reuse could  happen  because  the  main
//        program  is  interrupted  by  a  signal handler that allocates a file descriptor, or because a parallel thread
//        allocates a file descriptor.
// 
//        Note the following points:
// 
//        *  If oldfd is not a valid file descriptor, then the call fails, and newfd is not closed.
// 
//        *  If oldfd is a valid file descriptor, and newfd has the same value as oldfd, then dup2() does  nothing,  and
//           returns newfd.
static inline int wl_syscall__dupfd_2(int fd, int minfd) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__DUPFD_2, 1, minfd);
}



// sets the CLOSE-ON-EXEC flag on the given object
// 
// this function should return NULL only if close is called on the passed object
extern const int WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE;

// sets the CLOSE-ON-EXEC flag on the given object
// 
// this function should return NULL only if close is called on the passed object
static inline int wl_syscall__set_exec_or_close(int fd) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__SET_EXEC_OR_CLOSE, 0);
}



extern const int WL_MINIOBJ_SYSCALL__NR__READ;

static inline int wl_syscall__read(int fd, void * buf, size_t count) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__READ, 2, buf, count);
}



extern const int WL_MINIOBJ_SYSCALL__NR__WRITE;

static inline int wl_syscall__write(int fd, void * buf, size_t count) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__WRITE, 2, buf, count);
}



//        listen() marks the socket referred to by sockfd as a passive socket, that is, as a
//        socket that will be used to accept incoming connection requests using accept(2).
// 
//        The sockfd argument is  a  file  descriptor  that  refers  to  a  socket  of  type
//        SOCK_STREAM or SOCK_SEQPACKET.
// 
//        The  backlog  argument  defines  the  maximum length to which the queue of pending
//        connections for sockfd may grow.  If a connection request arrives when  the  queue
//        is full, the client may receive an error with an indication of ECONNREFUSED or, if
//        the underlying protocol supports retransmission, the request  may  be  ignored  so
//        that a later reattempt at connection succeeds.
extern const int WL_MINIOBJ_SYSCALL__NR__LISTEN;

//        listen() marks the socket referred to by sockfd as a passive socket, that is, as a
//        socket that will be used to accept incoming connection requests using accept(2).
// 
//        The sockfd argument is  a  file  descriptor  that  refers  to  a  socket  of  type
//        SOCK_STREAM or SOCK_SEQPACKET.
// 
//        The  backlog  argument  defines  the  maximum length to which the queue of pending
//        connections for sockfd may grow.  If a connection request arrives when  the  queue
//        is full, the client may receive an error with an indication of ECONNREFUSED or, if
//        the underlying protocol supports retransmission, the request  may  be  ignored  so
//        that a later reattempt at connection succeeds.
static inline int wl_syscall__listen(int fd, int backlog) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__LISTEN, 1, backlog);
}



//        When  a  socket  is  created  with  socket(2),  it exists in a name space (address
//        family) but has no address assigned to it.  bind() assigns the  address  specified
//        by  addr  to  the  socket  referred  to  by  the  file descriptor sockfd.  addrlen
//        specifies the size, in bytes,  of  the  address  structure  pointed  to  by  addr.
//        Traditionally, this operation is called “assigning a name to a socket”.
// 
//        It  is  normally  necessary  to  assign  a  local  address  using  bind() before a
//        SOCK_STREAM socket may receive connections (see accept(2)).
// 
//        The rules used in name binding vary between address families.  Consult the  manual
//        entries  in  Section  7  for  detailed  information.   For AF_INET, see ip(7); for
//        AF_INET6, see ipv6(7); for AF_UNIX, see unix(7); for AF_APPLETALK, see ddp(7); for
//        AF_PACKET,  see  packet(7);  for  AF_X25,  see  x25(7);  and  for  AF_NETLINK, see
//        netlink(7).
// 
//        The actual structure passed for the addr  argument  will  depend  on  the  address
//        family.  The sockaddr structure is defined as something like:
// 
//            struct sockaddr {
//                sa_family_t sa_family;
//                char        sa_data[14];
//            }
// 
//        The only purpose of this structure is to cast the structure pointer passed in addr
//        in order to avoid compiler warnings.
extern const int WL_MINIOBJ_SYSCALL__NR__BIND;

//        When  a  socket  is  created  with  socket(2),  it exists in a name space (address
//        family) but has no address assigned to it.  bind() assigns the  address  specified
//        by  addr  to  the  socket  referred  to  by  the  file descriptor sockfd.  addrlen
//        specifies the size, in bytes,  of  the  address  structure  pointed  to  by  addr.
//        Traditionally, this operation is called “assigning a name to a socket”.
// 
//        It  is  normally  necessary  to  assign  a  local  address  using  bind() before a
//        SOCK_STREAM socket may receive connections (see accept(2)).
// 
//        The rules used in name binding vary between address families.  Consult the  manual
//        entries  in  Section  7  for  detailed  information.   For AF_INET, see ip(7); for
//        AF_INET6, see ipv6(7); for AF_UNIX, see unix(7); for AF_APPLETALK, see ddp(7); for
//        AF_PACKET,  see  packet(7);  for  AF_X25,  see  x25(7);  and  for  AF_NETLINK, see
//        netlink(7).
// 
//        The actual structure passed for the addr  argument  will  depend  on  the  address
//        family.  The sockaddr structure is defined as something like:
// 
//            struct sockaddr {
//                sa_family_t sa_family;
//                char        sa_data[14];
//            }
// 
//        The only purpose of this structure is to cast the structure pointer passed in addr
//        in order to avoid compiler warnings.
static inline int wl_syscall__bind(int fd, const struct sockaddr *addr, socklen_t addrlen) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__BIND, 2, addr, addrlen);
}



//        The  connect()  system call connects the socket referred to by the file descriptor
//        sockfd to the address specified by addr.  The addrlen argument specifies the  size
//        of  addr.  The format of the address in addr is determined by the address space of
//        the socket sockfd; see socket(2) for further details.
// 
//        If the socket sockfd is of type SOCK_DGRAM, then addr  is  the  address  to  which
//        datagrams  are  sent  by  default,  and  the only address from which datagrams are
//        received.  If the socket is of  type  SOCK_STREAM  or  SOCK_SEQPACKET,  this  call
//        attempts to make a connection to the socket that is bound to the address specified
//        by addr.
// 
//        Some  protocol  sockets  (e.g.,  UNIX  domain  stream  sockets)  may  successfully
//        connect() only once.
// 
//        Some  protocol  sockets  (e.g., datagram sockets in the UNIX and Internet domains)
//        may use connect() multiple times to change their association.
// 
//        Some protocol sockets (e.g., TCP sockets as well as datagram sockets in  the  UNIX
//        and  Internet  domains)  may  dissolve the association by connecting to an address
//        with the sa_family member of sockaddr set to AF_UNSPEC; thereafter, the socket can
//        be  connected  to  another address.  (AF_UNSPEC is supported on Linux since kernel
//        2.2.)
extern const int WL_MINIOBJ_SYSCALL__NR__CONNECT;

//        The  connect()  system call connects the socket referred to by the file descriptor
//        sockfd to the address specified by addr.  The addrlen argument specifies the  size
//        of  addr.  The format of the address in addr is determined by the address space of
//        the socket sockfd; see socket(2) for further details.
// 
//        If the socket sockfd is of type SOCK_DGRAM, then addr  is  the  address  to  which
//        datagrams  are  sent  by  default,  and  the only address from which datagrams are
//        received.  If the socket is of  type  SOCK_STREAM  or  SOCK_SEQPACKET,  this  call
//        attempts to make a connection to the socket that is bound to the address specified
//        by addr.
// 
//        Some  protocol  sockets  (e.g.,  UNIX  domain  stream  sockets)  may  successfully
//        connect() only once.
// 
//        Some  protocol  sockets  (e.g., datagram sockets in the UNIX and Internet domains)
//        may use connect() multiple times to change their association.
// 
//        Some protocol sockets (e.g., TCP sockets as well as datagram sockets in  the  UNIX
//        and  Internet  domains)  may  dissolve the association by connecting to an address
//        with the sa_family member of sockaddr set to AF_UNSPEC; thereafter, the socket can
//        be  connected  to  another address.  (AF_UNSPEC is supported on Linux since kernel
//        2.2.)
static inline int wl_syscall__connect(int fd, const struct sockaddr *addr, socklen_t addrlen) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__CONNECT, 2, addr, addrlen);
}



//        The  mkstemp()  function  generates  a  unique  temporary  filename from template,
//        creates and opens the file, and returns an open file descriptor for the file.
// 
//        The last six characters of template must be "XXXXXX" and these are replaced with a
//        string  that  makes the filename unique.  Since it will be modified, template must
//        not be a string constant, but should be declared as a character array.
// 
//        The file is created with permissions 0600, that is,  read  plus  write  for  owner
//        only.   The  returned  file  descriptor provides both read and write access to the
//        file.  The file is opened with the open(2)  O_EXCL  flag,  guaranteeing  that  the
//        caller is the process that creates the file.
extern const int WL_MINIOBJ_SYSCALL__NR__MKSTEMP;

//        The  mkstemp()  function  generates  a  unique  temporary  filename from template,
//        creates and opens the file, and returns an open file descriptor for the file.
// 
//        The last six characters of template must be "XXXXXX" and these are replaced with a
//        string  that  makes the filename unique.  Since it will be modified, template must
//        not be a string constant, but should be declared as a character array.
// 
//        The file is created with permissions 0600, that is,  read  plus  write  for  owner
//        only.   The  returned  file  descriptor provides both read and write access to the
//        file.  The file is opened with the open(2)  O_EXCL  flag,  guaranteeing  that  the
//        caller is the process that creates the file.
static inline int wl_syscall__mkstemp(int fd, char *template_) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__MKSTEMP, 1, template_);
}



// windows does not accept the same flags that linux does
// particularly MSG_NOSIGNAL and MSG_DONTWAIT
extern const int WL_MINIOBJ_SYSCALL__NR__SEND;

// windows does not accept the same flags that linux does
// particularly MSG_NOSIGNAL and MSG_DONTWAIT
static inline int wl_syscall__send(int fd, const void* buf, size_t len, int flags) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__SEND, 3, buf, len, flags);
}



// windows does not accept the same flags that linux does
// particularly MSG_NOSIGNAL and MSG_DONTWAIT
extern const int WL_MINIOBJ_SYSCALL__NR__RECV;

// windows does not accept the same flags that linux does
// particularly MSG_NOSIGNAL and MSG_DONTWAIT
static inline int wl_syscall__recv(int fd, void* buf, size_t len, int flags) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__RECV, 3, buf, len, flags);
}



// attempts to obtain the pid, group id, and user id of the given socket
// if any of these given are NULL, then they should not be obtained
// 
// eg, if gid is NULL then the implementation shall not assign gid any value
// 
// returns 0 on success, -1 on failure
extern const int WL_MINIOBJ_SYSCALL__NR__PEERCRED;

// attempts to obtain the pid, group id, and user id of the given socket
// if any of these given are NULL, then they should not be obtained
// 
// eg, if gid is NULL then the implementation shall not assign gid any value
// 
// returns 0 on success, -1 on failure
static inline int wl_syscall__peercred(int fd, uid_t* uid, gid_t* gid, pid_t* pid) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__PEERCRED, 3, uid, gid, pid);
}



static inline int wl_syscall__peercred_struct(int fd, wl_peercred_t* s) {
    return wl_syscall__peercred(fd, &s->uid, &s->gid, &s->pid);
}



// obtains only the uid component of the peercred
static inline int wl_syscall__peercred_uid(int fd, uid_t * uid) {
    return wl_syscall__peercred(fd, uid, NULL, NULL);
}



// obtains only the gid component of the peercred
static inline int wl_syscall__peercred_gid(int fd, gid_t * gid) {
    return wl_syscall__peercred(fd, NULL, gid, NULL);
}



// obtains only the pid component of the peercred
// on windows this avoids an extra lookup that obtains the uid and gid components
static inline int wl_syscall__peercred_pid(int fd, pid_t * pid) {
    return wl_syscall__peercred(fd, NULL, NULL, pid);
}



extern const int WL_MINIOBJ_SYSCALL__NR__EPOLL_CTL;

static inline int wl_syscall__epoll_ctl(int fd, int op, int socket, struct epoll_event* events) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__EPOLL_CTL, 3, op, socket, events);
}



extern const int WL_MINIOBJ_SYSCALL__NR__EPOLL_WAIT;

static inline int wl_syscall__epoll_wait(int fd, struct epoll_event* events, int maxevents, int timeout) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__EPOLL_WAIT, 3, events, maxevents, timeout);
}



extern const int WL_MINIOBJ_SYSCALL__NR__TIMERFD_SETTIME;

static inline int wl_syscall__timerfd_settime(int fd, int flags, const struct itimerspec* newvalue, struct itimerspec* oldvalue) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__TIMERFD_SETTIME, 3, flags, newvalue, oldvalue);
}



extern const int WL_MINIOBJ_SYSCALL__NR__TIMERFD_GETTIME;

static inline int wl_syscall__timerfd_gettime(int fd, struct itimerspec* currvalue) {
    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__TIMERFD_GETTIME, 1, currvalue);
}

#ifdef __cplusplus
}
#endif

#endif // WL_SYSCALLS_H
