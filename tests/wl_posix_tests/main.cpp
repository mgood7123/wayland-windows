#include <stdio.h>
#include <gtest/gtest.h>
#include <wl_posix.h>

// run collected tests

int main(int argc, char **argv) {
    printf("\n");

    wl_miniobj_debug = false;
    wl_miniobj_log_allocation = false;
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
