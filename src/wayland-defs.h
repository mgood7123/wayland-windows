#ifndef WAYLAND_DEFS_H
#define WAYLAND_DEFS_H

#include "wl_posix.h"

#include <stdio.h>

#ifdef _WIN32
#define UNIMPLIMENTED printf("%s is unimplimented", __func__);
#else
#define UNIMPLIMENTED
#endif

#endif // WAYLAND_DEFS_H