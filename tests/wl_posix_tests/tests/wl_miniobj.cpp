#include <gtest/gtest.h>
#include <wl_posix.h>

TEST(miniobj, create_no_reg) {
    int obj_fd = wl_miniobj_create(100, 100, false);
    EXPECT_EQ(obj_fd, -1);
    EXPECT_EQ(wl_miniobj__total_allocations(), 0);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 0);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
}

WL_MINIOBJ__DECL_CALLBACK(close__, a, b, c) {
    if (wl_miniobj_is_being_destroyed(a)) {
        return 0;
    }
    wl_miniobj_destroy(a, NULL);
    return 0;
}

TEST(miniobj, create_reg_destroy) {
    wl_miniobj_reg(100, WL_MINIOBJ_SYSCALL__NR__CLOSE, close__);
    int obj_fd = wl_miniobj_create(100, 100, false);
    EXPECT_EQ(obj_fd, 0);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
}

TEST(miniobj, create_reg_close) {
    wl_miniobj_reg(100, WL_MINIOBJ_SYSCALL__NR__CLOSE, close__);
    int obj_fd = wl_miniobj_create(100, 100, false);
    EXPECT_EQ(obj_fd, 0);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_syscall__close(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
}

TEST(miniobj, duplicate_1) {
    wl_miniobj_reg(100, WL_MINIOBJ_SYSCALL__NR__CLOSE, close__);
    // underlying data is not reference counted, returned object itself is ref counted
    int obj_fd = wl_miniobj_create(100, 100, false);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    int new_obj_fd = wl_miniobj_duplicate(obj_fd);
    int new_obj_fd2 = wl_miniobj_duplicate(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(obj_fd, 0);
    EXPECT_EQ(new_obj_fd, 1);
    EXPECT_EQ(new_obj_fd2, 2);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_syscall__close(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_syscall__close(new_obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_syscall__close(new_obj_fd2);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
}

TEST(miniobj, duplicate_2) {
    wl_miniobj_reg(100, WL_MINIOBJ_SYSCALL__NR__CLOSE, close__);
    // underlying data is reference counted, returned object itself is not ref counted
    int obj_fd = wl_miniobj_create(100, 100, true);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    int new_obj_fd = wl_miniobj_duplicate(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 2);
    EXPECT_EQ(obj_fd, 0);
    EXPECT_EQ(new_obj_fd, 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_syscall__close(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 2);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
    wl_syscall__close(new_obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 2);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 2);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 2);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 2);
}

TEST(miniobj, duplicate_3) {
    wl_miniobj_reg(100, WL_MINIOBJ_SYSCALL__NR__CLOSE, close__);
    // underlying data is not reference counted, returned object itself is ref counted
    int obj_fd = wl_miniobj_create(100, 100, false);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    int new_obj_fd = wl_miniobj_duplicate(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(obj_fd, 0);
    EXPECT_EQ(new_obj_fd, 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
}

TEST(miniobj, duplicate_4) {
    wl_miniobj_reg(100, WL_MINIOBJ_SYSCALL__NR__CLOSE, close__);
    // underlying data is reference counted, returned object itself is not ref counted
    int obj_fd = wl_miniobj_create(100, 100, true);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    int new_obj_fd = wl_miniobj_duplicate(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 2);
    EXPECT_EQ(obj_fd, 0);
    EXPECT_EQ(new_obj_fd, 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 2);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 2);
}

WL_MINIOBJ__DECL_CALLBACK(close__2, a, b, c) {
    if (wl_miniobj_should_destroy(a)) {
        wl_miniobj* obj = wl_miniobj_get(a);
        EXPECT_EQ(obj->data, 5);
        obj->data = 0;
    }
    wl_miniobj_destroy(a, NULL);
    return 0;
}

TEST(miniobj, duplicate_5) {
    wl_miniobj_reg(100, WL_MINIOBJ_SYSCALL__NR__CLOSE, close__2);
    // underlying data is not reference counted, returned object itself is ref counted
    int obj_fd = wl_miniobj_create(100, 100, false);
    wl_miniobj * obj = wl_miniobj_get(obj_fd);
    obj->data = 5;
    EXPECT_EQ(obj->data, 5);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    int new_obj_fd = wl_miniobj_duplicate(obj_fd);
    int new_obj_fd2 = wl_miniobj_duplicate(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(obj_fd, 0);
    EXPECT_EQ(new_obj_fd, 1);
    EXPECT_EQ(new_obj_fd2, 2);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    EXPECT_EQ(obj->data, 5);
    wl_syscall__close(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    EXPECT_EQ(obj->data, 5);
    wl_syscall__close(new_obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    EXPECT_EQ(obj->data, 5);
    wl_syscall__close(new_obj_fd2);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
}

TEST(miniobj, duplicate_6) {
    wl_miniobj_reg(100, WL_MINIOBJ_SYSCALL__NR__CLOSE, close__2);
    // underlying data is not reference counted, data itself is ref counted
    int obj_fd = wl_miniobj_create(100, 100, true);
    wl_miniobj * obj = wl_miniobj_get(obj_fd);
    obj->data = 5;
    EXPECT_EQ(obj->data, 5);
    EXPECT_EQ(wl_miniobj__total_allocations(), 1);
    int new_obj_fd = wl_miniobj_duplicate(obj_fd);
    wl_miniobj * obj2 = wl_miniobj_get(new_obj_fd);
    EXPECT_EQ(obj2->data, 5);
    int new_obj_fd2 = wl_miniobj_duplicate(new_obj_fd);
    wl_miniobj * obj3 = wl_miniobj_get(new_obj_fd2);
    EXPECT_EQ(obj3->data, 5);
    EXPECT_EQ(wl_miniobj__total_allocations(), 3);
    EXPECT_EQ(obj_fd, 0);
    EXPECT_EQ(new_obj_fd, 1);
    EXPECT_EQ(new_obj_fd2, 2);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 0);
    EXPECT_EQ(obj->data, 5);
    EXPECT_EQ(obj2->data, 5);
    EXPECT_EQ(obj3->data, 5);
    wl_syscall__close(obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 3);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 1);
    EXPECT_EQ(obj2->data, 5);
    EXPECT_EQ(obj3->data, 5);
    wl_syscall__close(new_obj_fd);
    EXPECT_EQ(wl_miniobj__total_allocations(), 3);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 2);
    EXPECT_EQ(obj3->data, 5);
    wl_syscall__close(new_obj_fd2);
    EXPECT_EQ(wl_miniobj__total_allocations(), 3);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 3);
    wl_miniobj_deinit();
    EXPECT_EQ(wl_miniobj__total_allocations(), 3);
    EXPECT_EQ(wl_miniobj__total_deallocations(), 3);
}
