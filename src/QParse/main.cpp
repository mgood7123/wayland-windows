#include <Rules_Extra.h>
#include <stdio.h>
#include <fstream>
#include <algorithm>

static std::string readFile(const std::string &fileName)
{
    std::ifstream ifs(fileName.c_str(), std::ios::in | std::ios::binary | std::ios::ate);

    std::ifstream::pos_type fileSize = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    std::vector<char> bytes(fileSize);
    ifs.read(bytes.data(), fileSize);

    return std::string(bytes.data(), fileSize);
}

int main(int argc, char** argv) {
    printf("\n");

    std::string a = "../../wl_syscalls.decl";
    auto content = readFile(a);

    typedef struct Info {
        bool is_typedef;
        std::vector<std::string> comment;
        std::vector<std::string> header;
        std::string current_typedef, current_syscall, current_arguments, current_argument_count, current_arguments_usages;
    } Info;

    Info info;
    info.is_typedef = false;

    std::vector<Info> syscalls;

    using namespace CPP;

    Iterator it = content;

    it.enable_logging = false;

    auto space = new Rules::Or({new Rules::Char(' '), new Rules::Char('\t')});

    auto spaces = new Rules::ZeroOrMore(space);

    auto single_comment = new Rules::Sequence({
        new Rules::String("//"),
        spaces,
        new Rules::Sequence({
            new Rules::Until(new Rules::At(new Rules::NewlineOrEOF), [&](CPP::Rules::Input i) {
                info.comment.push_back(i.string());
            }),
            new Rules::Optional(new Rules::Newline)
        })
    });

    auto block_comment = new Rules::Sequence({
        new Rules::String("#COMMENT_BEGIN"), new Rules::Newline(),
        new Rules::MatchBUntilA(
            new Rules::Sequence({
                new Rules::String("#COMMENT_END"), new Rules::Newline()
            }),
            new Rules::Sequence({
                new Rules::Until(new Rules::At(new Rules::NewlineOrEOF), [&](CPP::Rules::Input i) {
                    info.comment.push_back(i.string());
                }),
                new Rules::Optional(new Rules::Newline)
            })
        )
    });
    
    auto c_ident = Rules_NS_LogTrace1(new Rules::Sequence({
        new Rules::Range({'a', 'z', 'A', 'Z', '_'}),
        new Rules::Optional(new Rules::OneOrMore(new Rules::Range({'a', 'z', 'A', 'Z', '0', '9', '_'})))
    }), "c_ident");

    auto number = Rules_NS_LogTrace1(new Rules::OneOrMore(new Rules::Range({'0', '9'})), "number");

    auto c_value = Rules_NS_LogTrace1(new Rules::Or({c_ident, number}), "c_value");

    auto syscall = new Rules::TemporaryAction(c_ident, [&](CPP::Rules::Input i) {
        std::string sl = i.string();
        std::transform(sl.begin(), sl.end(), sl.begin(), std::tolower);
        if (!info.is_typedef) {
            info.current_syscall = sl;
        } else {
            info.current_typedef = sl;
            info.is_typedef = false;
        }
    });

    auto arguments = new Rules::Sequence({
        new Rules::Char('<'),
        spaces,
        new Rules::Or({
            Rules_NS_LogTrace1(new Rules::Sequence({
                Rules_NS_LogTrace(new Rules::NotAt(
                    new Rules::Or({
                        new Rules::Sequence({
                            new Rules::Char('>'),
                            spaces,
                            new Rules::Or({
                                single_comment,
                                new Rules::Newline()
                            })
                        }),
                        new Rules::String("...")
                    })
                )),
                new Rules::ErrorIfNotMatch(new Rules::TemporaryAction(number, [&](Rules::Input i) { info.current_argument_count = i.string(); }), "expected an integer"),
                new Rules::ErrorIfNotMatch(new Rules::Char(','), "expected comma after number of arguments"),
                spaces,
                Rules_NS_LogTrace1(new Rules::ErrorIfNotMatch(new Rules::Sequence({
                    c_ident,
                    new Rules::Until(new Rules::At(new Rules::Sequence({spaces, new Rules::Char('>')})))
                }, [&](CPP::Rules::Input i) {
                    info.current_arguments = i.string();
                }), "expected argument declarations, followed by closing '>'"), "argument declaration"),
                spaces,
                new Rules::ErrorIfNotMatch(new Rules::Char('>'), "expected closing '>'"),
                spaces,
                new Rules::ErrorIfNotMatch(new Rules::Char('<'), "expected opening '<'"),
                spaces,
                new Rules::ErrorIfNotMatch(new Rules::Sequence({
                    new Rules::Optional(new Rules::Char('&')),
                    c_value,
                    // we cannot use an Until rule here since we need to skip '->' if it occurs
                    Rules_NS_LogTrace1(new Rules::MatchBUntilA(
                        Rules_NS_LogTrace1(new Rules::At(new Rules::Char('>')), "at >"),
                        new Rules::Or({
                            new Rules::Sequence({
                                new Rules::At(new Rules::String("->")),
                                Rules_NS_LogTrace1(new Rules::String("->"), "ignore ->")
                            }),
                            new Rules::Any
                        })
                    ), "argument usages"),
                }, [&](CPP::Rules::Input i) { info.current_arguments_usages = i.string(); }), "expected argument usages, followed by closing '>'"),
            }), "numbered arguments"),
            Rules_NS_LogTrace1(new Rules::If(
                [&]() { return info.current_typedef.length() != 0; },
                new Rules::ErrorIfMatch(
                    new Rules::At(new Rules::String("..."))
                    , "typedef declaration does not support varadic arguments (...)"
                ),
                new Rules::Optional(
                    new Rules::String("...", [&](CPP::Rules::Input i) { info.current_arguments = i.string(); })
                )
            ), "any number of arguments")
        }),
        spaces,
        new Rules::ErrorIfNotMatch(new Rules::Char('>'), "expected closing '>'")
    });

    auto syscall_line_end = new Rules::Or({
        single_comment,
        new Rules::Newline,
        new Rules::At(new Rules::EndOfFile)
    });

    auto syscall_line = new Rules::Sequence({
        syscall,
        spaces,
        new Rules::Optional(Rules_NS_LogTrace(arguments)),
        spaces,
        syscall_line_end,
    });

    auto syscall_line__or__typedef_syscall_line = new Rules::Or({
        new Rules::Sequence({
            new Rules::At(new Rules::Sequence({
                Rules_NS_LogTrace1(syscall, "at syscall"), spaces,
                new Rules::Or({
                    Rules_NS_LogTrace1(new Rules::Char('<'), "at <"),
                    Rules_NS_LogTrace1(syscall_line_end, "at syscall line end")
                })
            })),
            syscall_line
        }),
        new Rules::Sequence({
            new Rules::At(new Rules::Sequence({
                Rules_NS_LogTrace1(new Rules::String("typedef"), "at typedef"),
                spaces,
                Rules_NS_LogTrace1(syscall, "at syscall"), spaces,
                Rules_NS_LogTrace1(syscall, "at syscall"), spaces,
                new Rules::Or({
                    Rules_NS_LogTrace1(new Rules::Char('<'), "at <"),
                    Rules_NS_LogTrace1(syscall_line_end, "at syscall line end")
                })
            })),
            new Rules::String("typedef", [&](CPP::Rules::Input i) { info.is_typedef = true; }),
            spaces,
            new Rules::ErrorIfNotMatch(syscall, "expected syscall"),
            spaces,
            syscall_line
        }),
    }, [&](Rules::Input i) {
        syscalls.push_back(info);
        info.is_typedef = false;
        info.current_typedef.clear();
        info.comment.clear();
        info.comment.shrink_to_fit();
        info.current_syscall.clear();
        info.current_argument_count.clear();
        info.current_arguments.clear();
        info.current_arguments_usages.clear();
    });

    auto empty_line = new Rules::Sequence({
        new Rules::MatchBUntilA(new Rules::At(new Rules::NewlineOrEOF), space),
        new Rules::Optional(new Rules::Newline)
    }, [&](CPP::Rules::Input i) {
        info.comment.clear();
        info.comment.shrink_to_fit();
    });

    if (!Rules::MatchBUntilA(
        new Rules::EndOfFile,
        new Rules::ErrorIfNotMatch(
            new Rules::Or({
                single_comment,
                block_comment,
                empty_line,
                Rules_NS_LogTrace(syscall_line__or__typedef_syscall_line),
            }),
            "expected a comment, empty line, typedef syscall, or a syscall declaration"
        )
    ).match(it)) {
        printf("failed to parse syscalls.decl\n");
        return -1;
    }

    if (syscalls.empty()) {
        printf("no syscalls specified in syscalls.decl\n");
        return 0;
    }

    size_t len = syscalls.size();

    FILE* header = NULL;
    FILE* code = NULL;
    char buf[4096];
    int e = 0;
    
    e = fopen_s(&header, "../../wl_syscalls__fd_allocator.h", "wt");
    if (header == NULL) {
        memset(buf, 0, sizeof(char[4096]));
        strerror_s(buf, e);
        printf("failed to open '../../wl_syscalls__fd_allocator.h', errno = %s\n", buf);
        return 0;
    }
    e = fopen_s(&code, "../../wl_syscalls__fd_allocator.cpp", "wt");
    if (code == NULL) {
        memset(buf, 0, sizeof(char[4096]));
        strerror_s(buf, e);
        printf("failed to open '../../wl_syscalls__fd_allocator.cpp', errno = %s\n", buf);
        fclose(header);
        return 0;
    }

    fprintf(header, R"(#ifndef WL_SYSCALLS_FD_ALLOCATOR_H
#define WL_SYSCALLS_FD_ALLOCATOR_H

#include <stddef.h>

// set this to true to enable printf debugging
extern
#ifdef __cplusplus
"C"
#endif
bool wl_miniobj_debug;

typedef int wl_syscalls__fd_allocator__size_t;
#define wl_syscalls__fd_allocator__size_t_MAX INT_MAX

typedef struct wl_syscalls__fd_allocator {
    void* used;
    void* recycled;
} wl_syscalls__fd_allocator;

typedef void (*WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA)(size_t index, void ** data, bool in_destructor);

#ifdef __cplusplus
extern "C" {
#endif

wl_syscalls__fd_allocator * wl_syscalls__fd_allocator__create(void);
void                        wl_syscalls__fd_allocator__destroy(wl_syscalls__fd_allocator * fd);
int                         wl_syscalls__fd_allocator__allocate_fd(
                                wl_syscalls__fd_allocator * wl_syscalls__fd_allocator, void * data, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback
                            );
size_t                      wl_syscalls__fd_allocator__size(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator);
size_t                      wl_syscalls__fd_allocator__capacity(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator);
bool                        wl_syscalls__fd_allocator__fd_is_valid(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator, int fd);
void *                      wl_syscalls__fd_allocator__get_value_from_fd(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator, int fd);
void                        wl_syscalls__fd_allocator__deallocate_fd(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator, int fd);

void* KNHeap__create(void);
void  KNHeap__destroy(void* instance);
int   KNHeap__getSize(void* instance);
void  KNHeap__getMin(void* instance, wl_syscalls__fd_allocator__size_t *key, void* *value);
void  KNHeap__deleteMin(void* instance, wl_syscalls__fd_allocator__size_t *key, void* *value);
void  KNHeap__insert(void* instance, wl_syscalls__fd_allocator__size_t key, void* value);

void*  ShrinkingVectorIndexAllocator__create(void);
void   ShrinkingVectorIndexAllocator__destroy(void* instance);
size_t ShrinkingVectorIndexAllocator__size(void* instance);
size_t ShrinkingVectorIndexAllocator__capacity(void* instance);
size_t ShrinkingVectorIndexAllocator__add(void* instance, void* value, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback);
size_t ShrinkingVectorIndexAllocator__reuse(void* instance, size_t index, void* value, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback);
bool   ShrinkingVectorIndexAllocator__index_is_valid(void* instance, size_t index);
void*  ShrinkingVectorIndexAllocator__data(void* instance, size_t index);
bool   ShrinkingVectorIndexAllocator__remove(void* instance, int index);

#ifdef __cplusplus
}
#endif

#endif // WL_SYSCALLS_FD_ALLOCATOR_H
)");

    fprintf(code, R"(#include "wl_syscalls__fd_allocator.h"
#include "wl_syscalls__fd_allocator.h"
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

bool wl_miniobj_debug = false;

#include <stdio.h>

// INTEGER FILE DESCRIPTOR ALLOCATOR

#include <cstdbool> // bool
#include <cstdio>   // printf
#include <cstring>  // malloc
#include <vector>   // vector

static void WL_SYSCALLS_FD_ALLOCATOR__DESTROY_DATA_CALLBACK__DO_NOTHING(size_t index, void** unused, bool in_destructor) {}

class ShrinkingVectorIndexAllocator {
  public:
    class Holder {
        public:

        bool used;
        void * data;
        size_t index;
        WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback;

        Holder(void* data, size_t index, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback)
        : used(false), data(data), index(index), callback(callback == nullptr ? WL_SYSCALLS_FD_ALLOCATOR__DESTROY_DATA_CALLBACK__DO_NOTHING : callback)
        {}

        Holder(void) : Holder(nullptr, 0, WL_SYSCALLS_FD_ALLOCATOR__DESTROY_DATA_CALLBACK__DO_NOTHING)
        {}

        void destroy(void) {
            if (callback != NULL) {
                callback(index, &data, false);
                callback = NULL;
            }
        }

        ~Holder(void) {
            if (callback != NULL) {
                callback(index, &data, true);
                callback = NULL;
            }
        }
    };

    class Chunk {
        public:
        
        Holder* data;
        size_t size;
        size_t capacity;

        Chunk(Holder* data, size_t size, size_t capacity)
         :
            data(data),
            size(size),
            capacity(capacity)
        {};

         Chunk(Chunk && old) {
            data = old.data;
            size = old.size;
            capacity = old.capacity;
            old.data = nullptr;
            old.size = 0;
            old.capacity = 0;
         }

         Chunk & operator=(Chunk && old) {
            data = old.data;
            size = old.size;
            capacity = old.capacity;
            old.data = nullptr;
            old.size = 0;
            old.capacity = 0;
            return *this;
         }

         ~Chunk(void) {
            delete[] data;
         }
    };

    std::vector<Chunk> chunks;
    size_t current_chunk_index;
    size_t total_size;
    size_t total_capacity;

    int get_chunk(size_t i) {
        return (8*sizeof(size_t) - __builtin_clzll(i + 2) - 1) - 1;
    }

    size_t get_chunk_subindex(size_t i, int chunk) {
        return i - (1 << (chunk + 1)) + 2;
    }

    void print_chunk_and_index(size_t i) {
        int chunk = get_chunk(i);
        printf("index: %%zu, chunk: %%d, sub array index: %%zu\n", i, chunk, get_chunk_subindex(i, chunk));
    }
    
    public:
    ShrinkingVectorIndexAllocator(void) {
        chunks = std::vector<Chunk>();
        current_chunk_index = 0;
        total_size = 0;
        total_capacity = 0;
    }
    size_t size(void) { return total_size; }
    size_t capacity(void) { return total_capacity; }
    void* operator[] (size_t value) {
        int CI = get_chunk(value);
        size_t DI = get_chunk_subindex(value, CI);
        return chunks[CI].data[DI].data;
    }

    void print(void) {
        if (total_capacity == 0) {
            printf("array = nullptr\n");
        } else {
            printf("array (size %%zu, capacity %%zu, chunk size: %%d, chunk capacity: %%d)\n", total_size, total_capacity, (int)chunks.size(), (int)chunks.capacity());
            for(int i = 0; i < chunks.size(); i++) {
                printf("  chunk %%d (size %%zu, capacity %%zu) = [", i, chunks[i].size, chunks[i].capacity);
                for(size_t i_ = 0; i_ < chunks[i].capacity; i_++) {
                    if (i_ != 0) printf(", ");
                    printf("%%zu", i_);
                }
                printf("]\n");
            }
        }
    }

    size_t add(void* value, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback) {
        if (total_capacity == 0) {
            chunks.reserve(1);
            chunks.emplace_back(Chunk(new Holder[2], 1, 2));
            chunks[0].data[0].used = true;
            chunks[0].data[0].data = value;
            chunks[0].data[0].index = total_size;
            chunks[0].data[0].callback = callback;
            total_size ++;
            total_capacity += 2;
            current_chunk_index = 0;
            if (wl_miniobj_debug) printf("added, total_size: %%zu, total_capacity: %%zu\n", total_size, total_capacity);
            return total_size-1;
        } else {
            if (chunks[current_chunk_index].size == chunks[current_chunk_index].capacity) {
                size_t cap = chunks[current_chunk_index].capacity*2;
                chunks.reserve(current_chunk_index+2);
                chunks.emplace_back(Chunk(new Holder[cap], 0, cap));
                current_chunk_index++;
                chunks[current_chunk_index].data[0].used = true;
                chunks[current_chunk_index].data[0].data = value;
                chunks[current_chunk_index].data[0].index = total_size;
                chunks[current_chunk_index].data[0].callback = callback;
                chunks[current_chunk_index].size = 1;
                total_size ++;
                total_capacity += chunks[current_chunk_index].capacity;
                if (wl_miniobj_debug) printf("added, total_size: %%zu, total_capacity: %%zu\n", total_size, total_capacity);
                return total_size-1;
            } else {
                chunks[current_chunk_index].data[chunks[current_chunk_index].size].used = true;
                chunks[current_chunk_index].data[chunks[current_chunk_index].size].data = value;
                chunks[current_chunk_index].data[chunks[current_chunk_index].size].index = total_size;
                chunks[current_chunk_index].data[chunks[current_chunk_index].size].callback = callback;
                chunks[current_chunk_index].size++;
                total_size ++;
                if (wl_miniobj_debug) printf("added, total_size: %%zu, total_capacity: %%zu\n", total_size, total_capacity);
                return total_size-1;
            }
        }
        return -1;
    }

    size_t reuse(size_t index, void * data, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback) {
        if (total_capacity == 0) {
            return -1;
        }
        int CI = get_chunk(index);
        size_t DI = get_chunk_subindex(index, CI);
        chunks[CI].data[DI].used = true;
        chunks[CI].data[DI].data = data;
        chunks[CI].data[DI].callback = callback;
        chunks[CI].size++;
        total_size++;
        if (wl_miniobj_debug) printf("reuse, total_size: %%zu, total_capacity: %%zu\n", total_size, total_capacity);
        return index;
    }

    Holder* index_if_valid(size_t index, int * CI, size_t * DI) {
        if (total_capacity == 0) {
            if (wl_miniobj_debug) printf("index invalid: %%zu (total capazity is zero), false\n", index);
            return NULL;
        }
        *CI = get_chunk(index);
        size_t s = chunks.size();
        if (*CI >= s) {
            if (wl_miniobj_debug) printf("index invalid: %%zu (CI %%i >= %%zu), false\n", index, *CI, s);
            return NULL;
        }
        *DI = get_chunk_subindex(index, *CI);
        if (*DI >= chunks[*CI].capacity) {
            if (wl_miniobj_debug) printf("index invalid: %%zu (DI %%zu >= %%zu), false\n", index, *DI, chunks[*CI].capacity);
            return NULL;
        }
        if (chunks[*CI].data[*DI].used) {
            if (wl_miniobj_debug) printf("index valid: %%zu, true\n", index);
            return &chunks[*CI].data[*DI];
        }
        if (wl_miniobj_debug) printf("index invalid: %%zu, false\n", index);
        return NULL;
    }

    bool remove(size_t index) {
        if (total_capacity == 0) {
            return false;
        }
        int CI;
        size_t DI;
        Holder * holder = index_if_valid(index, &CI, &DI);
        if (holder == NULL) {
            return false;
        }
        if (wl_miniobj_debug) printf("removing %%zu, CI: %%d, DI: %%zu, total_size: %%zu, total_capacity: %%zu, chunks size: %%zu\n", index, CI, DI, total_size, total_capacity, chunks.size());
        chunks[CI].data[DI].destroy();
        chunks[CI].data[DI].used = false;
        chunks[CI].size--;
        total_size--;
        if (chunks[CI].size == 0) {
            total_capacity -= chunks[CI].capacity;
            if (total_capacity == 0) {
                chunks.clear();
                chunks.shrink_to_fit();
            } else {
                if (chunks[chunks.size()-1].size == 0) {
                    int CII = chunks.size()-1;
                    while(true) {
                        if (chunks[CII].size != 0) {
                            break;
                        }
                        current_chunk_index--;
                        chunks.erase(chunks.cbegin() + CII);
                        chunks.shrink_to_fit();
                        CII--;
                    }
                }
            }
        }
        return true;
    }
};

// INTEGER FILE DESCRIPTOR RECYCLER

// C++ needed for implementation

// this files contains all the application independent little
// functions and macros used for the optimizer.
// In particular Peters debug macros and Dags stuff
// from dbasic.h cdefs, random,...

//////////////// stuff originally from debug.h ///////////////////////////////
// (c) 1997 Peter Sanders
// some little utilities for debugging adapted
// to the paros conventions


#ifndef UTIL
#define UTIL

#define Assert(c)
#define Assert2(c)
#define Debug4(c)

////////////// min, max etc. //////////////////////////////////////

#ifndef Max
#define Max(x,y) ((x)>=(y)?(x):(y))
#endif

#ifndef Min
#define Min(x,y) ((x)<=(y)?(x):(y))
#endif

#ifndef Abs
#define Abs(x) ((x) < 0 ? -(x) : (x))
#endif

#ifndef PI
#define PI 3.1415927
#endif

// is this the right definition of limit?
inline double limit(double x, double bound)
{
  if      (x >  bound) { return  bound; }
  else if (x < -bound) { return -bound; }
  else                   return x;
}

#endif

// hierarchical memory priority queue data structure
#ifndef KNHEAP
#define KNHEAP

const int KNBufferSize1 = 32; // equalize procedure call overheads etc.
const int KNN = 512; // bandwidth
const int KNKMAX = 64;  // maximal arity
const int KNLevels = 4; // overall capacity >= KNN*KNKMAX^KNLevels
/*
const int KNBufferSize1 = 3; // equalize procedure call overheads etc.
const int KNN = 10; // bandwidth
const int KNKMAX = 4;  // maximal arity
const int KNLevels = 4; // overall capacity >= KNN*KNKMAX^KNLevels
*/
template <class Key, class Value>
struct KNElement {Key key; Value value;};

//////////////////////////////////////////////////////////////////////
// fixed size binary heap
template <class Key, class Value, int capacity>
class BinaryHeap {
  //  static const Key infimum  = 4;
  //static const Key supremum = numeric_limits<Key>.max();
  typedef KNElement<Key, Value> Element;
  Element data[capacity + 2];
  int size;  // index of last used element
public:
  BinaryHeap(Key sup, Key infimum):size(0) {
    data[0].key = infimum; // sentinel
    data[capacity + 1].key = sup;
    reset();
  }
  Key getSupremum(void) { return data[capacity + 1].key; }
  void reset(void);
  int   getSize(void)     const { return size; }
  Key   getMinKey(void)   const { return data[1].key; }
  Value getMinValue(void) const { return data[1].value; }
  void  deleteMin(void);
  void  deleteMinFancy(Key *key, Value *value) {
    *key   = getMinKey(void);
    *value = getMinValue(void);
    deleteMin(void);
  }
  void  insert(Key k, Value v);
  void  sortTo(Element *to); // sort in increasing order and empty
  //void  sortInPlace(void); // in decreasing order
};


// reset size to 0 and fill data array with sentinels
template <class Key, class Value, int capacity>
inline void BinaryHeap<Key, Value, capacity>::
reset(void) {
  size = 0;
  Key sup = getSupremum();
  for (int i = 1;  i <= capacity;  i++) {
    data[i].key = sup;
  }
  // if this becomes a bottle neck
  // we might want to replace this by log KNN
  // memcpy-s
}

template <class Key, class Value, int capacity>
inline void BinaryHeap<Key, Value, capacity>::
deleteMin(void)
{
  Assert2(size > 0);

  // first move up elements on a min-path
  int hole = 1;
  int succ = 2;
  int sz   = size;
  while (succ < sz) {
    Key key1 = data[succ].key;
    Key key2 = data[succ + 1].key;
    if (key1 > key2) {
      succ++;
      data[hole].key   = key2;
      data[hole].value = data[succ].value;
    } else {
      data[hole].key   = key1;
      data[hole].value = data[succ].value;
    }
    hole = succ;
    succ <<= 1;
  }

  // bubble up rightmost element
  Key bubble = data[sz].key;
  int pred = hole >> 1;
  while (data[pred].key > bubble) { // must terminate since min at root
    data[hole] = data[pred];
    hole = pred;
    pred >>= 1;
  }

  // finally move data to hole
  data[hole].key = bubble;
  data[hole].value = data[sz].value;

  data[size].key = getSupremum(); // mark as deleted
  size = sz - 1;
}


// empty the heap and put the element to "to"
// sorted in increasing order
template <class Key, class Value, int capacity>
inline void BinaryHeap<Key, Value, capacity>::
sortTo(Element *to)
{
  const int           sz = size;
  const Key          sup = getSupremum();
  Element * const beyond = to + sz;
  Element * const root   = data + 1;
  while (to < beyond) {
    // copy minimun
    *to = *root;
    to++;

    // bubble up second smallest as in deleteMin
    int hole = 1;
    int succ = 2;
    while (succ <= sz) {
      Key key1 = data[succ    ].key;
      Key key2 = data[succ + 1].key;
      if (key1 > key2) {
        succ++;
        data[hole].key   = key2;
        data[hole].value = data[succ].value;
      } else {
        data[hole].key = key1;
        data[hole].value = data[succ].value;
      }
      hole = succ;
      succ <<= 1;
    }

    // just mark hole as deleted
    data[hole].key = sup;
  }
  size = 0;
}


template <class Key, class Value, int capacity>
inline void BinaryHeap<Key, Value, capacity>::
insert(Key k, Value v)
{
  Assert2(size < capacity);

  size++;
  int hole = size;
  int pred = hole >> 1;
  Key predKey = data[pred].key;
  while (predKey > k) { // must terminate due to sentinel at 0
    data[hole].key   = predKey;
    data[hole].value = data[pred].value;
    hole = pred;
    pred >>= 1;
    predKey = data[pred].key;
  }

  // finally move data to hole
  data[hole].key   = k;
  data[hole].value = v;
}

//////////////////////////////////////////////////////////////////////
// The data structure from Knuth, "Sorting and Searching", Section 5.4.1
template <class Key, class Value>
class KNLooserTree {
  // public: // should not be here but then I would need a scary
  // sequence of template friends which I doubt to work
  // on all compilers
  typedef KNElement<Key, Value> Element;
  struct Entry   {
    Key key;   // Key of Looser element (winner for 0)
    int index; // number of loosing segment
  };

  // stack of empty segments
  int empty[KNKMAX]; // indices of empty segments
  int lastFree;  // where in "empty" is the last valid entry?

  int size; // total number of elements stored
  int logK; // log of current tree size
  int k; // invariant k = 1 << logK

  Element dummy; // target of empty segment pointers

  // upper levels of looser trees
  // entry[0] contains the winner info
  Entry entry[KNKMAX];

  // leaf information
  // note that Knuth uses indices k..k-1
  // while we use 0..k-1
  Element *current[KNKMAX]; // pointer to actual element
  Element *segment[KNKMAX]; // start of Segments

  // private member functions
  int initWinner(int root);
  void updateOnInsert(int node, Key newKey, int newIndex,
                      Key *winnerKey, int *winnerIndex, int *mask);
  void deallocateSegment(int index);
  void doubleK(void);
  void compactTree(void);
  void rebuildLooserTree(void);
  int segmentIsEmpty(int i);
public:
  KNLooserTree(void);
  void init(Key sup); // before, no consistent state is reached :-(

  void multiMergeUnrolled3(Element *to, int l);
  void multiMergeUnrolled4(Element *to, int l);
  void multiMergeUnrolled5(Element *to, int l);
  void multiMergeUnrolled6(Element *to, int l);
  void multiMergeUnrolled7(Element *to, int l);
  void multiMergeUnrolled8(Element *to, int l);
  void multiMergeUnrolled9(Element *to, int l);
  void multiMergeUnrolled10(Element *to, int l);

  void multiMerge(Element *to, int l); // delete l smallest element to "to"
  void multiMergeK(Element *to, int l);
  int  spaceIsAvailable(void) { return k < KNKMAX || lastFree >= 0; }
     // for new segment
  void insertSegment(Element *to, int sz); // insert segment beginning at to
  int  getSize(void) { return size; }
  Key getSupremum(void) { return dummy.key; }
};


//////////////////////////////////////////////////////////////////////
// 2 level multi-merge tree
template <class Key, class Value>
class KNHeap {
  typedef KNElement<Key, Value> Element;

  KNLooserTree<Key, Value> tree[KNLevels];

  // one delete buffer for each tree (extra space for sentinel)
  Element buffer2[KNLevels][KNN + 1]; // tree->buffer2->buffer1
  Element *minBuffer2[KNLevels];

  // overall delete buffer
  Element buffer1[KNBufferSize1 + 1];
  Element *minBuffer1;

  // insert buffer
  BinaryHeap<Key, Value, KNN> insertHeap;

  // how many levels are active
  int activeLevels;

  // total size not counting insertBuffer and buffer1
  int size;

  // private member functions
  void refillBuffer1(void);
  void refillBuffer11(int sz);
  void refillBuffer12(int sz);
  void refillBuffer13(int sz);
  void refillBuffer14(int sz);
  int refillBuffer2(int k);
  int makeSpaceAvailable(int level);
  void emptyInsertHeap(void);
  Key getSupremum(void) const { return buffer2[0][KNN].key; }
  int getSize1(void) const { return ( buffer1 + KNBufferSize1) - minBuffer1; }
  int getSize2(int i) const { return &(buffer2[i][KNN])     - minBuffer2[i]; }
public:
  KNHeap(Key sup, Key infimum);
  int   getSize(void) const;
  void  getMin(Key *key, Value *value);
  void  deleteMin(Key *key, Value *value);
  void  insert(Key key, Value value);
};


template <class Key, class Value>
inline int KNHeap<Key, Value>::getSize(void) const
{
  return
    size +
    insertHeap.getSize() +
    ((buffer1 + KNBufferSize1) - minBuffer1);
}

template <class Key, class Value>
inline void  KNHeap<Key, Value>::getMin(Key *key, Value *value) {
  Key key1 = minBuffer1->key;
  Key key2 = insertHeap.getMinKey();
  if (key2 >= key1) {
    *key   = key1;
    *value = minBuffer1->value;
  } else {
    *key   = key2;
    *value = insertHeap.getMinValue();
  }
}

template <class Key, class Value>
inline void  KNHeap<Key, Value>::deleteMin(Key *key, Value *value) {
  Key key1 = minBuffer1->key;
  Key key2 = insertHeap.getMinKey();
  if (key2 >= key1) {
    *key   = key1;
    *value = minBuffer1->value;
    Assert2(minBuffer1 < buffer1 + KNBufferSize1); // no delete from empty
    minBuffer1++;
    if (minBuffer1 == buffer1 + KNBufferSize1) {
      refillBuffer1();
    }
  } else {
    *key = key2;
    *value = insertHeap.getMinValue();
    insertHeap.deleteMin();
  }
}

template <class Key, class Value>
inline  void  KNHeap<Key, Value>::insert(Key k, Value v) {
  if (insertHeap.getSize() == KNN) { emptyInsertHeap(); }
  insertHeap.insert(k, v);
}
#endif

#include <string.h>
///////////////////////// LooserTree ///////////////////////////////////
template <class Key, class Value>
KNLooserTree<Key, Value>::
KNLooserTree(void) : lastFree(0), size(0), logK(0), k(1)
{
  empty  [0] = 0;
  segment[0] = 0;
  current[0] = &dummy;
  // entry and dummy are initialized by init
  // since they need the value of supremum
}


template <class Key, class Value>
void KNLooserTree<Key, Value>::
init(Key sup)
{
  dummy.key      = sup;
  rebuildLooserTree();
  Assert2(current[entry[0].index] == &dummy);
}


// rebuild looser tree information from the values in current
template <class Key, class Value>
void KNLooserTree<Key, Value>::
rebuildLooserTree(void)
{  
  int winner = initWinner(1);
  entry[0].index = winner;
  entry[0].key   = current[winner]->key;
}


// given any values in the leaves this
// routing recomputes upper levels of the tree
// from scratch in linear time
// initialize entry[root].index and the subtree rooted there
// return winner index
template <class Key, class Value>
int KNLooserTree<Key, Value>::
initWinner(int root)
{
  if (root >= k) { // leaf reached
    return root - k;
  } else {
    int left  = initWinner(2*root    );
    int right = initWinner(2*root + 1);
    Key lk    = current[left ]->key;
    Key rk    = current[right]->key;
    if (lk <= rk) { // right subtree looses
      entry[root].index = right;
      entry[root].key   = rk;
      return left;
    } else {
      entry[root].index = left;
      entry[root].key   = lk;
      return right;
    }
  }
}


// first go up the tree all the way to the root
// hand down old winner for the respective subtree
// based on new value, and old winner and looser 
// update each node on the path to the root top down.
// This is implemented recursively
template <class Key, class Value>
void KNLooserTree<Key, Value>::
updateOnInsert(int node, 
               Key     newKey, int     newIndex, 
               Key *winnerKey, int *winnerIndex, // old winner
               int *mask) // 1 << (ceil(log KNK) - dist-from-root)
{
  if (node == 0) { // winner part of root
    *mask = 1 << (logK - 1);    
    *winnerKey   = entry[0].key;
    *winnerIndex = entry[0].index;
    if (newKey < entry[node].key) {
      entry[node].key   = newKey;
      entry[node].index = newIndex;
    }
  } else {
    updateOnInsert(node >> 1, newKey, newIndex, winnerKey, winnerIndex, mask);
    Key looserKey   = entry[node].key;
    int looserIndex = entry[node].index;
    if ((*winnerIndex & *mask) != (newIndex & *mask)) { // different subtrees
      if (newKey < looserKey) { // newKey will have influence here
        if (newKey < *winnerKey) { // old winner loses here
          entry[node].key   = *winnerKey;
          entry[node].index = *winnerIndex;
        } else { // new entry looses here
          entry[node].key   = newKey;
          entry[node].index = newIndex;
        }
      } 
      *winnerKey   = looserKey;
      *winnerIndex = looserIndex;
    }
    // note that nothing needs to be done if
    // the winner came from the same subtree
    // a) newKey <= winnerKey => even more reason for the other tree to loose
    // b) newKey >  winnerKey => the old winner will beat the new
    //                           entry further down the tree
    // also the same old winner is handed down the tree

    *mask >>= 1; // next level
  }
}


// make the tree two times as wide
// may only be called if no free slots are left ?? necessary ??
template <class Key, class Value>
void KNLooserTree<Key, Value>::
doubleK(void)
{
  // make all new entries empty
  // and push them on the free stack
  Assert2(lastFree == -1); // stack was empty (probably not needed)
  Assert2(k < KNKMAX);
  for (int i = 2*k - 1;  i >= k;  i--) {
    current[i] = &dummy;
    lastFree++;
    empty[lastFree] = i;
  }

  // double the size
  k *= 2;  logK++;

  // recompute looser tree information
  rebuildLooserTree();
}


// compact nonempty segments in the left half of the tree
template <class Key, class Value>
void KNLooserTree<Key, Value>::
compactTree(void)
{
  Assert2(logK > 0);
  Key sup = dummy.key;

  // compact all nonempty segments to the left
  int from = 0;
  int to   = 0;
  for(;  from < k;  from++) {
    if (current[from]->key != sup) {
      current[to] = current[from];
      segment[to] = segment[from];
      to++;
    } 
  }

  // half degree as often as possible
  while (to < k/2) {
    k /= 2;  logK--;
  }

  // overwrite garbage and compact the stack of empty segments
  lastFree = -1; // none free
  for (;  to < k;  to++) {
    // push 
    lastFree++;
    empty[lastFree] = to;

    current[to] = &dummy;
  }

  // recompute looser tree information
  rebuildLooserTree();
}


// insert segment beginning at to
// require: spaceIsAvailable() == 1 
template <class Key, class Value>
void KNLooserTree<Key, Value>::
insertSegment(Element *to, int sz)
{
  if (sz > 0) {
    Assert2(to[0   ].key != getSupremum());
    Assert2(to[sz-1].key != getSupremum());
    // get a free slot
    if (lastFree < 0) { // tree is too small
      doubleK();
    }
    int index = empty[lastFree];
    lastFree--; // pop


    // link new segment
    current[index] = segment[index] = to;
    size += sz;
    
    // propagate new information up the tree
    Key dummyKey;
    int dummyIndex;
    int dummyMask;
    updateOnInsert((index + k) >> 1, to->key, index, 
                   &dummyKey, &dummyIndex, &dummyMask);
  } else {
    // immediately deallocate
    // this is not only an optimization 
    // but also needed to keep empty segments from
    // clogging up the tree
    delete [] to;
  }
}


// free an empty segment
template <class Key, class Value>
void KNLooserTree<Key, Value>::
deallocateSegment(int index)
{
  // reroute current pointer to some empty dummy segment
  // with a sentinel key
  current[index] = &dummy;

  // free memory
  delete [] segment[index];
  segment[index] = 0;
  
  // push on the stack of free segment indices
  lastFree++;
  empty[lastFree] = index;
}

// multi-merge for a fixed K=1<<LogK
// this looks ugly but the include file explains
// why this is the most portable choice
#define LogK 3
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeUnrolled3(Element *to, int l)
// body of the function multiMergeUnrolled
// it will be included multiple times with
// different settings for LogP
// Note that in gcc it is sufficient to simply
// use an addtional argument and to declare things an inline
// function. But I was not able to convince SunCC to
// inline and constant fold it.
// Similarly I tried introducing LogP as a template
// parameter but this did not compile on SunCC
  {
    Element *done = to + l;
    Entry    *regEntry   = entry;
    Element **regCurrent = current;
    int      winnerIndex = regEntry[0].index;
    Key      winnerKey   = regEntry[0].key;
    Element *winnerPos;
    Key sup = dummy.key; // supremum
    
    Assert2(logK >= LogK);
    while (to < done) {
      winnerPos = regCurrent[winnerIndex];
      
      // write result
      to->key   = winnerKey;
      to->value = winnerPos->value;
      
      // advance winner segment
      winnerPos++;
      regCurrent[winnerIndex] = winnerPos;
      winnerKey = winnerPos->key;
      
      // remove winner segment if empty now
      if (winnerKey == sup) { 
        deallocateSegment(winnerIndex); 
      } 
      to++;
      
      // update looser tree
#define TreeStep(L)\
      if (1 << LogK >= 1 << L) {\
        Entry *pos##L = regEntry+((winnerIndex+(1<<LogK)) >> ((LogK-L)+1));\
        Key    key##L = pos##L->key;\
        if (key##L < winnerKey) {\
          int index##L  = pos##L->index;\
          pos##L->key   = winnerKey;\
          pos##L->index = winnerIndex;\
          winnerKey     = key##L;\
          winnerIndex   = index##L;\
        }\
      }
      TreeStep(10);
      TreeStep(9);
      TreeStep(8);
      TreeStep(7);
      TreeStep(6);
      TreeStep(5);
      TreeStep(4);
      TreeStep(3);
      TreeStep(2);
      TreeStep(1);
#undef TreeStep      
    }
    regEntry[0].index = winnerIndex;
    regEntry[0].key   = winnerKey;  
  }
#undef LogK
#define LogK 4
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeUnrolled4(Element *to, int l)
// body of the function multiMergeUnrolled
// it will be included multiple times with
// different settings for LogP
// Note that in gcc it is sufficient to simply
// use an addtional argument and to declare things an inline
// function. But I was not able to convince SunCC to
// inline and constant fold it.
// Similarly I tried introducing LogP as a template
// parameter but this did not compile on SunCC
  {
    Element *done = to + l;
    Entry    *regEntry   = entry;
    Element **regCurrent = current;
    int      winnerIndex = regEntry[0].index;
    Key      winnerKey   = regEntry[0].key;
    Element *winnerPos;
    Key sup = dummy.key; // supremum
    
    Assert2(logK >= LogK);
    while (to < done) {
      winnerPos = regCurrent[winnerIndex];
      
      // write result
      to->key   = winnerKey;
      to->value = winnerPos->value;
      
      // advance winner segment
      winnerPos++;
      regCurrent[winnerIndex] = winnerPos;
      winnerKey = winnerPos->key;
      
      // remove winner segment if empty now
      if (winnerKey == sup) { 
        deallocateSegment(winnerIndex); 
      } 
      to++;
      
      // update looser tree
#define TreeStep(L)\
      if (1 << LogK >= 1 << L) {\
        Entry *pos##L = regEntry+((winnerIndex+(1<<LogK)) >> ((LogK-L)+1));\
        Key    key##L = pos##L->key;\
        if (key##L < winnerKey) {\
          int index##L  = pos##L->index;\
          pos##L->key   = winnerKey;\
          pos##L->index = winnerIndex;\
          winnerKey     = key##L;\
          winnerIndex   = index##L;\
        }\
      }
      TreeStep(10);
      TreeStep(9);
      TreeStep(8);
      TreeStep(7);
      TreeStep(6);
      TreeStep(5);
      TreeStep(4);
      TreeStep(3);
      TreeStep(2);
      TreeStep(1);
#undef TreeStep      
    }
    regEntry[0].index = winnerIndex;
    regEntry[0].key   = winnerKey;  
  }
#undef LogK
#define LogK 5
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeUnrolled5(Element *to, int l)
// body of the function multiMergeUnrolled
// it will be included multiple times with
// different settings for LogP
// Note that in gcc it is sufficient to simply
// use an addtional argument and to declare things an inline
// function. But I was not able to convince SunCC to
// inline and constant fold it.
// Similarly I tried introducing LogP as a template
// parameter but this did not compile on SunCC
  {
    Element *done = to + l;
    Entry    *regEntry   = entry;
    Element **regCurrent = current;
    int      winnerIndex = regEntry[0].index;
    Key      winnerKey   = regEntry[0].key;
    Element *winnerPos;
    Key sup = dummy.key; // supremum
    
    Assert2(logK >= LogK);
    while (to < done) {
      winnerPos = regCurrent[winnerIndex];
      
      // write result
      to->key   = winnerKey;
      to->value = winnerPos->value;
      
      // advance winner segment
      winnerPos++;
      regCurrent[winnerIndex] = winnerPos;
      winnerKey = winnerPos->key;
      
      // remove winner segment if empty now
      if (winnerKey == sup) { 
        deallocateSegment(winnerIndex); 
      } 
      to++;
      
      // update looser tree
#define TreeStep(L)\
      if (1 << LogK >= 1 << L) {\
        Entry *pos##L = regEntry+((winnerIndex+(1<<LogK)) >> ((LogK-L)+1));\
        Key    key##L = pos##L->key;\
        if (key##L < winnerKey) {\
          int index##L  = pos##L->index;\
          pos##L->key   = winnerKey;\
          pos##L->index = winnerIndex;\
          winnerKey     = key##L;\
          winnerIndex   = index##L;\
        }\
      }
      TreeStep(10);
      TreeStep(9);
      TreeStep(8);
      TreeStep(7);
      TreeStep(6);
      TreeStep(5);
      TreeStep(4);
      TreeStep(3);
      TreeStep(2);
      TreeStep(1);
#undef TreeStep      
    }
    regEntry[0].index = winnerIndex;
    regEntry[0].key   = winnerKey;  
  }
#undef LogK
#define LogK 6
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeUnrolled6(Element *to, int l)
// body of the function multiMergeUnrolled
// it will be included multiple times with
// different settings for LogP
// Note that in gcc it is sufficient to simply
// use an addtional argument and to declare things an inline
// function. But I was not able to convince SunCC to
// inline and constant fold it.
// Similarly I tried introducing LogP as a template
// parameter but this did not compile on SunCC
  {
    Element *done = to + l;
    Entry    *regEntry   = entry;
    Element **regCurrent = current;
    int      winnerIndex = regEntry[0].index;
    Key      winnerKey   = regEntry[0].key;
    Element *winnerPos;
    Key sup = dummy.key; // supremum
    
    Assert2(logK >= LogK);
    while (to < done) {
      winnerPos = regCurrent[winnerIndex];
      
      // write result
      to->key   = winnerKey;
      to->value = winnerPos->value;
      
      // advance winner segment
      winnerPos++;
      regCurrent[winnerIndex] = winnerPos;
      winnerKey = winnerPos->key;
      
      // remove winner segment if empty now
      if (winnerKey == sup) { 
        deallocateSegment(winnerIndex); 
      } 
      to++;
      
      // update looser tree
#define TreeStep(L)\
      if (1 << LogK >= 1 << L) {\
        Entry *pos##L = regEntry+((winnerIndex+(1<<LogK)) >> ((LogK-L)+1));\
        Key    key##L = pos##L->key;\
        if (key##L < winnerKey) {\
          int index##L  = pos##L->index;\
          pos##L->key   = winnerKey;\
          pos##L->index = winnerIndex;\
          winnerKey     = key##L;\
          winnerIndex   = index##L;\
        }\
      }
      TreeStep(10);
      TreeStep(9);
      TreeStep(8);
      TreeStep(7);
      TreeStep(6);
      TreeStep(5);
      TreeStep(4);
      TreeStep(3);
      TreeStep(2);
      TreeStep(1);
#undef TreeStep      
    }
    regEntry[0].index = winnerIndex;
    regEntry[0].key   = winnerKey;  
  }
#undef LogK
#define LogK 7
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeUnrolled7(Element *to, int l)
// body of the function multiMergeUnrolled
// it will be included multiple times with
// different settings for LogP
// Note that in gcc it is sufficient to simply
// use an addtional argument and to declare things an inline
// function. But I was not able to convince SunCC to
// inline and constant fold it.
// Similarly I tried introducing LogP as a template
// parameter but this did not compile on SunCC
  {
    Element *done = to + l;
    Entry    *regEntry   = entry;
    Element **regCurrent = current;
    int      winnerIndex = regEntry[0].index;
    Key      winnerKey   = regEntry[0].key;
    Element *winnerPos;
    Key sup = dummy.key; // supremum
    
    Assert2(logK >= LogK);
    while (to < done) {
      winnerPos = regCurrent[winnerIndex];
      
      // write result
      to->key   = winnerKey;
      to->value = winnerPos->value;
      
      // advance winner segment
      winnerPos++;
      regCurrent[winnerIndex] = winnerPos;
      winnerKey = winnerPos->key;
      
      // remove winner segment if empty now
      if (winnerKey == sup) { 
        deallocateSegment(winnerIndex); 
      } 
      to++;
      
      // update looser tree
#define TreeStep(L)\
      if (1 << LogK >= 1 << L) {\
        Entry *pos##L = regEntry+((winnerIndex+(1<<LogK)) >> ((LogK-L)+1));\
        Key    key##L = pos##L->key;\
        if (key##L < winnerKey) {\
          int index##L  = pos##L->index;\
          pos##L->key   = winnerKey;\
          pos##L->index = winnerIndex;\
          winnerKey     = key##L;\
          winnerIndex   = index##L;\
        }\
      }
      TreeStep(10);
      TreeStep(9);
      TreeStep(8);
      TreeStep(7);
      TreeStep(6);
      TreeStep(5);
      TreeStep(4);
      TreeStep(3);
      TreeStep(2);
      TreeStep(1);
#undef TreeStep      
    }
    regEntry[0].index = winnerIndex;
    regEntry[0].key   = winnerKey;  
  }
#undef LogK
#define LogK 8
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeUnrolled8(Element *to, int l)
// body of the function multiMergeUnrolled
// it will be included multiple times with
// different settings for LogP
// Note that in gcc it is sufficient to simply
// use an addtional argument and to declare things an inline
// function. But I was not able to convince SunCC to
// inline and constant fold it.
// Similarly I tried introducing LogP as a template
// parameter but this did not compile on SunCC
  {
    Element *done = to + l;
    Entry    *regEntry   = entry;
    Element **regCurrent = current;
    int      winnerIndex = regEntry[0].index;
    Key      winnerKey   = regEntry[0].key;
    Element *winnerPos;
    Key sup = dummy.key; // supremum
    
    Assert2(logK >= LogK);
    while (to < done) {
      winnerPos = regCurrent[winnerIndex];
      
      // write result
      to->key   = winnerKey;
      to->value = winnerPos->value;
      
      // advance winner segment
      winnerPos++;
      regCurrent[winnerIndex] = winnerPos;
      winnerKey = winnerPos->key;
      
      // remove winner segment if empty now
      if (winnerKey == sup) { 
        deallocateSegment(winnerIndex); 
      } 
      to++;
      
      // update looser tree
#define TreeStep(L)\
      if (1 << LogK >= 1 << L) {\
        Entry *pos##L = regEntry+((winnerIndex+(1<<LogK)) >> ((LogK-L)+1));\
        Key    key##L = pos##L->key;\
        if (key##L < winnerKey) {\
          int index##L  = pos##L->index;\
          pos##L->key   = winnerKey;\
          pos##L->index = winnerIndex;\
          winnerKey     = key##L;\
          winnerIndex   = index##L;\
        }\
      }
      TreeStep(10);
      TreeStep(9);
      TreeStep(8);
      TreeStep(7);
      TreeStep(6);
      TreeStep(5);
      TreeStep(4);
      TreeStep(3);
      TreeStep(2);
      TreeStep(1);
#undef TreeStep      
    }
    regEntry[0].index = winnerIndex;
    regEntry[0].key   = winnerKey;  
  }
#undef LogK
#define LogK 9
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeUnrolled9(Element *to, int l)
// body of the function multiMergeUnrolled
// it will be included multiple times with
// different settings for LogP
// Note that in gcc it is sufficient to simply
// use an addtional argument and to declare things an inline
// function. But I was not able to convince SunCC to
// inline and constant fold it.
// Similarly I tried introducing LogP as a template
// parameter but this did not compile on SunCC
  {
    Element *done = to + l;
    Entry    *regEntry   = entry;
    Element **regCurrent = current;
    int      winnerIndex = regEntry[0].index;
    Key      winnerKey   = regEntry[0].key;
    Element *winnerPos;
    Key sup = dummy.key; // supremum
    
    Assert2(logK >= LogK);
    while (to < done) {
      winnerPos = regCurrent[winnerIndex];
      
      // write result
      to->key   = winnerKey;
      to->value = winnerPos->value;
      
      // advance winner segment
      winnerPos++;
      regCurrent[winnerIndex] = winnerPos;
      winnerKey = winnerPos->key;
      
      // remove winner segment if empty now
      if (winnerKey == sup) { 
        deallocateSegment(winnerIndex); 
      } 
      to++;
      
      // update looser tree
#define TreeStep(L)\
      if (1 << LogK >= 1 << L) {\
        Entry *pos##L = regEntry+((winnerIndex+(1<<LogK)) >> ((LogK-L)+1));\
        Key    key##L = pos##L->key;\
        if (key##L < winnerKey) {\
          int index##L  = pos##L->index;\
          pos##L->key   = winnerKey;\
          pos##L->index = winnerIndex;\
          winnerKey     = key##L;\
          winnerIndex   = index##L;\
        }\
      }
      TreeStep(10);
      TreeStep(9);
      TreeStep(8);
      TreeStep(7);
      TreeStep(6);
      TreeStep(5);
      TreeStep(4);
      TreeStep(3);
      TreeStep(2);
      TreeStep(1);
#undef TreeStep      
    }
    regEntry[0].index = winnerIndex;
    regEntry[0].key   = winnerKey;  
  }
#undef LogK
#define LogK 10
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeUnrolled10(Element *to, int l)
// body of the function multiMergeUnrolled
// it will be included multiple times with
// different settings for LogP
// Note that in gcc it is sufficient to simply
// use an addtional argument and to declare things an inline
// function. But I was not able to convince SunCC to
// inline and constant fold it.
// Similarly I tried introducing LogP as a template
// parameter but this did not compile on SunCC
  {
    Element *done = to + l;
    Entry    *regEntry   = entry;
    Element **regCurrent = current;
    int      winnerIndex = regEntry[0].index;
    Key      winnerKey   = regEntry[0].key;
    Element *winnerPos;
    Key sup = dummy.key; // supremum
    
    Assert2(logK >= LogK);
    while (to < done) {
      winnerPos = regCurrent[winnerIndex];
      
      // write result
      to->key   = winnerKey;
      to->value = winnerPos->value;
      
      // advance winner segment
      winnerPos++;
      regCurrent[winnerIndex] = winnerPos;
      winnerKey = winnerPos->key;
      
      // remove winner segment if empty now
      if (winnerKey == sup) { 
        deallocateSegment(winnerIndex); 
      } 
      to++;
      
      // update looser tree
#define TreeStep(L)\
      if (1 << LogK >= 1 << L) {\
        Entry *pos##L = regEntry+((winnerIndex+(1<<LogK)) >> ((LogK-L)+1));\
        Key    key##L = pos##L->key;\
        if (key##L < winnerKey) {\
          int index##L  = pos##L->index;\
          pos##L->key   = winnerKey;\
          pos##L->index = winnerIndex;\
          winnerKey     = key##L;\
          winnerIndex   = index##L;\
        }\
      }
      TreeStep(10);
      TreeStep(9);
      TreeStep(8);
      TreeStep(7);
      TreeStep(6);
      TreeStep(5);
      TreeStep(4);
      TreeStep(3);
      TreeStep(2);
      TreeStep(1);
#undef TreeStep      
    }
    regEntry[0].index = winnerIndex;
    regEntry[0].key   = winnerKey;  
  }
#undef LogK

// delete the l smallest elements and write them to "to"
// empty segments are deallocated
// require:
// - there are at least l elements
// - segments are ended by sentinels
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMerge(Element *to, int l)
{
  switch(logK) {
  case 0: 
    Assert2(k == 1);
    Assert2(entry[0].index == 0);
    Assert2(lastFree == -1 || l == 0);
    memcpy(to, current[0], l * sizeof(Element));
    current[0] += l;
    entry[0].key = current[0]->key;
    if (segmentIsEmpty(0)) deallocateSegment(0); 
    break;
  case 1:
    Assert2(k == 2);
    merge(current + 0, current + 1, to, l);
    rebuildLooserTree();
    if (segmentIsEmpty(0)) deallocateSegment(0); 
    if (segmentIsEmpty(1)) deallocateSegment(1); 
    break;
  case 2:
    Assert2(k == 4);
    merge4(current + 0, current + 1, current + 2, current + 3, to, l);
    rebuildLooserTree();
    if (segmentIsEmpty(0)) deallocateSegment(0); 
    if (segmentIsEmpty(1)) deallocateSegment(1); 
    if (segmentIsEmpty(2)) deallocateSegment(2); 
    if (segmentIsEmpty(3)) deallocateSegment(3);
    break;
  case  3: multiMergeUnrolled3(to, l); break;
  case  4: multiMergeUnrolled4(to, l); break;
  case  5: multiMergeUnrolled5(to, l); break;
  case  6: multiMergeUnrolled6(to, l); break;
  case  7: multiMergeUnrolled7(to, l); break;
  case  8: multiMergeUnrolled8(to, l); break;
  case  9: multiMergeUnrolled9(to, l); break;
  case 10: multiMergeUnrolled10(to, l); break;
  default: multiMergeK       (to, l); break;
  }
  size -= l;

  // compact tree if it got considerably smaller
  if (k > 1 && lastFree >= 3*k/5 - 1) { 
    // using k/2 would be worst case inefficient
    compactTree(); 
  }
}


// is this segment empty and does not point to dummy yet?
template <class Key, class Value>
inline int KNLooserTree<Key, Value>::
segmentIsEmpty(int i)
{
  return current[i]->key == getSupremum() && 
         current[i] != &dummy;
}


// multi-merge for arbitrary K
template <class Key, class Value>
void KNLooserTree<Key, Value>::
multiMergeK(Element *to, int l)
{
  Entry *currentPos;
  Key currentKey;
  int currentIndex; // leaf pointed to by current entry
  int kReg = k;
  Element *done = to + l;
  int      winnerIndex = entry[0].index;
  Key      winnerKey   = entry[0].key;
  Element *winnerPos;
  Key sup = dummy.key; // supremum
  while (to < done) {
    winnerPos = current[winnerIndex];

    // write result
    to->key   = winnerKey;
    to->value = winnerPos->value;

    // advance winner segment
    winnerPos++;
    current[winnerIndex] = winnerPos;
    winnerKey = winnerPos->key;

    // remove winner segment if empty now
    if (winnerKey == sup) { 
      deallocateSegment(winnerIndex); 
    } 
    
    // go up the entry-tree
    for (int i = (winnerIndex + kReg) >> 1;  i > 0;  i >>= 1) {
      currentPos = entry + i;
      currentKey = currentPos->key;
      if (currentKey < winnerKey) {
        currentIndex      = currentPos->index;
        currentPos->key   = winnerKey;
        currentPos->index = winnerIndex;
        winnerKey         = currentKey;
        winnerIndex       = currentIndex;
      }
    }

    to++;
  }
  entry[0].index = winnerIndex;
  entry[0].key   = winnerKey;  
}

////////////////////////// KNHeap //////////////////////////////////////
template <class Key, class Value>
KNHeap<Key, Value>::
KNHeap(Key sup, Key infimum) : insertHeap(sup, infimum),
                                   activeLevels(0), size(0)
{
  buffer1[KNBufferSize1].key = sup; // sentinel
  minBuffer1 = buffer1 + KNBufferSize1; // empty
  for (int i = 0;  i < KNLevels;  i++) {
    tree[i].init(sup); // put tree[i] in a consistent state
    buffer2[i][KNN].key = sup; // sentinel
    minBuffer2[i] = &(buffer2[i][KNN]); // empty
  }
}


//--------------------- Buffer refilling -------------------------------

// refill buffer2[j] and return number of elements found
template <class Key, class Value>
int KNHeap<Key, Value>::refillBuffer2(int j)
{
  Element *oldTarget;
  int deleteSize;
  int treeSize = tree[j].getSize();
  int bufferSize = (&(buffer2[j][0]) + KNN) - minBuffer2[j];
  if (treeSize + bufferSize >= KNN) { // buffer will be filled
    oldTarget = &(buffer2[j][0]);
    deleteSize = KNN - bufferSize;
  } else {
    oldTarget = &(buffer2[j][0]) + KNN - treeSize - bufferSize;
    deleteSize = treeSize;
  }

  // shift  rest to beginning
  // possible hack:
  // - use memcpy if no overlap
  memmove(oldTarget, minBuffer2[j], bufferSize * sizeof(Element));
  minBuffer2[j] = oldTarget;

  // fill remaining space from tree
  tree[j].multiMerge(oldTarget + bufferSize, deleteSize);
  return deleteSize + bufferSize;
}
 
 
// move elements from the 2nd level buffers 
// to the delete buffer
template <class Key, class Value>
void KNHeap<Key, Value>::refillBuffer1(void) 
{
  int totalSize = 0;
  int sz;
  for (int i = activeLevels - 1;  i >= 0;  i--) {
    if ((&(buffer2[i][0]) + KNN) - minBuffer2[i] < KNBufferSize1) {
      sz = refillBuffer2(i);
      // max active level dry now?
      if (sz == 0 && i == activeLevels - 1) { activeLevels--; }
      else {totalSize += sz;}
    } else {
      totalSize += KNBufferSize1; // actually only a sufficient lower bound
    }
  }
  if (totalSize >= KNBufferSize1) { // buffer can be filled
    minBuffer1 = buffer1;
    sz = KNBufferSize1; // amount to be copied
    size -= KNBufferSize1; // amount left in buffer2
  } else {
    minBuffer1 = buffer1 + KNBufferSize1 - totalSize;
    sz = totalSize;
    Assert2(size == sz); // trees and buffer2 get empty
    size = 0;
  }

  // now call simplified refill routines
  // which can make the assumption that
  // they find all they are asked to find in the buffers
  minBuffer1 = buffer1 + KNBufferSize1 - sz;
  switch(activeLevels) {
  case 1: memcpy(minBuffer1, minBuffer2[0], sz * sizeof(Element));
          minBuffer2[0] += sz;
          break;
  case 2: merge(&(minBuffer2[0]), 
                &(minBuffer2[1]), minBuffer1, sz);
          break;
  case 3: merge3(&(minBuffer2[0]), 
                 &(minBuffer2[1]),
                 &(minBuffer2[2]), minBuffer1, sz);
          break;
  case 4: merge4(&(minBuffer2[0]), 
                 &(minBuffer2[1]),
                 &(minBuffer2[2]),
                 &(minBuffer2[3]), minBuffer1, sz);
          break;
//  case 2: refillBuffer12(sz); break;
//  case 3: refillBuffer13(sz); break;
//  case 4: refillBuffer14(sz); break;
  }
}


template <class Key, class Value>
void KNHeap<Key, Value>::refillBuffer13(int sz) 
{ Assert(0); // not yet implemented
}

template <class Key, class Value>
void KNHeap<Key, Value>::refillBuffer14(int sz) 
{ Assert(0); // not yet implemented
}


//--------------------------------------------------------------------

// check if space is available on level k and
// empty this level if necessary leading to a recursive call.
// return the level where space was finally available
template <class Key, class Value>
int KNHeap<Key, Value>::makeSpaceAvailable(int level)
{
  int finalLevel;

  Assert2(level <= activeLevels);
  if (level == activeLevels) { activeLevels++; }
  if (tree[level].spaceIsAvailable()) { 
    finalLevel = level;
  } else {
    finalLevel = makeSpaceAvailable(level + 1);
    int segmentSize = tree[level].getSize();
    Element *newSegment = new Element[segmentSize + 1];
    tree[level].multiMerge(newSegment, segmentSize); // empty this level
    //    tree[level].cleanUp();
    newSegment[segmentSize].key = buffer1[KNBufferSize1].key; // sentinel
    // for queues where size << #inserts
    // it might make sense to stay in this level if
    // segmentSize < alpha * KNN * k^level for some alpha < 1
    tree[level + 1].insertSegment(newSegment, segmentSize);
  }
  return finalLevel;
}


// empty the insert heap into the main data structure
template <class Key, class Value>
void KNHeap<Key, Value>::emptyInsertHeap(void)
{
  const Key sup = getSupremum();

  // build new segment
  Element *newSegment = new Element[KNN + 1];
  Element *newPos = newSegment;

  // put the new data there for now
  insertHeap.sortTo(newSegment);
  newSegment[KNN].key = sup; // sentinel

  // copy the buffer1 and buffer2[0] to temporary storage
  // (the tomporary can be eliminated using some dirty tricks)
  const int tempSize = KNN + KNBufferSize1;
  Element temp[tempSize + 1]; 
  int sz1 = getSize1();
  int sz2 = getSize2(0);
  Element *pos = temp + tempSize - sz1 - sz2;
  memcpy(pos      , minBuffer1   , sz1 * sizeof(Element)); 
  memcpy(pos + sz1, minBuffer2[0], sz2 * sizeof(Element));
  temp[tempSize].key = sup; // sentinel

  // refill buffer1
  // (using more complicated code it could be made somewhat fuller
  // in certein circumstances)
  merge(&pos, &newPos, minBuffer1, sz1);

  // refill buffer2[0]
  // (as above we might want to take the opportunity
  // to make buffer2[0] fuller)
  merge(&pos, &newPos, minBuffer2[0], sz2);

  // merge the rest to the new segment
  // note that merge exactly trips into the footsteps
  // of itself
  merge(&pos, &newPos, newSegment, KNN);
  
  // and insert it
  int freeLevel = makeSpaceAvailable(0);
  Assert2(freeLevel == 0 || tree[0].getSize() == 0);
  tree[0].insertSegment(newSegment, KNN);

  // get rid of invalid level 2 buffers
  // by inserting them into tree 0 (which is almost empty in this case)
  if (freeLevel > 0) {
    for (int i = freeLevel;  i >= 0;  i--) { // reverse order not needed 
      // but would allow immediate refill
      newSegment = new Element[getSize2(i) + 1]; // with sentinel
      memcpy(newSegment, minBuffer2[i], (getSize2(i) + 1) * sizeof(Element));
      tree[0].insertSegment(newSegment, getSize2(i));
      minBuffer2[i] = buffer2[i] + KNN; // empty
    }
  }

  // update size
  size += KNN; 

  // special case if the tree was empty before
  if (minBuffer1 == buffer1 + KNBufferSize1) { refillBuffer1(); }
}

/////////////////////////////////////////////////////////////////////
// auxiliary functions

// merge sz element from the two sentinel terminated input
// sequences *f0 and *f1 to "to"
// advance *fo and *f1 accordingly.
// require: at least sz nonsentinel elements available in f0, f1
// require: to may overwrite one of the sources as long as
//   *fx + sz is before the end of fx
template <class Key, class Value>
void merge(KNElement<Key, Value> **f0,
           KNElement<Key, Value> **f1,
           KNElement<Key, Value>  *to, int sz) 
{
  KNElement<Key, Value> *from0   = *f0;
  KNElement<Key, Value> *from1   = *f1;
  KNElement<Key, Value> *done    = to + sz;
  Key      key0    = from0->key;
  Key      key1    = from1->key;

  while (to < done) {
    if (key1 <= key0) {
      to->key   = key1;
      to->value = from1->value; // note that this may be the same address
      from1++; // nach hinten schieben?
      key1 = from1->key;
    } else {
      to->key   = key0;
      to->value = from0->value; // note that this may be the same address
      from0++; // nach hinten schieben?
      key0 = from0->key;
    }
    to++;
  }
  *f0   = from0;
  *f1   = from1;
}


// merge sz element from the three sentinel terminated input
// sequences *f0, *f1 and *f2 to "to"
// advance *f0, *f1 and *f2 accordingly.
// require: at least sz nonsentinel elements available in f0, f1 and f2
// require: to may overwrite one of the sources as long as
//   *fx + sz is before the end of fx
template <class Key, class Value>
void merge3(KNElement<Key, Value> **f0,
           KNElement<Key, Value> **f1,
           KNElement<Key, Value> **f2,
           KNElement<Key, Value>  *to, int sz) 
{
  KNElement<Key, Value> *from0   = *f0;
  KNElement<Key, Value> *from1   = *f1;
  KNElement<Key, Value> *from2   = *f2;
  KNElement<Key, Value> *done    = to + sz;
  Key      key0    = from0->key;
  Key      key1    = from1->key;
  Key      key2    = from2->key;

  if (key0 < key1) {
    if (key1 < key2)   { goto s012; }
    else { 
      if (key2 < key0) { goto s201; }
      else             { goto s021; }
    }
  } else {
    if (key1 < key2) {
      if (key0 < key2) { goto s102; }
      else             { goto s120; }
    } else             { goto s210; }
  }

#define Merge3Case(a,b,c)\
  s ## a ## b ## c :\
  if (to == done) goto finish;\
  to->key = key ## a;\
  to->value = from ## a -> value;\
  to++;\
  from ## a ++;\
  key ## a = from ## a -> key;\
  if (key ## a < key ## b) goto s ## a ## b ## c;\
  if (key ## a < key ## c) goto s ## b ## a ## c;\
  goto s ## b ## c ## a;

  // the order is choosen in such a way that 
  // four of the trailing gotos can be eliminated by the optimizer
  Merge3Case(0, 1, 2);
  Merge3Case(1, 2, 0);
  Merge3Case(2, 0, 1);
  Merge3Case(1, 0, 2);
  Merge3Case(0, 2, 1);
  Merge3Case(2, 1, 0);

 finish:
  *f0   = from0;
  *f1   = from1;
  *f2   = from2;
}


// merge sz element from the three sentinel terminated input
// sequences *f0, *f1, *f2 and *f3 to "to"
// advance *f0, *f1, *f2 and *f3 accordingly.
// require: at least sz nonsentinel elements available in f0, f1, f2 and f2
// require: to may overwrite one of the sources as long as
//   *fx + sz is before the end of fx
template <class Key, class Value>
void merge4(KNElement<Key, Value> **f0,
           KNElement<Key, Value> **f1,
           KNElement<Key, Value> **f2,
           KNElement<Key, Value> **f3,
           KNElement<Key, Value>  *to, int sz) 
{
  KNElement<Key, Value> *from0   = *f0;
  KNElement<Key, Value> *from1   = *f1;
  KNElement<Key, Value> *from2   = *f2;
  KNElement<Key, Value> *from3   = *f3;
  KNElement<Key, Value> *done    = to + sz;
  Key      key0    = from0->key;
  Key      key1    = from1->key;
  Key      key2    = from2->key;
  Key      key3    = from3->key;

#define StartMerge4(a, b, c, d)\
  if (key##a <= key##b && key##b <= key##c && key##c <= key##d)\
    goto s ## a ## b ## c ## d;

  StartMerge4(0, 1, 2, 3);
  StartMerge4(1, 2, 3, 0);
  StartMerge4(2, 3, 0, 1);
  StartMerge4(3, 0, 1, 2);

  StartMerge4(0, 3, 1, 2);
  StartMerge4(3, 1, 2, 0);
  StartMerge4(1, 2, 0, 3);
  StartMerge4(2, 0, 3, 1);

  StartMerge4(0, 2, 3, 1);
  StartMerge4(2, 3, 1, 0);
  StartMerge4(3, 1, 0, 2);
  StartMerge4(1, 0, 2, 3);

  StartMerge4(2, 0, 1, 3);
  StartMerge4(0, 1, 3, 2);
  StartMerge4(1, 3, 2, 0);
  StartMerge4(3, 2, 0, 1);

  StartMerge4(3, 0, 2, 1);
  StartMerge4(0, 2, 1, 3);
  StartMerge4(2, 1, 3, 0);
  StartMerge4(1, 3, 0, 2);

  StartMerge4(1, 0, 3, 2);
  StartMerge4(0, 3, 2, 1);
  StartMerge4(3, 2, 1, 0);
  StartMerge4(2, 1, 0, 3);

#define Merge4Case(a, b, c, d)\
  s ## a ## b ## c ## d:\
  if (to == done) goto finish;\
  to->key = key ## a;\
  to->value = from ## a -> value;\
  to++;\
  from ## a ++;\
  key ## a = from ## a -> key;\
  if (key ## a < key ## c) {\
    if (key ## a < key ## b) { goto s ## a ## b ## c ## d; }\
    else                     { goto s ## b ## a ## c ## d; }\
  } else {\
    if (key ## a < key ## d) { goto s ## b ## c ## a ## d; }\
    else                     { goto s ## b ## c ## d ## a; }\
  }    
  
  Merge4Case(0, 1, 2, 3);
  Merge4Case(1, 2, 3, 0);
  Merge4Case(2, 3, 0, 1);
  Merge4Case(3, 0, 1, 2);

  Merge4Case(0, 3, 1, 2);
  Merge4Case(3, 1, 2, 0);
  Merge4Case(1, 2, 0, 3);
  Merge4Case(2, 0, 3, 1);

  Merge4Case(0, 2, 3, 1);
  Merge4Case(2, 3, 1, 0);
  Merge4Case(3, 1, 0, 2);
  Merge4Case(1, 0, 2, 3);

  Merge4Case(2, 0, 1, 3);
  Merge4Case(0, 1, 3, 2);
  Merge4Case(1, 3, 2, 0);
  Merge4Case(3, 2, 0, 1);

  Merge4Case(3, 0, 2, 1);
  Merge4Case(0, 2, 1, 3);
  Merge4Case(2, 1, 3, 0);
  Merge4Case(1, 3, 0, 2);

  Merge4Case(1, 0, 3, 2);
  Merge4Case(0, 3, 2, 1);
  Merge4Case(3, 2, 1, 0);
  Merge4Case(2, 1, 0, 3);

 finish:
  *f0   = from0;
  *f1   = from1;
  *f2   = from2;
  *f3   = from3;
}

//  C++ done

// C bindings for C++

#include <limits.h>

void* KNHeap__create(void) {
    return new KNHeap<wl_syscalls__fd_allocator__size_t, void*>(wl_syscalls__fd_allocator__size_t_MAX, -wl_syscalls__fd_allocator__size_t_MAX);
}

void KNHeap__destroy(void* instance) {
    delete reinterpret_cast<KNHeap<wl_syscalls__fd_allocator__size_t, void*>*>(instance);
}

int   KNHeap__getSize(void* instance) {
    return reinterpret_cast<KNHeap<wl_syscalls__fd_allocator__size_t, void*>*>(instance)->getSize();
}
void  KNHeap__getMin(void* instance, wl_syscalls__fd_allocator__size_t *key, void* *value) {
    reinterpret_cast<KNHeap<wl_syscalls__fd_allocator__size_t, void*>*>(instance)->getMin(key, value);
}
void  KNHeap__deleteMin(void* instance, wl_syscalls__fd_allocator__size_t *key, void* *value) {
    reinterpret_cast<KNHeap<wl_syscalls__fd_allocator__size_t, void*>*>(instance)->deleteMin(key, value);
}
void  KNHeap__insert(void* instance, wl_syscalls__fd_allocator__size_t key, void* value) {
    reinterpret_cast<KNHeap<wl_syscalls__fd_allocator__size_t, void*>*>(instance)->insert(key, value);
}

void*  ShrinkingVectorIndexAllocator__create(void) {
    return new ShrinkingVectorIndexAllocator();
}
void   ShrinkingVectorIndexAllocator__destroy(void* instance) {
    delete reinterpret_cast<ShrinkingVectorIndexAllocator*>(instance);
}
size_t ShrinkingVectorIndexAllocator__size(void* instance) {
    return reinterpret_cast<ShrinkingVectorIndexAllocator*>(instance)->size();
}
size_t ShrinkingVectorIndexAllocator__capacity(void* instance) {
    return reinterpret_cast<ShrinkingVectorIndexAllocator*>(instance)->capacity();
}
size_t ShrinkingVectorIndexAllocator__add(void* instance, void* value, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback) {
    return reinterpret_cast<ShrinkingVectorIndexAllocator*>(instance)->add(value, callback);
}
size_t ShrinkingVectorIndexAllocator__reuse(void* instance, size_t index, void* value, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback) {
    return reinterpret_cast<ShrinkingVectorIndexAllocator*>(instance)->reuse(index, value, callback);
}
bool ShrinkingVectorIndexAllocator__index_is_valid(void* instance, size_t index) {
    int a;
    size_t b;
    return reinterpret_cast<ShrinkingVectorIndexAllocator*>(instance)->index_if_valid(index, &a, &b) != NULL;
}
void* ShrinkingVectorIndexAllocator__data(void* instance, size_t index) {
    return reinterpret_cast<ShrinkingVectorIndexAllocator*>(instance)->operator[](index);
}
bool   ShrinkingVectorIndexAllocator__remove(void* instance, int index) {
    return reinterpret_cast<ShrinkingVectorIndexAllocator*>(instance)->remove(index);
}

// C bindings done, C++ no longer needed

wl_syscalls__fd_allocator * wl_syscalls__fd_allocator__create(void) {
    wl_syscalls__fd_allocator * fd = (wl_syscalls__fd_allocator *) malloc(sizeof(wl_syscalls__fd_allocator));
    if (fd == NULL) {
        return NULL;
    }
    fd->used = ShrinkingVectorIndexAllocator__create();
    fd->recycled = KNHeap__create();
    return fd;
}

void wl_syscalls__fd_allocator__destroy(wl_syscalls__fd_allocator * fd) {
    ShrinkingVectorIndexAllocator__destroy(fd->used);
    KNHeap__destroy(fd->recycled);
    free(fd);
}

size_t wl_syscalls__fd_allocator__size(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator) {
    return ShrinkingVectorIndexAllocator__size(wl_syscalls__fd_allocator->used);
}

size_t wl_syscalls__fd_allocator__capacity(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator) {
    return ShrinkingVectorIndexAllocator__capacity(wl_syscalls__fd_allocator->used);
}

int wl_syscalls__fd_allocator__allocate_fd(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator, void * data, WL_SYSCALLS_FD_ALLOCATOR_CALLBACK___DESTROY_DATA callback) {
    if (KNHeap__getSize(wl_syscalls__fd_allocator->recycled) > 0) {
        void* null_data;
        int fd;
        KNHeap__deleteMin(wl_syscalls__fd_allocator->recycled, &fd, &null_data);
        return ShrinkingVectorIndexAllocator__reuse(wl_syscalls__fd_allocator->used, fd, data, callback);
    } else {
        return ShrinkingVectorIndexAllocator__add(wl_syscalls__fd_allocator->used, data, callback);
    }
}

bool wl_syscalls__fd_allocator__fd_is_valid(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator, int fd) {
    return ShrinkingVectorIndexAllocator__index_is_valid(wl_syscalls__fd_allocator->used, fd);
}

void * wl_syscalls__fd_allocator__get_value_from_fd(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator, int fd) {
    return ShrinkingVectorIndexAllocator__data(wl_syscalls__fd_allocator->used, fd);
}

void wl_syscalls__fd_allocator__deallocate_fd(wl_syscalls__fd_allocator * wl_syscalls__fd_allocator, int fd) {
    size_t diff = ShrinkingVectorIndexAllocator__capacity(wl_syscalls__fd_allocator->used);
    if (!ShrinkingVectorIndexAllocator__remove(wl_syscalls__fd_allocator->used, fd)) {
        if (wl_miniobj_debug) printf("ATTEMPTING TO DEALLOCATE INVALID INDEX: %%d\n", fd);
        return;
    }
    size_t cap = ShrinkingVectorIndexAllocator__capacity(wl_syscalls__fd_allocator->used);
    if ((diff - cap) != 0) {
        if (cap == 0) {
            KNHeap__destroy(wl_syscalls__fd_allocator->recycled);
            wl_syscalls__fd_allocator->recycled = KNHeap__create();
        } else if (KNHeap__getSize(wl_syscalls__fd_allocator->recycled) > 0) {
            // we need to remove size
            int size = ShrinkingVectorIndexAllocator__size(wl_syscalls__fd_allocator->used);
            void * tmp = KNHeap__create();
            int removed = -1;
            void * null_data;
            // extract valid items
            while(true) {
                if (KNHeap__getSize(wl_syscalls__fd_allocator->recycled) == 0) {
                    break;
                }
                KNHeap__deleteMin(wl_syscalls__fd_allocator->recycled, &removed, &null_data);
                if (removed >= size) {
                    break;
                }
                KNHeap__insert(tmp, removed, NULL);
            }
            // rebuild queue
            if (wl_miniobj_debug) printf("REBUILD FD RECYCLE QUEUE\n");
            KNHeap__destroy(wl_syscalls__fd_allocator->recycled);
            wl_syscalls__fd_allocator->recycled = KNHeap__create();
            removed = -1;
            while(KNHeap__getSize(tmp) != 0) {
                KNHeap__deleteMin(tmp, &removed, &null_data);
                KNHeap__insert(wl_syscalls__fd_allocator->recycled, removed, NULL);
            }
            KNHeap__destroy(tmp);
        }
    } else {
        KNHeap__insert(wl_syscalls__fd_allocator->recycled, fd, NULL);
    }
}
)");

    fflush(header);
    fclose(header);
    fflush(code);
    fclose(code);
    

    e = fopen_s(&header, "../../wl_syscalls.h", "wt");
    if (header == NULL) {
        memset(buf, 0, sizeof(char[4096]));
        strerror_s(buf, e);
        printf("failed to open '../../wl_syscalls.h', errno = %s\n", buf);
        return 0;
    }
    e = fopen_s(&code, "../../wl_syscalls.c", "wt");
    if (code == NULL) {
        memset(buf, 0, sizeof(char[4096]));
        strerror_s(buf, e);
        printf("failed to open '../../wl_syscalls.c', errno = %s\n", buf);
        fclose(header);
        return 0;
    }

    printf("\n");

    fprintf(header, R"(#ifndef WL_SYSCALLS_H
#define WL_SYSCALLS_H

#include <stdio.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h> // CHAR_BIT

// sanity check
#if CHAR_BIT != 8
#error system does not support 8 bit addressing
#endif

#ifdef __cplusplus
#define restrict
#endif

#include "wl_syscalls__fd_allocator.h"

#ifdef _WIN32
// winsock2 includes time.h
#include <winsock2.h>
#include <Windows.h>
#include <synchapi.h>
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;

#include <aclapi.h>
#include <afunix.h>

#include <tcpmib.h>
#include <iphlpapi.h>

typedef PSID uid_t;
typedef PSID gid_t;
typedef DWORD pid_t;
typedef int socklen_t;

struct iovec {
	void  *iov_base;    /* Starting address */
	size_t iov_len;     /* Number of bytes to transfer */
};

#define AF_LOCAL AF_UNIX
#define PF_LOCAL PF_UNIX

#define	SOCK_CLOEXEC		0x8000	/* set FD_CLOEXEC */
#define	SOCK_NONBLOCK		0x4000	/* set O_NONBLOCK */
#define MSG_NOSIGNAL        0x1000  /* dont generate SIGPIPE */
#define MSG_DONTWAIT        0x2000  /* dont block */

typedef union epoll_data {
  void* ptr;
  int fd;
  uint32_t u32;
  uint64_t u64;
  SOCKET sock; /* Windows specific */
  HANDLE hnd;  /* Windows specific */
} epoll_data_t;

struct epoll_event {
  uint32_t events;   /* Epoll events and flags */
  epoll_data_t data; /* User data variable */
};

#define CLANG__ALWAYS_INLINE __attribute((always_inline))

/*
 * Add the pseudo keyword 'fallthrough' so case statement blocks
 * must end with any of these keywords:
 *   break;
 *   CLANG__FALLTHROUGH;
 *   continue;
 *   goto <label>;
 *   return [expression];
 *
 *  gcc: https://gcc.gnu.org/onlinedocs/gcc/Statement-Attributes.html#Statement-Attributes
 */
#if __has_attribute(__fallthrough__)
# define CLANG__FALLTHROUGH                    __attribute__((__fallthrough__))
#else
# define CLANG__FALLTHROUGH                    do {} while (0)  /* fallthrough */
#endif

#ifdef _WIN64

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
typedef long long LP64__LONG;

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
typedef unsigned long long LP64__ULONG;

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__LONG_MIN LLONG_MIN

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__LONG_MAX LLONG_MAX

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__ULONG_MIN ULLONG_MIN

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__ULONG_MAX ULLONG_MAX

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__1L 1LL

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__1UL 1ULL

#else
#ifdef _WIN32

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
typedef long LP64__LONG;

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
typedef unsigned long LP64__ULONG;

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__LONG_MIN LONG_MIN

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__LONG_MAX LONG_MAX

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__ULONG_MIN ULONG_MIN

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__ULONG_MAX ULONG_MAX

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__1L 1L

// alpine (and other linux systems) uses LP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 64 bits)
// windows uses LPP64
//    (32-bit systems long == 32 bits, 64-bit systems long == 32 bits)
#define LP64__1UL 1UL

#endif
#endif

// time64_t is undefined on windows (and __time64_t is undefined in 32-bit)
// alpine specifies time_t as int64
typedef int64_t time_64_t;

// suseconds_t is undefined on windows
// alpine specifies suseconds_t as int64
typedef int64_t suseconds_t;

// windows defined timespec, we need to rename
//
// alpine specifies time_t as int64
// alpine specifies suseconds_t as int64
//
struct timeval_64_t { time_64_t tv_sec; suseconds_t tv_usec; };

// windows defined timespec, we need to rename
//
// alpine pads the timespec to 64-bits
//   "the kernel interfaces use a timespec layout padded to match the representation of a pair of 64-bit values, which requires endian-specific padding."
//
// alpine specifies time_t as int64
//
// UNIX long is LP64, 32-bit systems long == 32 bits, 64-bit systems long == 64 bits
//

// clang defines __BYTE_ORDER__

struct timespec_N_bit {
	time_64_t tv_sec;
	int :8*(sizeof(time_t)-sizeof(LP64__LONG))*(__BYTE_ORDER__==4321);
	LP64__LONG tv_nsec;
	int :8*(sizeof(time_t)-sizeof(LP64__LONG))*(__BYTE_ORDER__!=4321);
};

struct itimerspec {
	struct timespec_N_bit it_interval;  /* Interval for periodic timer */
	struct timespec_N_bit it_value;     /* Initial expiration */
};

#else

// windows includes time.h so include it for linux as well
#include <time.h>
#include <sys/socket.h>
#include <sys/eventfd.h>
#ifdef HAVE_SYS_UCRED_H
#include <sys/ucred.h>
#endif

#endif

typedef struct wl_peercred_t {
    pid_t pid;
    uid_t uid;
    gid_t gid;
} wl_peercred_t;

#define WL_MINIOBJ_MACRO____PP_128TH_ARG( _1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,_31,_32,_33,_34,_35,_36,_37,_38,_39,_40,_41,_42,_43,_44,_45,_46,_47,_48,_49,_50,_51,_52,_53,_54,_55,_56,_57,_58,_59,_60,_61,_62,_63,_64,_65,_66,_67,_68,_69,_70,_71,_72,_73,_74,_75,_76,_77,_78,_79,_80,_81,_82,_83,_84,_85,_86,_87,_88,_89,_90,_91,_92,_93,_94,_95,_96,_97,_98,_99,_100,_101,_102,_103,_104,_105,_106,_107,_108,_109,_110,_111,_112,_113,_114,_115,_116,_117,_118,_119,_120,_121,_122,_123,_124,_125,N,...) N

#define WL_MINIOBJ_MACRO____PP_RSEQ_N() 125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0

#define WL_MINIOBJ_MACRO____PP_NARG(N, ...) WL_MINIOBJ_MACRO____PP_NARG_(__VA_ARGS__, WL_MINIOBJ_MACRO____PP_RSEQ_N())

#define WL_MINIOBJ_MACRO____PP_NARG_(...) WL_MINIOBJ_MACRO____PP_128TH_ARG(__VA_ARGS__)

#ifdef __cplusplus
extern "C" {
#endif

// deinitializes the global miniobj system
//
// the global miniobj system will be re-initialized automatically when needed
void wl_miniobj_deinit(void);

/*
 * A mini object, used to implement a
 * kernel-like api for opaque objects
 * 
 * this MUST NOT be passed to free() since it maintains a reference count
 */
typedef struct wl_miniobj {
    uint16_t version;
    uint16_t type;
    uint64_t data;
} wl_miniobj;

typedef int (*WL_MINIOBJ_CALLBACK)(int fd, int argc, va_list args);

// creates a new miniobj object wrapped in an integer identifier
//
//   PERF RECOMMENDATION:
//     it is recommended to delay the creation of the miniobj itself until all data has been set-up
//         this results in fast failure and minimizes possible data races
//
//       instead of doing
//
//         WL_MINIOBJ_CREATE_WITH_OBJ(obj, fd, ver, type, true);
//         obj->data = allocate_ref_counted_data();
//         if (obj->data != nullptr) {
//             ((REF*)obj->data)->ref = create();
//             if (((REF*)obj->data)->ref == INVALID_HANDLE) {
//                 deallocate_ref_counted_data(obj->data);
//                 wl_miniobj_destroy(obj_fd, obj);
//
//                 // return error
//                 errno = EINVAL;
//                 return -1;
//             }
//         }
//         if (obj->data == nullptr) {
//             wl_miniobj_destroy(obj_fd, obj);
//
//             // return error
//             errno = EINVAL;
//             return -1;
//         }
//         return fd;
//
//       we could instead do
//
//         HANDLE h = create();
//         if (h == INVALID_HANDLE) {
//             errno = EINVAL;
//             return -1;
//         }
//
//         REF * data = allocate_ref_counted_data();
//         if (data == nullptr) {
//             destroy(h);
//             errno = EINVAL;
//             return -1;
//         }
//
//         data->ref = h;
//
//         WL_MINIOBJ_CREATE_WITH_OBJ(obj, fd, ver, type, true);
//         obj->data = data;
//         return fd;
//
//
// ERROR HANDLING: in events of error handling, call wl_miniobj_destroy first
//   it is common to do the following:
//
//       WL_MINIOBJ_CREATE_WITH_OBJ(
//           obj, obj_fd,                             // obj
//           obj_version, obj_type,                   // info
//           obj_underlying_data_is_reference_counted // ref count
//       );
//
//       int some_error = setup_obj(obj_fd, obj);
//
//       if (some_error != 0) {
//
//           // only if there is data to clean up
//           // wl_syscall__close(obj_fd);
//
//           // otherwise call this as it is cheaper
//           wl_miniobj_destroy(obj_fd, obj);
//
//           // return error
//           errno = EINVAL;
//           return -1;
//       }
//
// 
// the miniobj can be obtained by passing the returned fd into wl_miniobj_get
//
// WL_MINIOBJ_CREATE_WITH_OBJ can be used instead to automatically obtain the miniobj
//
// WL_MINIOBJ_CREATE(fd, ...);
// wl_miniobj * obj = wl_miniobj_get(fd);
// // or
// WL_MINIOBJ_CREATE_WITH_OBJ(obj, fd, ...);
//
// param: underlying_data_is_reference_counted
//    true:  this is not internally reference counted by the miniobj registry
//    false: this is internally reference counted by the miniobj registry
//
// the following objects are known to be reference counted:
//    windows HANDLE
//    unix file descriptors (int fd)
//
// this object should be destroyed by passing it to wl_miniobj_destroy
//
// returns name with a value of -1 if the type has not registered
// returns name with a value of -1 if malloc failed
//
#define WL_MINIOBJ_CREATE(name, obj_version, obj_type, underlying_data_is_reference_counted) int name = wl_miniobj_create(obj_version, obj_type, underlying_data_is_reference_counted)

// same as WL_MINIOBJ_CREATE but also extracts the miniobj from the returned fd
//
// WL_MINIOBJ_CREATE_WITH_OBJ(obj, fd, ...);
//
//
// this object should be destroyed by passing it to wl_miniobj_destroy
//
// returns fd_name with a value of -1 if the type has not registered
// returns fd_name with a value of -1 if malloc failed
//
#define WL_MINIOBJ_CREATE_WITH_OBJ(fd_name, obj_name, obj_version, obj_type, underlying_data_is_reference_counted) WL_MINIOBJ_CREATE(fd_name, obj_version, obj_type, underlying_data_is_reference_counted); wl_miniobj * obj_name = wl_miniobj_get(fd_name)

// obtains the miniobj referenced by the given fd
//
// returns NULL if the fd is invalid
// returns NULL if the fd is currently being deallocated (to avoid recursively deallocating the fd)
//
wl_miniobj *  wl_miniobj_get(int fd);

// returns true if the miniobj is currently being destroyed
bool wl_miniobj_is_being_destroyed(int fd);

// duplicates a miniobj object and returns it wrapped in a new fd
//
// if the underlying data IS reference counted
// we must construct a new object
// each duplicate holds its own data
// this object itself is not reference counted
//
// if the underlying data IS NOT reference counted
// we must return the same object
// this object itself is reference counted
//
#define WL_MINIOBJ_DUPLICATE(out_fd, in_fd) int out_fd = wl_miniobj_duplicate(in_fd)

// same as WL_MINIOBJ_DUPLICATE but also extracts the miniobj from the returned fd
#define WL_MINIOBJ_DUPLICATE_WITH_OBJ(out_fd, out_obj, in_fd) int out_fd = wl_miniobj_duplicate(in_fd); wl_miniobj * out_obj = wl_miniobj_get(out_fd)

// returns false if `obj == NULL || obj->version < _version || obj->type < _type`
//
#define WL_MINIOBJ_IS_NOT_VALID(obj, _version, _type) (obj == NULL || obj->version < _version || obj->type < _type)

#define WL_MINIOBJ_HAS_FLAG(flags, flag) (((flags) & (flag)) != 0)
#define WL_MINIOBJ_ADD_FLAG(flags, flag) flags |= (flag)
#define WL_MINIOBJ_REMOVE_FLAG(flags, flag) flags &= ~(flag)
#define WL_MINIOBJ_SET_FLAG(flags, flag, boolean_value) { if (boolean_value) { WL_MINIOBJ_ADD_FLAG(flags, flag); } else { WL_MINIOBJ_REMOVE_FLAG(flags, flag); } }
#define WL_MINIOBJ_HAS_INVALID_FLAGS(flags, all_supported_flags) (((flags) & ~ (all_supported_flags)) != 0)

#define WL_ARGV(type, name, argv) type name = va_arg(argv, type)

// set this to true to log miniobj malloc/free calls
extern bool wl_miniobj_log_allocation;
uint64_t wl_miniobj__total_allocations(void);
uint64_t wl_miniobj__total_deallocations(void);

// creates a new miniobj object wrapped in an integer identifier
// 
// the miniobj can be obtained by passing the returned fd into wl_miniobj_get
//
// int fd = wl_miniobj_create(...);
// wl_miniobj * obj = wl_miniobj_get(fd);
//
// param: underlying_data_is_reference_counted
//    true:  this is internally reference counted by the miniobj registry
//    false: this is not internally reference counted by the miniobj registry
//
// the following objects are known to be reference counted:
//    windows HANDLE
//    unix file descriptors (int fd)
//
// this object should be destroyed by passing it to wl_miniobj_destroy
//
// returns NULL if the type has not registered
//
int wl_miniobj_create(uint16_t version, uint16_t type, bool underlying_data_is_reference_counted);

// returns true if a call to wl_miniobj_destroy would destroy this object
//
// it is recommended to call wl_miniobj_destroy regardless of the return value
//
// usage:
//        if (wl_miniobj_should_destroy(fd)) {
//            cleanup(fd);
//        }
//        wl_miniobj_destroy(fd, NULL);
//
bool wl_miniobj_should_destroy(int fd);

// duplicates a miniobj object and returns it wrapped in a new fd
//
// if the underlying data IS reference counted
// we must construct a new object
// each duplicate holds its own data
// this object itself is not reference counted
//
// if the underlying data IS NOT reference counted
// we must return the same object
// this object itself is reference counted
//
int wl_miniobj_duplicate(int fd);

// destroys an allocated miniobj object
//
// it is UB to manually free() this object as the reference count WILL NOT decrease nor will it's wrapped fd be deallocated
// 
// it is valid to pass the fd to wl_syscalls__fd_allocator__deallocate_fd however this requires a valid fd allocator instance
//   and MUST NOT be done from a close syscall callback (as it will recurse infinitely)
//
// if the object is internally reference counted by the miniobj registry then this function also decrements it after freeing
//
// this WILL call the close() SYSCALL ( WL_MINIOBJ_SYSCALL__NR__CLOSE ) on the object due to the fd being deallocated
// HOWEVER wl_miniobj_get will detect this and return NULL
//
// it is SAFE to pass a NULL object to this function
//
void wl_miniobj_destroy(int fd, wl_miniobj * obj);


void wl_miniobj_internal_reg(uint16_t obj_type, int argc, ...);

// registers a new wl_miniobj type
//
// this takes [ obj_type, syscall, callback, syscall, callback, ... ]
//
// a syscall should ALWAYS have a matching callback
//
// care should be taken when passing an obj_type since this
// will be a direct index into the registery
//
// this will return NULL if the type already exists
// and print an error to the console specifying the type id that was given
//
//
// it is recommended for a registered type to have a registered close callback
// this is to clean up anything done when creating the object
//
// this may include:
// allocations, mutexes, open files/handles/descriptors, ect
// and others that must be cleaned up before deleting the object
//
// // declare a callback
// WL_MINIOBJ__DECL_CALLBACK(my_close, fd, argc, argv) {
//     if (wl_miniobj_should_destroy(fd)) {
//         // handle object destruction here
//     }
//     // optionally freeing with wl_miniobj_destroy(fd, NULL);
// }
//
// // pass this callback to wl_miniobj_reg
// wl_miniobj_reg(obj_type, WL_MINIOBJ_SYSCALL__NR__CLOSE, my_close);
//
#define wl_miniobj_reg(obj_type, ...) wl_miniobj_internal_reg(obj_type, WL_MINIOBJ_MACRO____PP_NARG(0, 0, ## __VA_ARGS__) - 1, ## __VA_ARGS__)

// unregisters a registered wl_miniobj type
// 
// does nothing if already unregistered
//
// a type CANNOT be unregistered if there are allocated instances of it
//
void wl_miniobj_unreg(uint16_t obj_type);

// extracts obj pointed to by fd and invokes a syscall on it
//
// does nothing if obj is NULL
// does nothing if obj is unregistered
// does nothing if obj is not interested in desired syscall
//
int WL_MINIOBJ__DO_SYSCALL(int fd, uint16_t syscall, int argc, ...);

// a macro to be used for declaring syscall callbacks
//
// it is recommended for a registered type to have a registered close callback
// this is to clean up anything done when creating the object
//
// this may include:
// allocations, mutexes, open files/handles/descriptors, ect
// and others that must be cleaned up before deleting the object
//
// // declare a callback
// WL_MINIOBJ__DECL_CALLBACK(my_close, fd, argc, argv) {
//     if (wl_miniobj_should_destroy(fd)) {
//         // handle object destruction here
//     }
//     // optionally freeing with wl_miniobj_destroy(fd, NULL);
// }
//
// // pass this callback to wl_miniobj_reg
// wl_miniobj_reg(obj_type, WL_MINIOBJ_SYSCALL__NR__CLOSE, my_close);
//
#define WL_MINIOBJ__DECL_CALLBACK(name, fd, argc, argv) static int name (int fd, const int argc, va_list argv)

)");

fprintf(code, R"(#include "wl_syscalls.h"

int8_t FLAG_IS_BEING_DESTROYED = 0x1;
int8_t FLAG_FD_IS_BEING_DESTROYED = 0x2;
int8_t FLAG_UNDERLYING_DATA_IS_REFERENCE_COUNTED = 0x4;
int8_t FLAG_FD_IS_BEING_DESTROYED_MANUALLY = 0x8;

/*
 * A mini object, used to implement a
 * kernel-like api for opaque objects
 *
 * private implementation
 */
typedef struct wl_miniobj_priv {
    wl_miniobj public_obj;
    uint16_t references;
    int8_t  flags;
} wl_miniobj_priv;

typedef struct wl_miniobj_registry_t {
    uint8_t registered; // true if taken, false if free
    uint16_t references; // reference count all allocated objects, it is illegal to unregister if reference is not 0
    uint16_t count;
    uint16_t * accepted_syscalls;
    WL_MINIOBJ_CALLBACK * callbacks;
} wl_miniobj_registry_t;

// this computes to 1 MB on a 64 bit system
// this is enough to hold the maximum number of objects that we can contain

// for performance, we avoid allocating an wl_miniobj_registry_t object and instead directly use our allocated memory

// to maximize performance, the registered type shall directly correspond to an array index in the registry

wl_miniobj_registry_t * wl_miniobj__registry = NULL;
uint16_t wl_miniobj__registry_count = 0; // we still need this to know how many objects there are

wl_syscalls__fd_allocator * wl_miniobj_fd_list = NULL;

bool wl_miniobj_log_allocation = false;

uint64_t wl_miniobj_total_allocations = 0;
uint64_t wl_miniobj_total_deallocations = 0;

uint64_t wl_miniobj__total_allocations(void) { return wl_miniobj_total_allocations; }
uint64_t wl_miniobj__total_deallocations(void) { return wl_miniobj_total_deallocations; }

static void wl_miniobj_init(void) {
    if (wl_miniobj__registry == NULL) {
        wl_miniobj__registry = (wl_miniobj_registry_t*) calloc(65535, sizeof(wl_miniobj_registry_t)); // zero out every field
        if (wl_miniobj__registry == NULL) {
            printf("WL MINIOBJ ERROR:       FAILED TO INITIALIZE REGISTRY\n");
        }
        wl_miniobj__registry_count = 0;
        wl_miniobj_total_allocations = 0;
        wl_miniobj_total_deallocations = 0;
        wl_miniobj_fd_list = wl_syscalls__fd_allocator__create();
        if (wl_miniobj_fd_list == NULL) {
            printf("WL MINIOBJ ERROR:       FAILED TO INITIALIZE FD ALLOCATOR\n");
            free(wl_miniobj__registry);
            wl_miniobj__registry = NULL;
        }
    }
}

void wl_miniobj_deinit(void) {
    if (wl_miniobj__registry != NULL) {
        size_t objcount = wl_syscalls__fd_allocator__size(wl_miniobj_fd_list);
        if (objcount != 0) {
            printf("WL MINIOBJ WARNING: there are %%zu allocated objects still present, they will be destroyed\n", objcount);
        }
        wl_syscalls__fd_allocator__destroy(wl_miniobj_fd_list);
        for (uint16_t i = 0; i < 65535; i++) {
            if (wl_miniobj__registry[i].registered) {
                wl_miniobj_unreg(i);
            }
        }
        wl_miniobj__registry_count = 0;
        free(wl_miniobj__registry);
        wl_miniobj__registry = NULL;
    }
}

static wl_miniobj_priv *  wl_miniobj_get_priv(int fd) {
    if (fd == -1) {
        printf("WL MINIOBJ ERROR: fd is -1\n");
        return NULL;
    }
    if (!wl_syscalls__fd_allocator__fd_is_valid(wl_miniobj_fd_list, fd)) {
        printf("WL MINIOBJ ERROR: fd (%%d) is invalid \n", fd);
        return NULL;
    }
    return (wl_miniobj_priv *) wl_syscalls__fd_allocator__get_value_from_fd(wl_miniobj_fd_list, fd);
}

wl_miniobj *  wl_miniobj_get(int fd) {
    wl_miniobj_priv * obj_ = wl_miniobj_get_priv(fd);
    if (obj_ != NULL) {
        if (WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_IS_BEING_DESTROYED)) {
            if (wl_miniobj_debug) printf("obj is being destroyed\n");
            return NULL;
        }
    }
    return (wl_miniobj*) obj_;
}

bool wl_miniobj_is_being_destroyed(int fd) {
    wl_miniobj_priv * obj_ = wl_miniobj_get_priv(fd);
    if (obj_ != NULL) {
        if (WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_IS_BEING_DESTROYED)) {
            return true;
        }
    }
    return false;
}

static void WL_MINIOBJ_DESTROY_CALLBACK(size_t fd, void** unused, bool in_destructor) {
    if (in_destructor) {
        wl_miniobj_priv * obj_ = wl_miniobj_get_priv(fd);
        if (obj_ != NULL) {
            WL_MINIOBJ_ADD_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED);
        }
    }
    wl_syscall__close(fd);
    *unused = NULL;
}

int wl_miniobj_create(uint16_t version, uint16_t type, bool underlying_data_is_reference_counted) {
    wl_miniobj_init();
    if (!wl_miniobj__registry[type].registered) {
        printf("WL MINIOBJ ERROR: attempted to create an unregistered object of type: %%u\n", type);
        return -1;
    }
    if (wl_miniobj__registry[type].references == 65535) {
        printf("WL MINIOBJ ERROR: maximum number of objects of type %%u have been created, please destroy an object of same type in order to make room for another object of same type to be created\n", type);
        return -1;
    }
    wl_miniobj * obj = (wl_miniobj*) malloc(sizeof(wl_miniobj_priv));
    if (obj == NULL) {
        return -1;
    }
    wl_miniobj_total_allocations++;
    obj->version = version;
    obj->type = type;
    obj->data = 0;
    wl_miniobj_priv * obj_ = (wl_miniobj_priv *) obj;
    obj_->flags = 0;
    WL_MINIOBJ_SET_FLAG(obj_->flags, FLAG_UNDERLYING_DATA_IS_REFERENCE_COUNTED, underlying_data_is_reference_counted);
    obj_->references = 1;
    wl_miniobj__registry[type].references++;

    if (wl_miniobj_log_allocation) printf("WL MINIOBJ WARNING: allocate (malloc) object: %%p, with type: %%u\n", obj, obj->type);
    
    return (int) wl_syscalls__fd_allocator__allocate_fd(wl_miniobj_fd_list, obj, WL_MINIOBJ_DESTROY_CALLBACK);
}

int wl_miniobj_duplicate(int fd) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    wl_miniobj_priv * obj_ = (wl_miniobj_priv *) obj;
    if (obj == NULL) {
        return -1;
    }
    if (!WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_UNDERLYING_DATA_IS_REFERENCE_COUNTED)) {
        // if the underlying data IS NOT reference counted
        // we must return the same object
        // this object itself is reference counted
        obj_->references++;
        return (int) wl_syscalls__fd_allocator__allocate_fd(wl_miniobj_fd_list, obj, WL_MINIOBJ_DESTROY_CALLBACK);
    } else {
        // if the underlying data IS reference counted
        // we must construct a new object
        // each duplicate holds its own data, however it is copied for convinience (new obj data = obj data)
        // this object itself is not reference counted
        int new_fd = wl_miniobj_create(obj->version, obj->type, WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_UNDERLYING_DATA_IS_REFERENCE_COUNTED));
        wl_miniobj * new_obj = wl_miniobj_get(new_fd);
        new_obj->data = obj->data;
        return new_fd;
    }
}

bool wl_miniobj_should_destroy(int fd) {
    wl_miniobj * obj = wl_miniobj_get(fd);
    if (obj == NULL) {
        return false;
    }
    if (!wl_miniobj__registry[obj->type].registered) {
        return false;
    }
    if (wl_miniobj__registry[obj->type].references == 0) {
        return false;
    }
    wl_miniobj_priv * obj_ = (wl_miniobj_priv *) obj;
    // each reference has its own fd
    uint16_t ref = obj_->references;
    if (WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED)) {
        ref--;
    }
    if (!WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED) && !WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED_MANUALLY)) {
        ref--;
    }
    if (ref == 0) {
        return true;
    }
    return false;
}

void wl_miniobj_destroy(int fd, wl_miniobj * obj) {
    if (fd == -1 && obj == NULL) {
        printf("WL MINIOBJ ERROR: cannot destroy NULL object\n");
        return;
    }
    if (fd != -1 && obj == NULL) {
        obj = wl_miniobj_get(fd);
        if (obj != NULL) {
            wl_miniobj_destroy(fd, obj);
        }
        return;
    }
    if (fd == -1 && obj != NULL) {
        printf("WL MINIOBJ ERROR: an fd is required to destroy object: %%u\n", obj->type);
        return;
    }
    if (!wl_miniobj__registry[obj->type].registered) {
        printf("WL MINIOBJ ERROR: cannot destroy an unregistered object of type: %%u\n", obj->type);
        return;
    }
    if (wl_miniobj__registry[obj->type].references == 0) {
        printf("WL MINIOBJ ERROR: type cannot be unregistered while there are references to it: %%u\n", obj->type);
        return;
    }
    wl_miniobj_priv * obj_ = (wl_miniobj_priv *) obj;
    // each reference has its own fd
    if (WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED)) {
        obj_->references--;
    }
    if (!WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED) && !WL_MINIOBJ_HAS_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED_MANUALLY)) {
        obj_->references--;
        if (obj_->references == 0) {
            WL_MINIOBJ_ADD_FLAG(obj_->flags, FLAG_IS_BEING_DESTROYED);
        }
        WL_MINIOBJ_ADD_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED_MANUALLY);
        wl_syscalls__fd_allocator__deallocate_fd(wl_miniobj_fd_list, fd);
        WL_MINIOBJ_REMOVE_FLAG(obj_->flags, FLAG_FD_IS_BEING_DESTROYED_MANUALLY);
    }
    if (obj_->references == 0) {
        wl_miniobj__registry[obj->type].references--;
        if (wl_miniobj_log_allocation) printf("WL MINIOBJ WARNING: deallocate (free) object: %%p, with type: %%u\n", obj, obj->type);
        free(obj);
        wl_miniobj_total_deallocations++;
    }
}

// OBJ, SYSCALL, CALLBACK, SYSCALL, CALLBACK, ...
void wl_miniobj_internal_reg(uint16_t obj_type, int argc, ...) {
    if (wl_miniobj__registry_count == 65535) {
        printf("WL MINIOBJ ERROR: maximum number of objects have been registered, please unregister an object in order to make room for another object to be registered\n");
        return;
    }

    if (argc %% 2 != 0) {
        printf("WL MINIOBJ ERROR: argc must be a multiple of 2\n");
        return;
    }

    wl_miniobj_init();

    if (wl_miniobj__registry[obj_type].registered) {
        printf("WL MINIOBJ ERROR: type conflict: %%u\n", obj_type);
        return;
    }

    wl_miniobj__registry[obj_type].registered = true;


    va_list ap;
    va_start(ap, argc);

    uint16_t ac = argc/2;
   
    wl_miniobj__registry[obj_type].references = 0;
    wl_miniobj__registry[obj_type].count = ac;
    wl_miniobj__registry[obj_type].accepted_syscalls = (uint16_t*) malloc(ac*sizeof(uint16_t));
    wl_miniobj__registry[obj_type].callbacks = (WL_MINIOBJ_CALLBACK*) malloc(ac*sizeof(WL_MINIOBJ_CALLBACK));
    
    for(int i = 0; i < ac; i++) {
        wl_miniobj__registry[obj_type].accepted_syscalls[i] = (uint16_t)va_arg(ap, int);
        wl_miniobj__registry[obj_type].callbacks[i] = va_arg(ap, WL_MINIOBJ_CALLBACK);
    }

    wl_miniobj__registry_count++;
}

void wl_miniobj_unreg(uint16_t obj_type) {
    if (wl_miniobj__registry_count == 0) {
        printf("WL MINIOBJ ERROR: no objects have been registered, cannot unregister type %%u\n", obj_type);
        return;
    }
    if (wl_miniobj__registry[obj_type].registered) {
        if (wl_miniobj__registry[obj_type].references != 0) {
            printf("WL MINIOBJ ERROR: type cannot be unregistered while there are references to it: %%u\n", obj_type);
            return;
        }
        free(wl_miniobj__registry[obj_type].callbacks);
        free(wl_miniobj__registry[obj_type].accepted_syscalls);
        wl_miniobj__registry[obj_type].callbacks = NULL;
        wl_miniobj__registry[obj_type].accepted_syscalls = NULL;
        wl_miniobj__registry[obj_type].count = 0;
        wl_miniobj__registry[obj_type].registered = false;
        wl_miniobj__registry_count--;
    } else {
        printf("WL MINIOBJ ERROR: type already unregistered: %%u\n", obj_type);
    }
}

int WL_MINIOBJ__DO_SYSCALL(int fd, uint16_t syscall, int argc, ...) {
    if (wl_miniobj__registry_count == 0) {
        printf("WL MINIOBJ ERROR: no objects have been registered\n");
        return -1;
    }

    // we cannot use wl_miniobj_get here as it would ambiguate errors
    //
    wl_miniobj * obj = (wl_miniobj *) wl_miniobj_get_priv(fd);

    if (obj == NULL) {
        printf("WL MINIOBJ ERROR: NULL object was passed to syscall\n");
        return -1;
    }
    int data = 0;
    va_list ap;
    va_start(ap, argc);
    switch(syscall) {
)");
    for(uint16_t i = 0; i < len; i++) {
        if (syscalls[i].current_typedef.length() != 0) continue;
    fprintf(code, R"(
        case %u: // wl_syscall__%s
        {
            if (!wl_miniobj__registry[obj->type].registered) {
                printf("WL MINIOBJ ERROR: syscall 'wl_syscall__%s' has been passed an unregistered object type: %%u\n", obj->type);
                break;
            }
            bool found_syscall = false;
            for(uint16_t i = 0; i < wl_miniobj__registry[obj->type].count; i++) {
                if (wl_miniobj__registry[obj->type].accepted_syscalls[i] == %u) {
                    found_syscall = true;
                    data = wl_miniobj__registry[obj->type].callbacks[i](fd, argc, ap);
                    break;
                }
            }
            if (!found_syscall) {
                printf("WL MINIOBJ ERROR: could not find syscall 'wl_syscall__%s' for object type %%u\n", obj->type);
            }
            break;
        }
)", i, syscalls[i].current_syscall.c_str(), syscalls[i].current_syscall.c_str(), i, syscalls[i].current_syscall.c_str());
    }
    fprintf(code, R"(
        default:
        {
            printf("WL MINIOBJ ERROR: unknown syscall: %%u\n", syscall);
            break;
        }
    }
    va_end(ap);
    return data;
}


)");

    fprintf(header, "// SYSCALLS START\n");
    for(uint16_t i = 0; i < len; i++) {
        fprintf(header, "//     %s\n", syscalls[i].current_syscall.c_str());
    }
    fprintf(header, "// SYSCALLS END\n\n");

    for(uint16_t i = 0; i < len; i++) {
        const char * t = NULL;
        std::string tu;
        if (syscalls[i].current_typedef.length() != 0) {
            t = syscalls[i].current_typedef.c_str();
            tu = syscalls[i].current_typedef.c_str();
            std::transform(tu.begin(), tu.end(), tu.begin(), std::toupper);
        }
        auto s = syscalls[i].current_syscall.c_str();
        std::string su = syscalls[i].current_syscall.c_str();
        std::transform(su.begin(), su.end(), su.begin(), std::toupper);

        auto & comments = syscalls[i].comment;

        if (t == NULL) {
            for (std::string & comment : comments) {
                fprintf(header, "// %s\n", comment.c_str());
            }
            fprintf(header, "extern const int WL_MINIOBJ_SYSCALL__NR__%s;\n\n", (t == NULL ? su : tu).c_str());
            fprintf(code, "const int WL_MINIOBJ_SYSCALL__NR__%s = %u;\n", (t == NULL ? su : tu).c_str(), i);
        }

        auto & argument_count = syscalls[i].current_argument_count;
        auto & argument_declaration = syscalls[i].current_arguments;
        auto & argument_usage = syscalls[i].current_arguments_usages;

        printf("syscall        : %s\n", s);
        if (t != NULL) {
        printf("typedef as     : %s\n", t);
        }
        if (argument_declaration.length() == 0) {
        printf("argument count : 0\n\n");
        }
        else if (argument_count.length() == 0) {
        printf("argument count : up to 125\n");
        printf("argument decl  : varadic arguments (...)\n\n");
        } else {
        printf("argument count : %s\n", argument_count.c_str());
        printf("argument decl  : %s\n", argument_declaration.length() == 0 ? "<none>" : argument_declaration.c_str());
        printf("argument usage : %s\n\n", argument_declaration.length() == 0 ? "<none>" : argument_usage.c_str());
        }

        if (
            argument_declaration.length() == 3
            && memcmp(argument_declaration.c_str(), "...", 3*sizeof(char)) == 0
        ) {
                for (std::string & comment : comments) {
                    fprintf(header, "// %s\n", comment.c_str());
                }
                fprintf(header, "#define wl_syscall__%s(fd, ...) WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__%s, WL_MINIOBJ_MACRO____PP_NARG(0, 0, ## __VA_ARGS__) - 1, ## __VA_ARGS__)\n", s, su.c_str());
        } else {
            if (argument_declaration.length() == 0) {
                for (std::string & comment : comments) {
                    fprintf(header, "// %s\n", comment.c_str());
                }
                fprintf(header, "static inline int wl_syscall__%s(int fd) {\n", s);
                if (t == NULL) {
                    fprintf(header, "    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__%s, 0);\n", su.c_str());
                } else {
                    fprintf(header, "    return wl_syscall__%s(fd);\n", t);
                }
                fprintf(header, "}\n");
            } else {
                for (std::string & comment : comments) {
                    fprintf(header, "// %s\n", comment.c_str());
                }
                fprintf(header, "static inline int wl_syscall__%s(int fd, %s) {\n", s, argument_declaration.c_str());
                if (t == NULL) {
                    fprintf(header, "    return WL_MINIOBJ__DO_SYSCALL(fd, WL_MINIOBJ_SYSCALL__NR__%s, %s, %s);\n", su.c_str(), argument_count.c_str(), argument_usage.c_str());
                } else {
                    fprintf(header, "    return wl_syscall__%s(fd, %s);\n", t, argument_usage.c_str());
                }
                fprintf(header, "}\n");
            }
        }
        if (i != len - 1) {
            fprintf(header, "\n\n\n");
        }
    }

fprintf(header, R"(
#ifdef __cplusplus
}
#endif

#endif // WL_SYSCALLS_H
)");

    fflush(header);
    fclose(header);
    fflush(code);
    fclose(code);
/*
// EXAMPLE
wl_miniobj* create_A(void) {
    wl_miniobj* o = wl_miniobj_create(2, 2);
    return o;
}

uint16_t del_A(wl_miniobj* obj, const int argc, va_list args) {
    if (obj->version < 2) {
        puts("obj is not A, version too low");
        return 0;
    }
    if (obj->type != 2) {
        puts("obj is not A");
        return 0;
    }
    puts("obj is A");
    free(obj);
    return 0;
}

wl_miniobj* create_B(void) {
    wl_miniobj* o = wl_miniobj_create(1, 1);
    return o;
}

uint16_t del_B(wl_miniobj* obj, const int argc, va_list args) {
    if (obj->version < 1) {
        puts("obj is not B, version too low");
        return 0;
    }
    if (obj->type != 1) {
        puts("obj is not B");
        return 0;
    }
    puts("obj is B");
    free(obj);
    return 0;
}

int main() {
    int A = 2, B = 1;

    wl_miniobj_reg(A, WL_MINIOBJ_SYSCALL__NR__DESTROY, del_A);
    wl_miniobj_reg(B, WL_MINIOBJ_SYSCALL__NR__DESTROY, del_B);

    wl_syscall__destroy(create_A()); wl_syscall__destroy(create_B());
    
    wl_miniobj_unreg(A);

    wl_syscall__destroy(create_A()); wl_syscall__destroy(create_B());
    
    wl_miniobj_reg(A, WL_MINIOBJ_SYSCALL__NR__DESTROY, del_B);

    wl_syscall__destroy(create_A()); wl_syscall__destroy(create_B());

    wl_miniobj_unreg(A);
    wl_miniobj_unreg(B);

    wl_miniobj_unreg(A);
    wl_miniobj_unreg(B);

    return 0;
}
*/

    return 0;
}