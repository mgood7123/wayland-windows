#include <gtest/gtest.h>
#include <wl_posix.h>

#include <windows.h>
#include <stdio.h>
#include <process.h>

#include "deadlock.h"

#include <mhook-lib/mhook.h>

const char * C_RUNTIME =
#ifdef _DLL
    "ucrtbase"
#else
#error UNKNOWN C RUNTIME
#endif
#ifdef _DEBUG
"d"
#endif
;

const char * VSC_RUNTIME =
#ifdef _DLL
    "vcruntime140"
#else
#error UNKNOWN MSVS C RUNTIME
#endif
#ifdef _DEBUG
"d"
#endif
;


#define FPH_BeginThread(N) uintptr_t N( void( __cdecl *start_address )( void * ), unsigned stack_size, void *arglist)
typedef FPH_BeginThread((*FP_BeginThread));
FP_BeginThread FP_R_BeginThread = (FP_BeginThread)GetProcAddress(GetModuleHandleA(C_RUNTIME), "_beginthread");
static FPH_BeginThread(FP_H_BeginThread) {
    printf("HOOK BeginThread HOOK ENTER\n");
    auto r = FP_R_BeginThread(start_address, stack_size, arglist);
    printf("HOOK BeginThread HOOK EXIT\n");
    return r;
}

#define FPH_EndThread(N) void N( void )
typedef FPH_EndThread((*FP_EndThread));
FP_EndThread FP_R_EndThread = (FP_EndThread)GetProcAddress(GetModuleHandleA(C_RUNTIME), "_endthread");
static FPH_EndThread(FP_H_EndThread) {
    printf("HOOK EndThread HOOK ENTER\n");
    FP_R_EndThread();
    printf("HOOK EndThread HOOK EXIT\n");
}

#define FPH_BeginThreadEx(N) uintptr_t N(void *security, unsigned stack_size, unsigned ( __stdcall *start_address )( void * ), void *arglist, unsigned initflag, unsigned *thrdaddr)
typedef FPH_BeginThreadEx((*FP_BeginThreadEx));
FP_BeginThreadEx FP_R_BeginThreadEx = (FP_BeginThreadEx)GetProcAddress(GetModuleHandleA(C_RUNTIME), "_beginthreadex");
static FPH_BeginThreadEx(FP_H_BeginThreadEx) {
    printf("HOOK BeginThreadEx HOOK ENTER\n");
    auto r = FP_R_BeginThreadEx(security, stack_size, start_address, arglist, initflag, thrdaddr);
    printf("HOOK BeginThreadEx HOOK EXIT\n");
    return r;
}

#define FPH_EndThreadEx(N) void N( unsigned retval )
typedef FPH_EndThreadEx((*FP_EndThreadEx));
FP_EndThreadEx FP_R_EndThreadEx = (FP_EndThreadEx)GetProcAddress(GetModuleHandleA(C_RUNTIME), "_endthreadex");
static FPH_EndThreadEx(FP_H_EndThreadEx) {
    printf("HOOK EndThreadEx HOOK ENTER\n");
    FP_R_EndThreadEx(retval);
    printf("HOOK EndThreadEx HOOK EXIT\n");
}

#define FPH_CreateThread(N) HANDLE N( \
  [in, optional]  LPSECURITY_ATTRIBUTES   lpThreadAttributes, \
  [in]            SIZE_T                  dwStackSize, \
  [in]            LPTHREAD_START_ROUTINE  lpStartAddress, \
  [in, optional]  __drv_aliasesMem LPVOID lpParameter, \
  [in]            DWORD                   dwCreationFlags, \
  [out, optional] LPDWORD                 lpThreadId \
)
typedef FPH_CreateThread((*FP_CreateThread));
FP_CreateThread FP_R_CreateThread = (FP_CreateThread)GetProcAddress(GetModuleHandleA("Kernel32"), "CreateThread");
static FPH_CreateThread(FP_H_CreateThread) {
    printf("HOOK CreateThread HOOK ENTER\n");
    auto r = FP_R_CreateThread(lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
    printf("HOOK CreateThread HOOK EXIT\n");
    return r;
}

#define FPH_ExitThread(N) void N([in] DWORD dwExitCode)
typedef FPH_ExitThread((*FP_ExitThread));
FP_ExitThread FP_R_ExitThread = (FP_ExitThread)GetProcAddress(GetModuleHandleA("Kernel32"), "ExitThread");
static FPH_ExitThread(FP_H_ExitThread) {
    printf("HOOK ExitThread HOOK ENTER\n");
    FP_R_ExitThread(dwExitCode);
    printf("HOOK ExitThread HOOK EXIT\n");
}

#define FPH_GetExitCodeThread(N) BOOL N( \
  [in] HANDLE thread, \
  [out] LPDWORD dwExitCode \
)
typedef FPH_GetExitCodeThread((*FP_GetExitCodeThread));
FP_GetExitCodeThread FP_R_GetExitCodeThread = (FP_GetExitCodeThread)GetProcAddress(GetModuleHandleA("Kernel32"), "GetExitCodeThread");
static FPH_GetExitCodeThread(FP_H_GetExitCodeThread) {
    printf("HOOK GetExitCodeThread HOOK ENTER\n");
    auto r = FP_R_GetExitCodeThread(thread, dwExitCode);
    printf("HOOK GetExitCodeThread HOOK EXIT\n");
    return r;
}

#define FPH_TerminateThread(N) BOOL N( \
  [in] HANDLE thread, \
  [in] DWORD dwExitCode \
)
typedef FPH_TerminateThread((*FP_TerminateThread));
FP_TerminateThread FP_R_TerminateThread = (FP_TerminateThread)GetProcAddress(GetModuleHandleA("Kernel32"), "TerminateThread");
static FPH_TerminateThread(FP_H_TerminateThread) {
    printf("HOOK TerminateThread HOOK ENTER\n");
    auto r = FP_R_TerminateThread(thread, dwExitCode);
    printf("HOOK TerminateThread HOOK EXIT\n");
    return r;
}


// our test thread
// we can install a signal handler via syscalls

HANDLE current_thread;

// lives in program memory, we can directly access it in assembly without touching any registers.
#define PROGRAM_MEMORY_VARIABLE(x) extern "C" x; x

PROGRAM_MEMORY_VARIABLE(DWORD64 jump_address);

PROGRAM_MEMORY_VARIABLE(int handled);

static DWORD thread2(LPVOID data) { 
    while(*(int*)data == 0) {}
    return 0;
}

struct reg {
    // general registers
    DWORD64 rax, rbx, rcx, rdx, rbp, rsp, rsi, rdi,
            r8, r9, r10, r11, r12, r13, r14, r15;
    
    // instruction pointer
    DWORD64 rip;

/*
    // attempting to support FPU state save/restore seems very difficult
    // as the linux kernel maintains in internal state along with cpu info about FPU
    // including thread-local structures for FPU as well as conditional FPU save/restore (task tracking FPU use) (can reuse FPU registers when switching tasks)
    
    typedef struct DECLSPEC_ALIGN(16) {
        DWORD64 Low;
        DWORD64 High;
    } DWORD128;

    typedef struct DECLSPEC_ALIGN(16) {
        DWORD128 Low;
        DWORD128 High;
    } DWORD256;

    typedef struct DECLSPEC_ALIGN(16) {
        DWORD256 Low;
        DWORD256 High;
    } DWORD512;

    DECLSPEC_ALIGN(16) uint8_t fxstate[512];
    
    // floating point registers
    DWORD64 st[8];

    // floating point registers - SSE
    DWORD128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7,
             xmm8, xmm9, xmm10, xmm11, xmm12, xmm13, xmm14, xmm15;

    // floating point registers - AVX-256
    DWORD256 ymm0, ymm1, ymm2, ymm3, ymm4, ymm5, ymm6, ymm7,
             ymm8, ymm9, ymm10, ymm11, ymm12, ymm13, ymm14, ymm15;

    // floating point registers - AVX-512
    DWORD512 zmm0, zmm1, zmm2, zmm3, zmm4, zmm5, zmm6, zmm7,
             zmm8, zmm9, zmm10, zmm11, zmm12, zmm13, zmm14, zmm15;
*/
};

PROGRAM_MEMORY_VARIABLE(struct reg current_reg);

#define set_reg(reg) __asm__ __volatile__ ("movq %0, %%" #reg "\n" :: "m"(current_reg.reg))
#define get_reg(reg) __asm__ __volatile__ ("movq %%" #reg ", %0\n" : "=m"(current_reg.reg))

#define get_regs__general() \
    get_reg(rax); \
    get_reg(rbx); \
    get_reg(rcx); \
    get_reg(rdx); \
    get_reg(rbp); \
    get_reg(rsp); \
    get_reg(rsi); \
    get_reg(rdi); \
    get_reg(r8); \
    get_reg(r9); \
    get_reg(r10); \
    get_reg(r10); \
    get_reg(r10); \
    get_reg(r10); \
    get_reg(r10); \
    get_reg(r15); \
 
#define set_regs__general() \
    set_reg(rax); \
    set_reg(rbx); \
    set_reg(rcx); \
    set_reg(rdx); \
    set_reg(rbp); \
    set_reg(rsp); \
    set_reg(rsi); \
    set_reg(rdi); \
    set_reg(r8); \
    set_reg(r9); \
    set_reg(r10); \
    set_reg(r10); \
    set_reg(r10); \
    set_reg(r10); \
    set_reg(r10); \
    set_reg(r15); \
 
#define get_regs() get_regs__general()
#define set_regs() set_regs__general()

void print_reg(struct reg * reg) {
    printf("REG:");
    // printf("    Register parameter home addresses:\n");
    // unsigned int64 can hold upto 20 digits
    // printf("        P1Home:               0x%0*llX       P2Home: 0x%0*llX  \n", 16, reg->p1Home, 16, reg->p2Home); // DWORD64
    // printf("        P3Home:               0x%0*llX       P4Home: 0x%0*llX  \n", 16, reg->p3Home, 16, reg->p4Home); // DWORD64
    // printf("        P5Home:               0x%0*llX       P6Home: 0x%0*llX  \n", 16, reg->p5Home, 16, reg->p6Home); // DWORD64
    // printf("\n    Control flags:\n");
    // printf("        ContextFlags:         0x%0*lX       MxCsr:  0x%0*lX  \n", 16, reg->contextFlags, 16, reg->mxCsr); // DWORD
    // printf("\n    Segment Registers and processor flags:\n");
    // printf("        SegCs:                0x%0*X       SegDs:  0x%0*X\n", 16, reg->segCs, 16, reg->segDs); // WORD
    // printf("        SegEs:                0x%0*X       SegFs:  0x%0*X\n", 16, reg->segEs, 16, reg->segFs); // WORD
    // printf("        SegGs:                0x%0*X       SegSs:  0x%0*X\n", 16, reg->segGs, 16, reg->segSs); // WORD
    // printf("        EFlags:               0x%0*lX \n", 16, reg->eFlags); // DWORD
    // printf("\n    Debug registers:\n");
    // printf("        Dr0:                  0x%0*llX       Dr1:    0x%0*llX  \n", 16, reg->dr0, 16, reg->dr1); // DWORD64
    // printf("        Dr2:                  0x%0*llX       Dr3:    0x%0*llX  \n", 16, reg->dr2, 16, reg->dr3); // DWORD64
    // printf("        Dr6:                  0x%0*llX       Dr7:    0x%0*llX  \n", 16, reg->dr6, 16, reg->dr7); // DWORD64
    printf("\n    Integer registers:\n");
    printf("        Rax:                  0x%0*llX       Rcx:    0x%0*llX  \n", 16, reg->rax, 16, reg->rcx); // DWORD64
    printf("        Rdx:                  0x%0*llX       Rbx:    0x%0*llX  \n", 16, reg->rdx, 16, reg->rbx); // DWORD64
    printf("        Rsp:                  0x%0*llX       Rbp:    0x%0*llX  \n", 16, reg->rsp, 16, reg->rbp); // DWORD64
    printf("        Rsi:                  0x%0*llX       Rdi:    0x%0*llX  \n", 16, reg->rsi, 16, reg->rdi); // DWORD64
    printf("        R8:                   0x%0*llX       R9:     0x%0*llX  \n", 16, reg->r8, 16, reg->r9); // DWORD64
    printf("        R10:                  0x%0*llX       R11:    0x%0*llX  \n", 16, reg->r10, 16, reg->r11); // DWORD64
    printf("        R12:                  0x%0*llX       R13:    0x%0*llX  \n", 16, reg->r12, 16, reg->r13); // DWORD64
    printf("        R14:                  0x%0*llX       R15:    0x%0*llX  \n", 16, reg->r14, 16, reg->r15); // DWORD64
    // printf("\n    Program counter:\n");
    // printf("        Rip:                  0x%0*llX  \n", 16, reg->rip); // DWORD64
    // // printf("\n    Special debug control registers:\n");
    // printf("        DebugControl:         0x%0*llX  \n", 16, reg->debugControl); // DWORD64
    // printf("        LastBranchToRip:      0x%0*llX  \n", 16, reg->lastBranchToRip); // DWORD64
    // printf("        LastBranchFromRip:    0x%0*llX  \n", 16, reg->lastBranchFromRip); // DWORD64
    // printf("        LastExceptionToRip:   0x%0*llX  \n", 16, reg->lastExceptionToRip); // DWORD64
    // printf("        LastExceptionFromRip: 0x%0*llX  \n", 16, reg->lastExceptionFromRip); // DWORD64
}

#undef PROGRAM_MEMORY_VARIABLE

#include <cpuid.h>

// variable lives at a known address in the executable, it can be referenced directly via ds:[<VARIABLE>]
//
// setting these will assemble to a single mov instruction if the data being assigned is not complex
//

#define PROGRAM_MEMORY_VARIABLE(x) extern "C" x; x

PROGRAM_MEMORY_VARIABLE(DWORD32 cpuid__eax);
PROGRAM_MEMORY_VARIABLE(DWORD32 cpuid__ebx);
PROGRAM_MEMORY_VARIABLE(DWORD32 cpuid__ecx);
PROGRAM_MEMORY_VARIABLE(DWORD32 cpuid__edx);



// https://github.com/microsoft/clang/blob/master/lib/Headers/cpuid.h

// we rely on __cpuid being macro defined as inline assembly

// reimplement as definitions to avoid additional cpu instructions

// optimize these for program memory variables

//#define get_reg(reg) __asm__ __volatile__ ("movq %%" #reg ", %0\n" : "=m"(current_reg.reg))
//#define set_reg(reg) __asm__ __volatile__ ("movq %0, %%" #reg "\n" :: "m"(current_reg.reg))

// we load/store directly to memory

// save and restore the following registers: eax, ebc, ecd, edx
// we cannot clobber them since the compiler may generate a push before the asm itself gets generated
//   unbalancing the stack if we jump before the pop gets called
//  the compiler might also generate an add rsp followed by sub rsp, similar to push pop generated
#if __i386__
#define safe__cpuid(__leaf, __eax, __ebx, __ecx, __edx) \
    __asm__ __volatile__ ( \
        "  pushl %%eax\n" \
        "  pushl %%ebx\n" \
        "  pushl %%ecx\n" \
        "  pushl %%edx\n" \
        "  movl  %4,%%eax\n" \
        "  cpuid\n" \
        "  movl  %%eax, %0\n" \
        "  movl  %%ebx, %1\n" \
        "  movl  %%ecx, %2\n" \
        "  movl  %%edx, %3\n" \
        "  popl %%edx\n" \
        "  popl %%ecx\n" \
        "  popl %%ebx\n" \
        "  popl %%eax\n" \
        : "=m"(__eax), "=m" (__ebx), "=m"(__ecx), "=m"(__edx) \
        : "m"(__leaf) \
    )

#define safe__cpuid_count(__leaf, __sub_leaf, __eax, __ebx, __ecx, __edx) \
    __asm__ __volatile__ ( \
        "  pushl %%eax\n" \
        "  pushl %%ebx\n" \
        "  pushl %%ecx\n" \
        "  pushl %%edx\n" \
        "  movl  %4,%%eax\n" \
        "  movl  %5,%%ecx\n" \
        "  cpuid\n" \
        "  movl  %%eax, %0\n" \
        "  movl  %%ebx, %1\n" \
        "  movl  %%ecx, %2\n" \
        "  movl  %%edx, %3\n" \
        "  popl %%edx\n" \
        "  popl %%ecx\n" \
        "  popl %%ebx\n" \
        "  popl %%eax\n" \
        : "=m"(__eax), "=m" (__ebx), "=m"(__ecx), "=m"(__edx) \
        : "m"(__leaf), "m"(__sub_leaf) \
    )
#else
#define safe__cpuid(__leaf, __eax, __ebx, __ecx, __edx) \
    __asm__ __volatile__ ( \
        "  pushq %%rax\n" \
        "  pushq %%rbx\n" \
        "  pushq %%rcx\n" \
        "  pushq %%rdx\n" \
        "  movl  %4,%%eax\n" \
        "  cpuid\n" \
        "  movl  %%eax, %0\n" \
        "  movl  %%ebx, %1\n" \
        "  movl  %%ecx, %2\n" \
        "  movl  %%edx, %3\n" \
        "  popq %%rdx\n" \
        "  popq %%rcx\n" \
        "  popq %%rbx\n" \
        "  popq %%rax\n" \
        : "=m"(__eax), "=m" (__ebx), "=m"(__ecx), "=m"(__edx) \
        : "m"(__leaf) \
    )

#define safe__cpuid_count(__leaf, __sub_leaf, __eax, __ebx, __ecx, __edx) \
    __asm__ __volatile__ ( \
        "  pushq %%rax\n" \
        "  pushq %%rbx\n" \
        "  pushq %%rcx\n" \
        "  pushq %%rdx\n" \
        "  movl  %4,%%eax\n" \
        "  movl  %5,%%ecx\n" \
        "  cpuid\n" \
        "  movl  %%eax, %0\n" \
        "  movl  %%ebx, %1\n" \
        "  movl  %%ecx, %2\n" \
        "  movl  %%edx, %3\n" \
        "  popq %%rdx\n" \
        "  popq %%rcx\n" \
        "  popq %%rbx\n" \
        "  popq %%rax\n" \
        : "=m"(__eax), "=m" (__ebx), "=m"(__ecx), "=m"(__edx) \
        : "m"(__leaf), "m"(__sub_leaf) \
    )
#endif

#if __i386__

// 32 bit needs to check for support

PROGRAM_MEMORY_VARIABLE(uint8_t cpuid__supported);
PROGRAM_MEMORY_VARIABLE(uint8_t cpuid__supported_checked);

#define check_cpuid_supported \
    if (cpuid__supported_checked == 0) { \
        __asm("  pushfl\n" \
            "  popl   %%eax\n" \
            "  movl   %%eax,%%ecx\n" \
            "  xorl   $0x00200000,%%eax\n" \
            "  pushl  %%eax\n" \
            "  popfl\n" \
            "  pushfl\n" \
            "  popl   %%eax\n" \
            "  movl   $0,%0\n" \
            "  cmpl   %%eax,%%ecx\n" \
            "  je     1f\n" \
            "  movl   $1,%0\n" \
            "1:" \
            : "=r" (cpuid__supported) : : "eax", "ecx"); \
        cpuid__supported_checked = 1; \
    }

#define IF_CPUID_IS_SUPPORTED if (cpuid__supported == 1)

#else

// 64 bit seems to always support cpuid according to clang cpuid.h

PROGRAM_MEMORY_VARIABLE(uint8_t cpuid__supported) = 1;

// nop on 64-bit
#define check_cpuid_supported ;
// nop on 64-bit
#define IF_CPUID_IS_SUPPORTED

#endif

// https://cdrdv2.intel.com/v1/dl/getContent/671200
// Intel® 64 and IA-32 Architectures Software Developer’s Manual
//  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
//
PROGRAM_MEMORY_VARIABLE(uint8_t CPU_IS_INTEL);

// https://www.amd.com/system/files/TechDocs/40332.pdf
// AMD64 Architecture Programmer’s Manual
//  Volumes 1–5
//
PROGRAM_MEMORY_VARIABLE(uint8_t CPU_IS_AMD);

PROGRAM_MEMORY_VARIABLE(uint8_t CPU_IS_UNSUPPORTED);

// https://github.com/digiks/ALPS-MP-M0.MP1-V2.34_VZ6580_WE_I_M_KERNEL/blob/4236a5fa2f7bd848497ad140f314156e5d6f7b23/arch/x86/kernel/cpu/microcode/core_early.c

#define QCHAR(a, b, c, d) ((a) + ((b) << 8) + ((c) << 16) + ((d) << 24))
#define CPUID_INTEL1 QCHAR('G', 'e', 'n', 'u')
#define CPUID_INTEL2 QCHAR('i', 'n', 'e', 'I')
#define CPUID_INTEL3 QCHAR('n', 't', 'e', 'l')
#define CPUID_AMD1 QCHAR('A', 'u', 't', 'h')
#define CPUID_AMD2 QCHAR('e', 'n', 't', 'i')
#define CPUID_AMD3 QCHAR('c', 'A', 'M', 'D')

#define CPUID_IS(ID, ebx, ecx, edx)	\
		(!((ebx ^ (CPUID_##ID##1))|(edx ^ (CPUID_##ID##2))|(ecx ^ (CPUID_##ID##3))))

#define CPUID(EAX) __cpuid(EAX, cpuid__eax, cpuid__ebx, cpuid__ecx, cpuid__edx)
#define CPUIDEX(EAX, ECX) __cpuid_count(EAX, ECX, cpuid__eax, cpuid__ebx, cpuid__ecx, cpuid__edx)

// incase we want to implement ptrace api
struct FPU_DATA_REGISTER {
    // each FPU data register is 80 bits
    // 8 bits * 10 = 80 bits
    uint8_t data[10];
};


PROGRAM_MEMORY_VARIABLE(uint8_t FPU_STATE_USE_Z_SAVE__FSAVE) = 1;
PROGRAM_MEMORY_VARIABLE(uint8_t FPU_STATE_USE_Z_SAVE__FXSAVE) = 2;
PROGRAM_MEMORY_VARIABLE(uint8_t FPU_STATE_USE_Z_SAVE__XSAVE) = 3;
PROGRAM_MEMORY_VARIABLE(DWORD32 FPU_XSAVE_SIZE);


PROGRAM_MEMORY_VARIABLE(uint8_t FPU_STATE_USE_Z_SAVE);
PROGRAM_MEMORY_VARIABLE(uint8_t FPU_STATE_DETERMINED);
PROGRAM_MEMORY_VARIABLE(uint8_t FPU_STATE_DETERMINED_LOCK) = 0; // any two threads CANNOT determine FPU state at the same time

// only these need be thread-local, everything else must be thread-safe
PROGRAM_MEMORY_VARIABLE(thread_local DECLSPEC_ALIGN(16) uint8_t FPU_STATE_FSAVE[108]);
PROGRAM_MEMORY_VARIABLE(thread_local DECLSPEC_ALIGN(16) uint8_t FPU_STATE_FXSAVE[512]);
PROGRAM_MEMORY_VARIABLE(thread_local uint8_t * FPU_STATE_XSAVE); // dynamically allocated based on available xsave features


// 8-bit lock
#define SPIN_LOCK__ENTER_8(SPIN_SECTION_NAME, LOCK) \
__asm__ __volatile__ ( \
    #SPIN_SECTION_NAME ":\n" \
    "    cmpb $0, %0\n" \
    "    je " #SPIN_SECTION_NAME "__GET_LOCK\n" \
    "    pause\n" \
    "    jmp " #SPIN_SECTION_NAME "\n" \
    #SPIN_SECTION_NAME "__GET_LOCK:\n" \
    "    movb $1, %%AL\n" \
    "    xchgb %0, %%AL\n" \
    "    cmpb $0, %%AL\n" \
    "    jne " #SPIN_SECTION_NAME "\n" \
    : "=m"(LOCK) : "m"(LOCK) : "eax" /* eax is clobbered because we modify it */ \
)

// 8-bit lock
#define SPIN_LOCK__EXIT_8(LOCK) \
__asm__ __volatile__ ( \
    "movb $0, %0\n" \
    :"=m"(LOCK) \
)

extern "C" void thread2_handler() {
}

extern "C" void thread2_handle() {
    handled = 1;
    get_regs();
    SPIN_LOCK__ENTER_8(SPIN, FPU_STATE_DETERMINED_LOCK);
    if (FPU_STATE_DETERMINED == 0) {

        // if we have not determined FPU state saving, do so now
        // we will assume that this does not change to prevent programs exploding due to features suddenly becoming unavailable

        // if the cpuid is not supported, then we cannot use FPU at all
        // if the cpuid 0 returns eax 0, then we cannot use FPU at all
        // if the cpuid 0 vendor string other than intel and amd, then notify that cpu is unsupported
        //   but still continue, we hope that the instructions are consistent across x86 vendors
        FPU_STATE_USE_Z_SAVE = 0;

        check_cpuid_supported;
        IF_CPUID_IS_SUPPORTED {
            // both Intel and AMD specify eax 0 returns the cpu id string in ebx, ecx, and edx, while eax returns the maximum value of input eax
            //
            // NOTE:
            //   On Intel 64 processors, CPUID clears the high 32 bits of the RAX/RBX/RCX/RDX registers in all modes.
            //

            // Intel® 64 and IA-32 Architectures Software Developer’s Manual
            //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
            //   page 813
            //
            // AMD64 Architecture Programmer’s Manual
            //  Volumes 1–5
            //   page 1838
            // 
            CPUID(0);


            if (cpuid__eax == 0) {
                // max feature level is zero, we cannot determine support for extended region nor fpu
                FPU_STATE_DETERMINED = 1;
                goto END_FPU;
            }


            if (CPUID_IS(INTEL, cpuid__ebx, cpuid__ecx, cpuid__edx)) {
                CPU_IS_INTEL = 1;
            } else if (CPUID_IS(AMD, cpuid__ebx, cpuid__ecx, cpuid__edx)) {
                CPU_IS_AMD = 1;
            } else {
                CPU_IS_UNSUPPORTED = 1;
            }

            // if we get here, we have a supported CPU

            // both Intel and AMD use eax 1 for XSAVE

            if (cpuid__eax < 1) {
                // we cannot obtain information about XSAVE, nor FPU support
                goto END_FPU;
            }

            // we can obtain information about XSAVE
            //
            // Intel® 64 and IA-32 Architectures Software Developer’s Manual
            //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
            //   page 813
            //
            // AMD64 Architecture Programmer’s Manual
            //  Volumes 1–5
            //   page 1839
            //
            // for Intel, refer to table below for more detail of return values of ecx and edx
            //
            // Intel® 64 and IA-32 Architectures Software Developer’s Manual
            //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
            //   page 837
            //
            CPUID(1);
            #define RBIT(reg, bit) (reg & (1 << bit))

            // first, we need to save FPU stack
            // this is done via FXSR instructions
            // both Intel and AMD use bits 0 to FPU support and 24 for FXSAVE/FXRSTOR support
            if (RBIT(cpuid__edx, 0) == 1) {
                // FPU is supported, FSAVE and FRSTOR exist as well

                // CPUID.1:ECX.XSAVE[bit 26] enumerates general support for the XSAVE feature set
                // If this bit is 1, the processor supports the following instructions: XGETBV, XRSTOR, XSAVE, and XSETBV.1
                // Further enumeration is provided through CPUID function 0DH.

                // on AMD eax 2 to 4 are reserved
                // on Intel eax 2 to 4 provide (2) Cache and TLB info, (3) processor serial number, (4) Deterministic Cache Parameters (additional input ECX)
                //
                // we do not require these but is useful to note
                //

                // INTEL NOTE:
                //   CPUID leaves above 2 and below 80000000H are visible only when IA32_MISC_ENABLE[bit 22] has its default value of 0.
                //
                // however, "Jester01: I am not aware of any BIOS that sets it, or even has an option to do so, and all OSes clear it explicitly"
                //

                // both Intel and AMD use eax 7 for extended feature info, including max input value for ecx (stored in eax)

                if (RBIT(cpuid__ecx, 26) == 0 || RBIT(cpuid__ecx, 27) == 0 || cpuid__eax < 0xD) {
                    // if bit 26 is 0 then processor does not support xsave feature set
                    // if bit 27 is 0 then processor has not enabled xsave feature set
                    // if eax is less than 0xD then we cannot obtain xsave area size
                    goto FPU_NO_XSAVE;
                }

                // AMD implies if YMM and related cannot be saved by FXSTOR, then related instructions that manipulate those registers also are not supported
                // we can assume this to be true for all XSAVE extended features

                //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
                //   page 821
                //
                // AMD64 Architecture Programmer’s Manual Volume 3: General-Purpose and System Instructions
                //   page 648
                //
                CPUIDEX(0xD, 0);

                if (cpuid__ecx == 576) {
                    // no extended features to save, this is basically FXSAVE plus 64-byte header
                    // we cannot jump to FPU_NO_XSAVE as we may support xsave but have no support for FXSAVE, in this case we must use xsave
                    if (RBIT(cpuid__edx, 24) == 1) {
                        // FXSAVE and FXSTOR are supported
                        FPU_STATE_DETERMINED = 1;
                        FPU_STATE_USE_Z_SAVE = FPU_STATE_USE_Z_SAVE__FXSAVE;
                        goto USE_FPU;
                    }
                }

                FPU_STATE_DETERMINED = 1;
                FPU_STATE_USE_Z_SAVE = FPU_STATE_USE_Z_SAVE__XSAVE;
                FPU_XSAVE_SIZE = cpuid__ecx;
                goto USE_FPU;

                FPU_NO_XSAVE:

                if (RBIT(cpuid__edx, 24) == 1) {
                    // FXSAVE and FXSTOR are supported

                    // INTEL - Saves the current state of the x87 FPU, MMX technology, XMM, and MXCSR registers to a 512-byte memory
                    //         The content layout of the 512 byte region depends on whether the
                    //         processor is operating in non-64-bit operating modes or 64-bit sub-mode of IA-32e mode.
                    //
                    //         The format of the memory image saved with the FXSAVE instruction is the same regardless of the current
                    //         addressing mode (32-bit or 16-bit) and operating mode (protected, real address, or system management).
                    //
                    // AMD - The architecture supports two 512-bit memory formats for FXSAVE, a 64-bit format that saves
                    //       XMM0-XMM15, and a 32-bit legacy format that saves only XMM0-XMM7. If FXSAVE is executed
                    //       in 64-bit mode, the 64-bit format is used, otherwise the 32-bit format is used. When the 64-bit format is
                    //       used, if the operand-size is 64-bit, FXSAVE saves the x87 pointer registers as offset64, otherwise it
                    //       saves them as sel:offset32.

                    //  AMD - FXSAVE does not save the x87 pointer registers (last instruction pointer, last data pointer, and last opcode) unless an exception has occured

                    FPU_STATE_DETERMINED = 1;
                    FPU_STATE_USE_Z_SAVE = FPU_STATE_USE_Z_SAVE__FXSAVE;
                    goto USE_FPU;
                } else {
                    // The FXSAVE and FXRSTOR instructions are not supported
                    FPU_STATE_DETERMINED = 1;
                    FPU_STATE_USE_Z_SAVE = FPU_STATE_USE_Z_SAVE__FSAVE;
                    goto USE_FPU;
                }
            }
        }
    }
    SPIN_LOCK__EXIT_8(FPU_STATE_DETERMINED_LOCK);

    USE_FPU:
    if (FPU_STATE_USE_Z_SAVE != 0) {
        // memory must always be zeroed before use
        if (FPU_STATE_USE_Z_SAVE == FPU_STATE_USE_Z_SAVE__XSAVE) {

            FPU_STATE_XSAVE = (uint8_t*)_mm_malloc(FPU_XSAVE_SIZE, 64);

            memset(FPU_STATE_XSAVE, 0, FPU_XSAVE_SIZE);

            // Intel® 64 and IA-32 Architectures Software Developer’s Manual
            //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
            //   page 2740 - XSAVE
            //   page 2731 - XRSTOR
            //
            // AMD64 Architecture Programmer’s Manual
            //  Volumes 1–5
            //   page 2800 - XSAVE
            //   page 2796 - XRSTOR

            // XSAVE - saves FXSAVE in addition to a 64 byte header, followed by YMM, LWP, MKP, CET_S, and CET_U registers
            // the extra registers are dependant on supported/enabled features and affect image size

            // Intel® 64 and IA-32 Architectures Software Developer’s Manual
            //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
            //   page 328 - XSAVE-MANAGED STATE
            //
            // AMD64 Architecture Programmer’s Manual
            //  Volumes 1–5
            //   page 813 - 11.5.2 XFEATURE_ENABLED_MASK
            //   page 814 - 11.5.3 Extended Save Area
            //   page 817 - 11.5.10 Mode-Specific XSAVE/XRSTOR State Management

            // The XSAVE header of an XSAVE area comprises the 64 bytes starting at offset 512 from the area’s base address
            // The extended region of an XSAVE area starts at byte offset 576 from the area’s base address
            
            // FXSAVE instruction does not check for pending unmasked floating-point exceptions. (The FXSAVE operation in
            // this regard is similar to the operation of the FNSAVE instruction).

            // Software can set any bit in EDX:EAX, regardless of whether the bit position in XCR0 is valid for the
            // processor. When the mask operand contains all 1's, all processor state components enabled in XCR0
            // are saved.

            // XSAVE does not initialize content, FSAVE does

            __asm__ __volatile__ (
                "fwait\n" // check for pending unmasked floating-point exceptions
                "xsave %0\n"
                "finit\n" // initialize content
                :: "m"(*FPU_STATE_XSAVE), "a"(-1), "d"(-1) // we write to this address given but we do not assign it
            );

        } else if (FPU_STATE_USE_Z_SAVE == FPU_STATE_USE_Z_SAVE__FXSAVE) {

            memset(FPU_STATE_FXSAVE, 0, 512);

            // Intel® 64 and IA-32 Architectures Software Developer’s Manual
            //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
            //   page 1057 - FXSAVE
            //   page 1054 - FXRSTOR
            //
            // AMD64 Architecture Programmer’s Manual
            //  Volumes 1–5
            //   page 2993 - FXSAVE
            //   page 2991 - FXRSTOR

            // FXSAVE - saves FSAVE in addition to XMM and MXCSR registers

            // AMD saves the data and instruction pointers when an exception is pending
            // INTEL always saves the data and instruction pointers

            // FXSAVE instruction does not check for pending unmasked floating-point exceptions. (The FXSAVE operation in
            // this regard is similar to the operation of the FNSAVE instruction).

            // FXSAVE does not initialize content, FSAVE does

            __asm__ __volatile__ (
                "fwait\n" // check for pending unmasked floating-point exceptions
                "fxsave %0\n"
                "finit\n" // initialize content
                :: "m"(*FPU_STATE_FXSAVE)
            );

        } else {

            memset(FPU_STATE_FSAVE, 0, 108);

            // Intel® 64 and IA-32 Architectures Software Developer’s Manual
            //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
            //   page 1020 - FSAVE
            //   page 1018 - FRSTOR
            //
            // AMD64 Architecture Programmer’s Manual
            //  Volumes 1–5
            //   page 2989 - FSAVE
            //   page 2987 - FRSTOR

            // FSAVE - saves FPU state in addition to Data/MMX registers (MMX registers are aliased to FPU data registers)

            // INTEL FSAVE always saves the full 64-bit data and instruction pointers
            // AMD FSAVE does not save the full 64-bit data and instruction pointers
            // AMD FRSTOR does not explicitly state whether it restores the full 64 bit pointers or not
            //
            // AMD FXSAVE does but it is unavailable for use
            // AMD FSTENV does not save the full 64-bit data and instruction pointers so we cannot use it

            // Intel® 64 and IA-32 Architectures Software Developer’s Manual
            //  Combined Volumes: 1, 2A, 2B, 2C, 2D, 3A, 3B, 3C, 3D and 4
            //   page 213 - x87 FPU Instruction and Data (Operand) Pointers
            //
            // AMD64 Architecture Programmer’s Manual
            //  Volumes 1–5
            //   page 332 - Pointers and Opcode State

            // Because the 64-bit media registers are physically aliased onto
            // the x87 registers, the FSAVE/FNSAVE and FRSTOR instructions can also be used to save and restore
            // the 64-bit media state. However, FSAVE/FNSAVE and FRSTOR do not save or restore the 128-bit media state
            
            __asm__ __volatile__ (
                "fwait\n"
                "fnsave %0\n"
                :: "m"(*FPU_STATE_FSAVE)
            );

        }
    }

    END_FPU:

    thread2_handler();

    if (FPU_STATE_USE_Z_SAVE != 0) {
        if (FPU_STATE_USE_Z_SAVE == FPU_STATE_USE_Z_SAVE__XSAVE) {
            __asm__ __volatile__ ("xrstor %0\n" :: "m"(*FPU_STATE_XSAVE), "a"(-1), "d"(-1));
            _mm_free(FPU_STATE_XSAVE);
        } else if (FPU_STATE_USE_Z_SAVE == FPU_STATE_USE_Z_SAVE__FXSAVE) {
            __asm__ __volatile__ ("fxrstor %0\n" :: "m"(*FPU_STATE_FXSAVE));
        } else {
            __asm__ __volatile__ ("frstor %0\n" :: "m"(*FPU_STATE_FSAVE));
        }
    }

    set_regs();
}


__attribute__((naked)) void thread2_ret() {
    // using a function call allows us to preserve the stack before we jump to our restore point
    // if we dont do this, compilers will emit return code AFTER our jump
    __asm__ __volatile__ ("call thread2_handle; jmp *%0\n" :: "m"(jump_address));
}

void print_context(CONTEXT context) {
    #define P_DWORD64(name, member) printf("        %s: %llu\n", #name, context.member)
    printf("CONTEXT:\n");
    printf("    Register parameter home addresses:\n");
    // unsigned int64 can hold upto 20 digits
    printf("        P1Home:               0x%0*llX       P2Home: 0x%0*llX  \n", 16, context.P1Home, 16, context.P2Home); // DWORD64
    printf("        P3Home:               0x%0*llX       P4Home: 0x%0*llX  \n", 16, context.P3Home, 16, context.P4Home); // DWORD64
    printf("        P5Home:               0x%0*llX       P6Home: 0x%0*llX  \n", 16, context.P5Home, 16, context.P6Home); // DWORD64
    printf("\n    Control flags:\n");
    printf("        ContextFlags:         0x%0*lX       MxCsr:  0x%0*lX  \n", 16, context.ContextFlags, 16, context.MxCsr); // DWORD
    printf("\n    Segment Registers and processor flags:\n");
    printf("        SegCs:                0x%0*X       SegDs:  0x%0*X\n", 16, context.SegCs, 16, context.SegDs); // WORD
    printf("        SegEs:                0x%0*X       SegFs:  0x%0*X\n", 16, context.SegEs, 16, context.SegFs); // WORD
    printf("        SegGs:                0x%0*X       SegSs:  0x%0*X\n", 16, context.SegGs, 16, context.SegSs); // WORD
    printf("        EFlags:               0x%0*lX \n", 16, context.EFlags); // DWORD
    printf("\n    Debug registers:\n");
    printf("        Dr0:                  0x%0*llX       Dr1:    0x%0*llX  \n", 16, context.Dr0, 16, context.Dr1); // DWORD64
    printf("        Dr2:                  0x%0*llX       Dr3:    0x%0*llX  \n", 16, context.Dr2, 16, context.Dr3); // DWORD64
    printf("        Dr6:                  0x%0*llX       Dr7:    0x%0*llX  \n", 16, context.Dr6, 16, context.Dr7); // DWORD64
    printf("\n    Integer registers:\n");
    printf("        Rax:                  0x%0*llX       Rcx:    0x%0*llX  \n", 16, context.Rax, 16, context.Rcx); // DWORD64
    printf("        Rdx:                  0x%0*llX       Rbx:    0x%0*llX  \n", 16, context.Rdx, 16, context.Rbx); // DWORD64
    printf("        Rsp:                  0x%0*llX       Rbp:    0x%0*llX  \n", 16, context.Rsp, 16, context.Rbp); // DWORD64
    printf("        Rsi:                  0x%0*llX       Rdi:    0x%0*llX  \n", 16, context.Rsi, 16, context.Rdi); // DWORD64
    printf("        R8:                   0x%0*llX       R9:     0x%0*llX  \n", 16, context.R8, 16, context.R9); // DWORD64
    printf("        R10:                  0x%0*llX       R11:    0x%0*llX  \n", 16, context.R10, 16, context.R11); // DWORD64
    printf("        R12:                  0x%0*llX       R13:    0x%0*llX  \n", 16, context.R12, 16, context.R13); // DWORD64
    printf("        R14:                  0x%0*llX       R15:    0x%0*llX  \n", 16, context.R14, 16, context.R15); // DWORD64
    printf("\n    Program counter:\n");
    printf("        Rip:                  0x%0*llX  \n", 16, context.Rip); // DWORD64
    printf("\n    Special debug control registers:\n");
    printf("        DebugControl:         0x%0*llX  \n", 16, context.DebugControl); // DWORD64
    printf("        LastBranchToRip:      0x%0*llX  \n", 16, context.LastBranchToRip); // DWORD64
    printf("        LastBranchFromRip:    0x%0*llX  \n", 16, context.LastBranchFromRip); // DWORD64
    printf("        LastExceptionToRip:   0x%0*llX  \n", 16, context.LastExceptionToRip); // DWORD64
    printf("        LastExceptionFromRip: 0x%0*llX  \n", 16, context.LastExceptionFromRip); // DWORD64
}

TEST(signal, thread_jump) {
    printf("C_RUNTIME: %s\n", C_RUNTIME);
    printf("VSC_RUNTIME: %s\n", VSC_RUNTIME);

    Mhook_SetHook((PVOID*)&FP_R_BeginThread, (PVOID)FP_H_BeginThread);
    Mhook_SetHook((PVOID*)&FP_R_EndThread, (PVOID)FP_H_EndThread);
    Mhook_SetHook((PVOID*)&FP_R_BeginThreadEx, (PVOID)FP_H_BeginThreadEx);
    Mhook_SetHook((PVOID*)&FP_R_EndThreadEx, (PVOID)FP_H_EndThreadEx);
    Mhook_SetHook((PVOID*)&FP_R_CreateThread, (PVOID)FP_H_CreateThread);
    Mhook_SetHook((PVOID*)&FP_R_ExitThread, (PVOID)FP_H_ExitThread);
    Mhook_SetHook((PVOID*)&FP_R_TerminateThread, (PVOID)FP_H_TerminateThread);
    Mhook_SetHook((PVOID*)&FP_R_GetExitCodeThread, (PVOID)FP_H_GetExitCodeThread);
    Mhook_SetHook((PVOID*)&FP_R_GetExitCodeThread, (PVOID)FP_H_GetExitCodeThread);
    for (int i = 0; i < 3; i++) {
        int data = 0;
        unsigned dwThreadId;

        printf("data: 0x%0*llX\n", 16, &data);
        printf("data = %p\n", &data);

        HANDLE hThread = (HANDLE) CreateThread(NULL, 0, (PTHREAD_START_ROUTINE)thread2, &data, CREATE_SUSPENDED, (LPDWORD) &dwThreadId);
        puts("suspended thread");
        CONTEXT init_context;
        init_context.ContextFlags = CONTEXT_FULL;
        if (!GetThreadContext(hThread, &init_context)) {
            puts("FAILED TO GET CURRENT INIT CONTEXT");
        }
        print_context(init_context);
        printf("sp: 0x%0*llX\n", 16, ((void**)init_context.Rsp));
        printf("sp[0]: 0x%0*llX\n", 16, ((void**)init_context.Rsp)[0]);
        printf("sp[1]: 0x%0*llX\n", 16, ((void**)init_context.Rsp)[1]);
        printf("sp[2]: 0x%0*llX\n", 16, ((void**)init_context.Rsp)[2]);
        printf("sp[3]: 0x%0*llX\n", 16, ((void**)init_context.Rsp)[3]);
        puts("resuming thread");
        ResumeThread(hThread);
        puts("resumed thread");
        Sleep(2000);
        puts("attempting to restart thread");
        CONTEXT context;
        context.ContextFlags = CONTEXT_FULL;
        printf("handled: %d\n", handled);
        printf("FPU_STATE_USE_Z_SAVE: %d\n", FPU_STATE_USE_Z_SAVE);
        print_reg(&current_reg);
        puts("suspending thread");
        SuspendThread(hThread);
        puts("suspended thread");
        data = 1;
        if (!GetThreadContext(hThread, &context)) {
            puts("FAILED TO GET CURRENT INIT CONTEXT");
        }
        puts("set Rip from thread2 to thread2_ret");
        current_thread = hThread;
        jump_address = context.Rip;
        context.Rip = (DWORD64) thread2_ret;
        print_context(context);
        printf("sp: 0x%0*llX\n", 16, ((void**)init_context.Rsp));
        printf("sp[0]: 0x%0*llX\n", 16, ((void**)init_context.Rsp)[0]);
        printf("sp[1]: 0x%0*llX\n", 16, ((void**)init_context.Rsp)[1]);
        printf("sp[2]: 0x%0*llX\n", 16, ((void**)init_context.Rsp)[2]);
        printf("sp[3]: 0x%0*llX\n", 16, ((void**)init_context.Rsp)[3]);
        if (!SetThreadContext(hThread, &context)) {
            puts("FAILED TO SET CURRENT CONTEXT");
            puts("resuming thread");
            ResumeThread(hThread);
            puts("resumed thread");
            return;
        }
        puts("SET CURRENT CONTEXT");
        puts("resuming thread");
        ResumeThread(hThread);
        puts("resumed thread");
        Sleep(2000);
        puts("done");
        TerminateThread(hThread, 0);
        printf("handled: %d\n", handled);
        printf("FPU_STATE_USE_Z_SAVE: %d\n", FPU_STATE_USE_Z_SAVE);
        print_reg(&current_reg);
    }

    std::thread foo = std::thread([]() {
        puts("THREAD 1 !");
    });
    foo.join();
    std::thread foo2 = std::thread([]() {
        puts("THREAD 2 !");
    });
    foo2.join();

    Mhook_Unhook((PVOID*)&FP_R_BeginThread);
    Mhook_Unhook((PVOID*)&FP_R_EndThread);
    Mhook_Unhook((PVOID*)&FP_R_BeginThreadEx);
    Mhook_Unhook((PVOID*)&FP_R_EndThreadEx);
    Mhook_Unhook((PVOID*)&FP_R_CreateThread);
    Mhook_Unhook((PVOID*)&FP_R_ExitThread);
    Mhook_Unhook((PVOID*)&FP_R_TerminateThread);
    Mhook_Unhook((PVOID*)&FP_R_GetExitCodeThread);
    Mhook_Unhook((PVOID*)&FP_R_GetExitCodeThread);

    std::thread foo3 = std::thread([]() {
        puts("THREAD 3 !");
    });
    foo3.join();
    std::thread foo4 = std::thread([]() {
        puts("THREAD 4 !");
    });
    foo4.join();
}








bool wl_thread_manager_debug = false;

#include <stdio.h>

// INTEGER FILE DESCRIPTOR ALLOCATOR

#include <cstdbool> // bool
#include <cstdio>   // printf
#include <cstring>  // malloc
#include <vector>   // vector

typedef void (*CHUNK_VECTOR_CALLBACK__DESTROY_DATA)(size_t index, void ** data, bool in_destructor);

static void CHUNK_VECTOR_CALLBACK____DO_NOTHING(size_t index, void** unused, bool in_destructor) {}

class ChunkVector {
  public:
    class Holder {
        public:

        bool used;
        void * data;
        size_t index;
        CHUNK_VECTOR_CALLBACK__DESTROY_DATA callback;

        Holder(void* data, size_t index, CHUNK_VECTOR_CALLBACK__DESTROY_DATA callback)
        : used(false), data(data), index(index), callback(callback == nullptr ? CHUNK_VECTOR_CALLBACK____DO_NOTHING : callback)
        {}

        Holder(void) : Holder(nullptr, 0, CHUNK_VECTOR_CALLBACK____DO_NOTHING)
        {}

        void destroy(bool invoke_destroy_callback) {
            if (callback != NULL) {
                if (invoke_destroy_callback) {
                    callback(index, &data, false);
                }
                callback = NULL;
            }
        }

        ~Holder(void) {
            if (callback != NULL) {
                callback(index, &data, true);
                callback = NULL;
            }
        }
    };

    class Chunk {
        public:
        
        Holder* data;
        size_t size;
        size_t capacity;

        Chunk(Holder* data, size_t size, size_t capacity)
         :
            data(data),
            size(size),
            capacity(capacity)
        {};

         Chunk(Chunk && old) {
            data = old.data;
            size = old.size;
            capacity = old.capacity;
            old.data = nullptr;
            old.size = 0;
            old.capacity = 0;
         }

         Chunk & operator=(Chunk && old) {
            data = old.data;
            size = old.size;
            capacity = old.capacity;
            old.data = nullptr;
            old.size = 0;
            old.capacity = 0;
            return *this;
         }

         ~Chunk(void) {
            delete[] data;
         }
    };

    std::vector<Chunk> chunks;
    size_t current_chunk_index;
    size_t total_size;
    size_t total_capacity;

    int get_chunk(size_t i) {
        return (8*sizeof(size_t) - __builtin_clzll(i + 2) - 1) - 1;
    }

    size_t get_chunk_subindex(size_t i, int chunk) {
        return i - (1 << (chunk + 1)) + 2;
    }

    void print_chunk_and_index(size_t i) {
        int chunk = get_chunk(i);
        printf("index: %zu, chunk: %d, sub array index: %zu\n", i, chunk, get_chunk_subindex(i, chunk));
    }
    
    public:
    CRITICAL_SECTION criticalSection;
    ChunkVector(void) {
        chunks = std::vector<Chunk>();
        current_chunk_index = 0;
        total_size = 0;
        total_capacity = 0;
        InitializeCriticalSectionAndSpinCount(&criticalSection, 4000);
    }
    ~ChunkVector() {
        DeleteCriticalSection(&criticalSection);
    }
    size_t size(void) { return total_size; }
    size_t capacity(void) { return total_capacity; }

    Holder* holderAt(size_t index) {
        if (total_capacity == 0) {
            if (wl_thread_manager_debug) printf("AT index invalid: %zu (total capazity is zero), false\n", index);
            return NULL;
        }
        int CI = get_chunk(index);
        size_t s = chunks.size();
        if (CI >= s) {
            if (wl_thread_manager_debug) printf("AT index invalid: %zu (CI %i >= %zu), false\n", index, CI, s);
            return NULL;
        }
        size_t DI = get_chunk_subindex(index, CI);
        if (DI >= chunks[CI].capacity) {
            if (wl_thread_manager_debug) printf("AT index invalid: %zu (DI %zu >= %zu), false\n", index, DI, chunks[CI].capacity);
            return NULL;
        }
        if (wl_thread_manager_debug) printf("AT index valid: %zu, true\n", index);
        return &chunks[CI].data[DI];
    }

    void* dataAt(size_t value) {
        Holder* h = holderAt(value);
        if (h == NULL) return NULL;
        return h->data;
    }

    void* operator[] (size_t value) {
        return dataAt(value);
    }

    class Iterator {
        int s;
        int s_m;
        size_t cc;
        size_t cc_m;
        std::vector<Chunk>* chunks;
        bool init;
        public:
        Iterator(std::vector<Chunk>* c) {
            chunks = c;
            s = 0;
            s_m = chunks->size();
            cc = 0;
            if (s_m != 0) {
                cc_m = chunks[0][0].capacity;
            }
        }
        bool has_next() {
            if (s < s_m) {
                if (cc < cc_m) {
                    return true;
                }
            }
            return false;
        }

        Holder* next() {
            Holder * h = NULL;
            if (s < s_m) {
                if (cc < cc_m) {
                    h = &chunks[0][s].data[cc];
                    cc++;
                    if (cc < cc_m) {
                        cc = 0;
                        cc_m = chunks[0][s].capacity;
                        s++;
                    }
                }
            }
            return h;
        }
    };

    Iterator iterator() { return Iterator(&chunks); }
    
    void print(void) {
        if (total_capacity == 0) {
            printf("array = nullptr\n");
        } else {
            printf("array (size %zu, capacity %zu, chunk size: %d, chunk capacity: %d)\n", total_size, total_capacity, (int)chunks.size(), (int)chunks.capacity());
            for(int i = 0; i < chunks.size(); i++) {
                printf("  chunk %d (size %zu, capacity %zu) = [", i, chunks[i].size, chunks[i].capacity);
                for(size_t i_ = 0; i_ < chunks[i].capacity; i_++) {
                    if (i_ != 0) printf(", ");
                    printf("%zu", i_);
                }
                printf("]\n");
            }
        }
    }

    size_t add(void* value, CHUNK_VECTOR_CALLBACK__DESTROY_DATA callback) {
        if (total_capacity == 0) {
            chunks.reserve(1);
            chunks.emplace_back(Chunk(new Holder[2], 1, 2));
            chunks[0].data[0].used = true;
            chunks[0].data[0].data = value;
            chunks[0].data[0].index = total_size;
            chunks[0].data[0].callback = callback;
            total_size ++;
            total_capacity += 2;
            current_chunk_index = 0;
            if (wl_thread_manager_debug) printf("added, total_size: %zu, total_capacity: %zu\n", total_size, total_capacity);
            return total_size-1;
        } else {
            if (chunks[current_chunk_index].size == chunks[current_chunk_index].capacity) {
                size_t cap = chunks[current_chunk_index].capacity*2;
                chunks.reserve(current_chunk_index+2);
                chunks.emplace_back(Chunk(new Holder[cap], 0, cap));
                current_chunk_index++;
                chunks[current_chunk_index].data[0].used = true;
                chunks[current_chunk_index].data[0].data = value;
                chunks[current_chunk_index].data[0].index = total_size;
                chunks[current_chunk_index].data[0].callback = callback;
                chunks[current_chunk_index].size = 1;
                total_size ++;
                total_capacity += chunks[current_chunk_index].capacity;
                if (wl_thread_manager_debug) printf("added, total_size: %zu, total_capacity: %zu\n", total_size, total_capacity);
                return total_size-1;
            } else {
                chunks[current_chunk_index].data[chunks[current_chunk_index].size].used = true;
                chunks[current_chunk_index].data[chunks[current_chunk_index].size].data = value;
                chunks[current_chunk_index].data[chunks[current_chunk_index].size].index = total_size;
                chunks[current_chunk_index].data[chunks[current_chunk_index].size].callback = callback;
                chunks[current_chunk_index].size++;
                total_size ++;
                if (wl_thread_manager_debug) printf("added, total_size: %zu, total_capacity: %zu\n", total_size, total_capacity);
                return total_size-1;
            }
        }
        return -1;
    }

    size_t reuse(size_t index, void * data, CHUNK_VECTOR_CALLBACK__DESTROY_DATA callback) {
        if (total_capacity == 0) {
            return -1;
        }
        int CI = get_chunk(index);
        size_t DI = get_chunk_subindex(index, CI);
        chunks[CI].data[DI].used = true;
        chunks[CI].data[DI].data = data;
        chunks[CI].data[DI].callback = callback;
        chunks[CI].size++;
        total_size++;
        if (wl_thread_manager_debug) printf("reuse, total_size: %zu, total_capacity: %zu\n", total_size, total_capacity);
        return index;
    }

    Holder* index_if_valid(size_t index, int * CI, size_t * DI) {
        if (total_capacity == 0) {
            if (wl_thread_manager_debug) printf("index invalid: %zu (total capazity is zero), false\n", index);
            return NULL;
        }
        *CI = get_chunk(index);
        size_t s = chunks.size();
        if (*CI >= s) {
            if (wl_thread_manager_debug) printf("index invalid: %zu (CI %i >= %zu), false\n", index, *CI, s);
            return NULL;
        }
        *DI = get_chunk_subindex(index, *CI);
        if (*DI >= chunks[*CI].capacity) {
            if (wl_thread_manager_debug) printf("index invalid: %zu (DI %zu >= %zu), false\n", index, *DI, chunks[*CI].capacity);
            return NULL;
        }
        if (chunks[*CI].data[*DI].used) {
            if (wl_thread_manager_debug) printf("index valid: %zu, true\n", index);
            return &chunks[*CI].data[*DI];
        }
        if (wl_thread_manager_debug) printf("index invalid: %zu, false\n", index);
        return NULL;
    }

    bool remove(size_t index, bool invoke_destroy_callback) {
        if (total_capacity == 0) {
            return false;
        }
        int CI;
        size_t DI;
        Holder * holder = index_if_valid(index, &CI, &DI);
        if (holder == NULL) {
            return false;
        }
        if (wl_thread_manager_debug) printf("removing %zu, CI: %d, DI: %zu, total_size: %zu, total_capacity: %zu, chunks size: %zu\n", index, CI, DI, total_size, total_capacity, chunks.size());
        chunks[CI].data[DI].destroy(invoke_destroy_callback);
        chunks[CI].data[DI].used = false;
        chunks[CI].size--;
        total_size--;
        if (chunks[CI].size == 0) {
            total_capacity -= chunks[CI].capacity;
            if (total_capacity == 0) {
                chunks.clear();
                chunks.shrink_to_fit();
            } else {
                if (chunks[chunks.size()-1].size == 0) {
                    int CII = chunks.size()-1;
                    while(true) {
                        if (chunks[CII].size != 0) {
                            break;
                        }
                        current_chunk_index--;
                        chunks.erase(chunks.cbegin() + CII);
                        chunks.shrink_to_fit();
                        CII--;
                    }
                }
            }
        }
        return true;
    }
};

ChunkVector thread_list;

struct SIGNAL_INFO_STATE {
    const uint8_t GENERATED = 1;
    const uint8_t PENDING = 2;
    const uint8_t ACCEPTED = 3;
};

// Linux Kernel defines

// KERNEL

// use kernel definition of sigset_t since it is easier to understand

#define _NSIG		64

#ifdef __i386__
# define _NSIG_BPW	32
#else
# define _NSIG_BPW	64
#endif

#define _NSIG_WORDS	(_NSIG / _NSIG_BPW)

typedef LP64__ULONG old_sigset_t;		/* at least 32 bits */

typedef struct {
	LP64__ULONG sig[_NSIG_WORDS];
} sigset_t;

#define SIG__MIN_VALUE 1

#define SIGHUP		 1
#define SIGINT		 2
#define SIGQUIT		 3
#define SIGILL		 4
#define SIGTRAP		 5
#define SIGABRT		 6
#define SIGIOT		 6
#define SIGBUS		 7
#define SIGFPE		 8
#define SIGKILL		 9
#define SIGUSR1		10
#define SIGSEGV		11
#define SIGUSR2		12
#define SIGPIPE		13
#define SIGALRM		14
#define SIGTERM		15
#define SIGSTKFLT	16
#define SIGCHLD		17
#define SIGCONT		18
#define SIGSTOP		19
#define SIGTSTP		20
#define SIGTTIN		21
#define SIGTTOU		22
#define SIGURG		23
#define SIGXCPU		24
#define SIGXFSZ		25
#define SIGVTALRM	26
#define SIGPROF		27
#define SIGWINCH	28
#define SIGIO		29
#define SIGPOLL		SIGIO
/*
#define SIGLOST		29
*/
#define SIGPWR		30
#define SIGSYS		31
#define	SIGUNUSED	31

#define SIG__MAX_VALUE 31

// /* These should not be considered constants from userland.  */
// #define SIGRTMIN	32
// #define SIGRTMAX	_NSIG

#define SA_RESTORER	0x04000000

# define __force	__attribute__((force))

/*
 * SA_FLAGS values:
 *
 * SA_NOCLDSTOP flag to turn off SIGCHLD when children stop.
 * SA_NOCLDWAIT flag on SIGCHLD to inhibit zombies.
 * SA_SIGINFO delivers the signal with SIGINFO structs.
 * SA_ONSTACK indicates that a registered stack_t will be used.
 * SA_RESTART flag to get restarting signals (which were the default long ago)
 * SA_NODEFER prevents the current signal from being masked in the handler.
 * SA_RESETHAND clears the handler when the signal is delivered.
 * SA_UNSUPPORTED is a flag bit that will never be supported. Kernels from
 * before the introduction of SA_UNSUPPORTED did not clear unknown bits from
 * sa_flags when read using the oldact argument to sigaction and rt_sigaction,
 * so this bit allows flag bit support to be detected from userspace while
 * allowing an old kernel to be distinguished from a kernel that supports every
 * flag bit.
 * SA_EXPOSE_TAGBITS exposes an architecture-defined set of tag bits in
 * siginfo.si_addr.
 *
 * SA_ONESHOT and SA_NOMASK are the historical Linux names for the Single
 * Unix names RESETHAND and NODEFER respectively.
 */
#ifndef SA_NOCLDSTOP
#define SA_NOCLDSTOP	0x00000001
#endif
#ifndef SA_NOCLDWAIT
#define SA_NOCLDWAIT	0x00000002
#endif
#ifndef SA_SIGINFO
#define SA_SIGINFO	0x00000004
#endif
/* 0x00000008 used on alpha, mips, parisc */
/* 0x00000010 used on alpha, parisc */
/* 0x00000020 used on alpha, parisc, sparc */
/* 0x00000040 used on alpha, parisc */
/* 0x00000080 used on parisc */
/* 0x00000100 used on sparc */
/* 0x00000200 used on sparc */
#define SA_UNSUPPORTED	0x00000400
#define SA_EXPOSE_TAGBITS	0x00000800
/* 0x00010000 used on mips */
/* 0x00800000 used for internal SA_IMMUTABLE */
/* 0x01000000 used on x86 */
/* 0x02000000 used on x86 */
/*
 * New architectures should not define the obsolete
 *	SA_RESTORER	0x04000000
 */
#ifndef SA_ONSTACK
#define SA_ONSTACK	0x08000000
#endif
#ifndef SA_RESTART
#define SA_RESTART	0x10000000
#endif
#ifndef SA_NODEFER
#define SA_NODEFER	0x40000000
#endif
#ifndef SA_RESETHAND
#define SA_RESETHAND	0x80000000
#endif

#define SA_NOMASK	SA_NODEFER
#define SA_ONESHOT	SA_RESETHAND

#ifndef SIG_BLOCK
#define SIG_BLOCK          0	/* for blocking signals */
#endif
#ifndef SIG_UNBLOCK
#define SIG_UNBLOCK        1	/* for unblocking signals */
#endif
#ifndef SIG_SETMASK
#define SIG_SETMASK        2	/* for setting the signal mask */
#endif

/*
 * si_code values
 * Digital reserves positive values for kernel-generated signals.
 */
#define SI_USER		0		/* sent by kill, sigsend, raise */
#define SI_KERNEL	0x80		/* sent by the kernel from somewhere */
#define SI_QUEUE	-1		/* sent by sigqueue */
#define SI_TIMER	-2		/* sent by timer expiration */
#define SI_MESGQ	-3		/* sent by real time mesq state change */
#define SI_ASYNCIO	-4		/* sent by AIO completion */
#define SI_SIGIO	-5		/* sent by queued SIGIO */
#define SI_TKILL	-6		/* sent by tkill system call */
#define SI_DETHREAD	-7		/* sent by execve() killing subsidiary threads */
#define SI_ASYNCNL	-60		/* sent by glibc async name lookup completion */

#ifndef __ASSEMBLY__
typedef void __signalfn_t(int);
typedef __signalfn_t *__sighandler_t;

typedef void __restorefn_t(void);
typedef __restorefn_t *__sigrestore_t;

#define SIG_DFL	((__force __sighandler_t)0)  /* default signal handling */
#define SIG_IGN	((__force __sighandler_t)1)  /* ignore signal */
#define SIG_ERR	((__force __sighandler_t)-1) /* error return from signal */
#endif

typedef union sigval {
	int sival_int;
	void *sival_ptr;
} sigval_t;

#define SI_MAX_SIZE	128

// https://elixir.bootlin.com/linux/latest/source/include/uapi/asm-generic/siginfo.h#L32 - union __sifields

#define __SIGINFO 			\
struct {				\
	int si_signo;			\
	int si_errno;			\
	int si_code;			\
    sigval_t si_sigval; \
	/*union __sifields _sifields;*/	\
}

typedef struct siginfo {
	union {
		__SIGINFO;
		int _si_pad[SI_MAX_SIZE/sizeof(int)];
	};
} siginfo_t;

#ifdef __i386__

struct sigaction {
	union {
	  __sighandler_t _sa_handler;
	  void (*_sa_sigaction)(int, struct siginfo *, void *);
	} _u;
	sigset_t sa_mask;
	LP64__ULONG sa_flags;
	void (*sa_restorer)(void);
};

#define sa_handler	_u._sa_handler
#define sa_sigaction	_u._sa_sigaction

#else /* __i386__ */

struct sigaction {
	__sighandler_t sa_handler;
	LP64__ULONG sa_flags;
	__sigrestore_t sa_restorer;
	sigset_t sa_mask;		/* mask last for extensibility */
};

#endif /* !__i386__ */


// POSIX - https://pubs.opengroup.org/onlinepubs/9699919799.2018edition/

// 2.4.1 Signal Generation and Delivery

// A signal is said to be "generated" for (or sent to) a process or thread when the event that causes the signal first occurs.
// Examples of such events include detection of hardware faults, timer expiration, signals generated via the sigevent structure
// and terminal activity, as well as invocations of the kill() and sigqueue() functions. In some circumstances, the same event
// generates signals for multiple processes.

// IMPLEMENTATION TRANSLATION:
// we will declare a signal as GENERATED upon the act of SENDING it to a process or thread
//
// declare signal
// send signal to thread X // SIGNAL IS IN GENERATED STATE
// send signal to process Z // SIGNAL IS IN GENERATED STATE
// send signal to thread X // SIGNAL EXISTS IN PENDING QUEUE, 1ST SIGNAL IS IN GENERATED STATE, 2ND SIGNAL IS IN PENDING STATE
//
// LINUX KERNEL:
//
// when a signal is generated, it is placed into a queue, ALWAYS
// 
// duplicate signals are not allowed
//


// At the time of generation, a determination shall be made whether the signal has been generated for the process or for a
// specific thread within the process. Signals which are generated by some action attributable to a particular thread, such as
// a hardware fault, shall be generated for the thread that caused the signal to be generated. Signals that are generated in
// association with a process ID or process group ID or an asynchronous event, such as terminal activity, shall be generated for
// the process.

// Each process has an action to be taken in response to each signal defined by the system (see Signal Actions).
// A signal is said to be "delivered" to a process when the appropriate action for the process and signal is taken.
// A signal is said to be "accepted" by a process when the signal is selected and returned by one of the sigwait() functions.

// During the time between the generation of a signal and its delivery or acceptance, the signal is said to be "pending".
// Ordinarily, this interval cannot be detected by an application.
//
// However, a signal can be "blocked" from delivery to a thread.
// If the action associated with a blocked signal is anything other than to ignore the signal, and if that signal is generated
// for the thread, the signal shall remain pending until it is unblocked, it is accepted when it is selected and returned by a call
// to the sigwait() function, or the action associated with it is set to ignore the signal.
//
// Signals generated for the process shall be delivered to exactly one of those threads within the process which is in a call to a
// sigwait() function selecting that signal or has not blocked delivery of the signal. If there are no threads in a call to a
// sigwait() function selecting that signal, and if all threads within the process block delivery of the signal, the signal shall
// remain pending on the process until a thread calls a sigwait() function selecting that signal, a thread unblocks delivery of the signal
// or the action associated with the signal is set to ignore the signal.
// If the action associated with a blocked signal is to ignore the signal and if that signal is generated for the process, it is unspecified
// whether the signal is discarded immediately upon generation or remains pending.

// IMPLEMENTATION TRANSLATION:
//
// all functions which wait for a signal AND return information about that signal will REMOVE
//   that signal (both BLOCKED and UNBLOCKED) from the set of pending signals
//      NO ACTION IS TAKEN, NO SIGNAL HANDLER IS CALLED
//
// if a signal is UNBLOCKED, action is taken upon it
//   it is unspecified what should happen when a signal is blocked, accepted, and then unblocked
//
// 
//
//
//
// WHEN WE ARE NOT INSIDE A SIGWAIT:
//   IF A SIGNAL IS PENDING, AND IT IS BLOCKED, THEN IT WILL REMAIN PENDING
//   IF A SIGNAL IS PENDING, AND IT IS UNBLOCKED, THEN IT WILL REMOVED AND ACTION TAKEN UPON THE REMOVED SIGNAL
//   IF A SIGNAL BECOMES PENDING, AND IT IS BLOCKED, THEN IT WILL REMAIN PENDING
//   IF A SIGNAL BECOMES PENDING, AND IT IS UNBLOCKED, THEN IT WILL REMOVED AND ACTION TAKEN UPON THE REMOVED SIGNAL
//
// WHEN WE ARE INSIDE A SIGWAIT:
//   IF A SIGNAL IS PENDING, AND IT IS BLOCKED, THEN IT WILL REMAIN PENDING
//   IF A SIGNAL IS PENDING, AND IT IS UNBLOCKED, THEN IT WILL REMOVED AND RETURNED, BUT NO ACTION WILL BE TAKEN UPON THE REMOVED SIGNAL
//   IF A SIGNAL BECOMES PENDING, AND IT IS BLOCKED, THEN IT WILL REMAIN PENDING
//   IF A SIGNAL BECOMES PENDING, AND IT IS UNBLOCKED, THEN IT WILL REMOVED AND RETURNED, BUT NO ACTION WILL BE TAKEN UPON THE REMOVED SIGNAL
//
//                                                                  | BLOCKED | UNBLOCKED
//    RETURNED BY SIGWAIT AND FRIENDS (BEFORE ENTERING, PENDING)    |   YES   |    NO
//    ACTION TAKEN                                                  |    NO   |   YES
//
//    RETURNED BY SIGWAIT AND FRIENDS (AFTER ENTERING )     |   YES   |   YES
//    ACTION TAKEN                                          |    NO   |   YES
//
//


// Each thread has a "signal mask" that defines the set of signals currently blocked from delivery to it. The signal mask for a thread
// shall be initialized from that of its parent or creating thread, or from the corresponding thread in the parent process if the thread
// was created as the result of a call to fork(). The pthread_sigmask(), sigaction(), sigprocmask(), and sigsuspend() functions control
// the manipulation of the signal mask.

// The determination of which action is taken in response to a signal is made at the time the signal is delivered, allowing for any changes
// since the time of generation. This determination is independent of the means by which the signal was originally generated.
// If a subsequent occurrence of a pending signal is generated, it is implementation-defined as to whether the signal is delivered or accepted
// more than once in circumstances other than those in which queuing is required. The order in which multiple, simultaneously pending signals
// outside the range SIGRTMIN to SIGRTMAX are delivered to or accepted by a process is unspecified.

// When any stop signal (SIGSTOP, SIGTSTP, SIGTTIN, SIGTTOU) is generated for a process or thread, all pending SIGCONT signals for that process
// or any of the threads within that process shall be discarded. Conversely, when SIGCONT is generated for a process or thread, all pending stop
// signals for that process or any of the threads within that process shall be discarded. When SIGCONT is generated for a process that is
// stopped, the process shall be continued, even if the SIGCONT signal is ignored by the process or is blocked by all threads within the process
// and there are no threads in a call to a sigwait() function selecting SIGCONT. If SIGCONT is blocked by all threads within the process, there
// are no threads in a call to a sigwait() function selecting SIGCONT, and SIGCONT is not ignored by the process, the SIGCONT signal shall remain
// pending on the process until it is either unblocked by a thread or a thread calls a sigwait() function selecting SIGCONT, or a stop signal is
// generated for the process or any of the threads within the process.

// An implementation shall document any condition not specified by this volume of POSIX.1-2017 under which the implementation generates signals.

// LIBC

// KERNEL

// static int Ksigsuspend(sigset_t *set)
// {
// 	current->saved_sigmask = current->blocked;
// 	set_current_blocked(set);

// 	while (!signal_pending(current)) {
// 		__set_current_state(TASK_INTERRUPTIBLE);
// 		schedule();
// 	}
// 	set_restore_sigmask();
// 	return -ERESTARTNOHAND;
// }

// /**
//  *  sys_rt_sigsuspend - replace the signal mask for a value with the
//  *	@unewset value until a signal is received
//  *  @unewset: new signal mask value
//  *  @sigsetsize: size of sigset_t type
//  */
// SYSCALL_DEFINE2(rt_sigsuspend, sigset_t __user *, unewset, size_t, sigsetsize)
// {
// 	sigset_t newset;

// 	/* XXX: Don't preclude handling different sized sigset_t's.  */
// 	if (sigsetsize != sizeof(sigset_t))
// 		return -EINVAL;

// 	if (copy_from_user(&newset, unewset, sizeof(newset)))
// 		return -EFAULT;
// 	return Ksigsuspend(&newset);
// }

// int sigsuspend(const sigset_t *mask) { return syscall_cp(SYS_rt_sigsuspend, mask, _NSIG/8);

class ThreadInfo {
    public:
    HANDLE thread;
    ChunkVector signal_queue;
    sigset_t thread_mask_queued;
    sigset_t thread_mask_blocked;
    sigaction action[SIG__MAX_VALUE]; // actions for each signal
    HANDLE has_pending_signals;
    size_t thread_list__index;
};

static void THREAD_INFO____DELETE(size_t index, void** data, bool in_destructor) {
    ThreadInfo* info = (ThreadInfo*) data;
    delete info;
}

// a process has no hard limit on how many threads it can create
// when a thread is requested to be sent a signal, it may or may not handle it

ThreadInfo* find_or_add_thread(HANDLE thread) {
    // search all threads for target thread
    auto iterator = thread_list.iterator();
    while (iterator.has_next()) {
        auto holder = iterator.next();
        if (holder != NULL) {
            ThreadInfo* info = (ThreadInfo*) holder->data;
            if (info->thread == thread) {
                return info;
            }
        }
    }
    ThreadInfo* info = new ThreadInfo();
    memset(info, 0, sizeof(ThreadInfo));
    info->thread = thread;
    info->has_pending_signals = CreateEventW(NULL, TRUE, TRUE, NULL);
    info->thread_list__index = thread_list.add(info, THREAD_INFO____DELETE);
    return info;
}

// sets errno to EINVAL if signum is an invalid signal

// IMPORTANT: this must be done before being passed to any functions except *sigfillset*
//
// initializes the signal set given by *set* to empty, with all signals excluded from the set
// returns 0 on success, -1 on error
static CLANG__ALWAYS_INLINE int sigemptyset(sigset_t *set)
{
	switch (_NSIG_WORDS) {
	default:
		memset(set, 0, sizeof(sigset_t));
		break;
	case 2: set->sig[1] = 0;
		CLANG__FALLTHROUGH;
	case 1:	set->sig[0] = 0;
		break;
	}
    return 0;
}

// IMPORTANT: this must be done before being passed to any functions except *sigemptyset*
//
// initializes the signal set given by *set* to full, including all signals
// returns 0 on success, -1 on error
static CLANG__ALWAYS_INLINE int sigfillset(sigset_t *set)
{
	switch (_NSIG_WORDS) {
	default:
		memset(set, -1, sizeof(sigset_t));
		break;
	case 2: set->sig[1] = -1;
		CLANG__FALLTHROUGH;
	case 1:	set->sig[0] = -1;
		break;
	}
    return 0;
}

// IMPORTANT: *set* must be passed to either *sigemptyset* or *sigfillset* before being passed to this function
//
// includes (adds) signal *signum* in the signal set given by *set*
// returns 0 on success, -1 on error
static CLANG__ALWAYS_INLINE int sigaddset(sigset_t *set, int _sig)
{
    if (_sig < SIG__MIN_VALUE || _sig > SIG__MAX_VALUE) {
        errno = EINVAL;
        return -1;
    }
	LP64__ULONG sig = _sig - 1;
	if (_NSIG_WORDS == 1)
		set->sig[0] |= LP64__1UL << sig;
	else
		set->sig[sig / _NSIG_BPW] |= LP64__1UL << (sig % _NSIG_BPW);
    return 0;
}

// IMPORTANT: *set* must be passed to either *sigemptyset* or *sigfillset* before being passed to this function
//
// excludes (removes) signal *signum* from the signal set given by *set*
// returns 0 on success, -1 on error
static CLANG__ALWAYS_INLINE int sigdelset(sigset_t *set, int _sig)
{
    if (_sig < SIG__MIN_VALUE || _sig > SIG__MAX_VALUE) {
        errno = EINVAL;
        return -1;
    }
	LP64__ULONG sig = _sig - 1;
	if (_NSIG_WORDS == 1)
		set->sig[0] &= ~(LP64__1UL << sig);
	else
		set->sig[sig / _NSIG_BPW] &= ~(LP64__1UL << (sig % _NSIG_BPW));
    return 0;
}

// IMPORTANT: *set* must be passed to either *sigemptyset* or *sigfillset* before being passed to this function
//
// tests if signal *signum* is included in the signal set given by *set*
// returns 1 if signal *signum* is included in the signal set given by *set*
// returns 0 if signal *signum* is excluded from the signal set given by *set*
// returns -1 on error
static CLANG__ALWAYS_INLINE int sigismember(sigset_t *set, int _sig)
{
    if (_sig < SIG__MIN_VALUE || _sig > SIG__MAX_VALUE) {
        errno = EINVAL;
        return -1;
    }
	LP64__ULONG sig = _sig - 1;
	if (_NSIG_WORDS == 1)
		return 1 & (set->sig[0] >> sig);
	else
		return 1 & (set->sig[sig / _NSIG_BPW] >> (sig % _NSIG_BPW));
}

// IMPORTANT: *set* must be passed to either *sigemptyset* or *sigfillset* before being passed to this function
//
// NON STANDARD
//
// returns 1 is *set* contains no signals, and 0 otherwise
static CLANG__ALWAYS_INLINE int sigisemptyset(sigset_t *set)
{
	switch (_NSIG_WORDS) {
	case 2:
		return (set->sig[1] | set->sig[0]) == 0;
	default:
		return set->sig[0] == 0;
	}
}

// IMPORTANT: *set1* and *set2* must be passed to either *sigemptyset* or *sigfillset* before being passed to this function
//
// NON STANDARD
//
// returns as-if == operator
static CLANG__ALWAYS_INLINE int sigequalsets(const sigset_t *set1, const sigset_t *set2)
{
	switch (_NSIG_WORDS) {
	case 2:
		return	(set1->sig[1] == set2->sig[1]) &&
			(set1->sig[0] == set2->sig[0]);
	default:
		return	set1->sig[0] == set2->sig[0];
	}
}

#define _SIG_SET_BINOP(name, op)					\
static CLANG__ALWAYS_INLINE int name(sigset_t *r, const sigset_t *a, const sigset_t *b) \
{									\
	if (_NSIG_WORDS == 2) {						\
		r->sig[1] = op(a->sig[1], b->sig[1]);					\
    } \
    r->sig[0] = op(a->sig[0], b->sig[0]);					\
	return 0;							\
}

#define _sig_or(x,y)	((x) | (y))

// IMPORTANT: *dest*, *left* and *right* must be passed to either *sigemptyset* or *sigfillset* before being passed to this function
//
// NON STANDARD
//
// places the union of the sets *left* and *right* in *dest*.
// returns 0 on success, -1 on error
_SIG_SET_BINOP(sigorset, _sig_or)

#define _sig_and(x,y)	((x) & (y))

// IMPORTANT: *dest*, *left* and *right* must be passed to either *sigemptyset* or *sigfillset* before being passed to this function
//
// NON STANDARD
//
// places the intersection of the sets *left* and *right* in *dest*.
// returns 0 on success, -1 on error
_SIG_SET_BINOP(sigandset, _sig_and)

#define _sig_andn(x,y)	((x) & ~(y))

// IMPORTANT: *dest*, *left* and *right* must be passed to either *sigemptyset* or *sigfillset* before being passed to this function
//
// NON STANDARD
//
// places the intersection of the sets *left* and inversed *right* in *dest*.
// returns 0 on success, -1 on error
_SIG_SET_BINOP(sigandnset, _sig_andn)

#undef _SIG_SET_BINOP
#undef _sig_or
#undef _sig_and
#undef _sig_andn

#define _SIG_SET_OP(name, op)						\
static CLANG__ALWAYS_INLINE int name(sigset_t *set)					\
{									\
	if (_NSIG_WORDS == 2) {						\
	    set->sig[1] = op(set->sig[1]);				\
    }						\
	set->sig[0] = op(set->sig[0]);				\
	return 0;							\
}

#define _sig_not(x)	(~(x))
_SIG_SET_OP(signotset, _sig_not)

#undef _SIG_SET_OP
#undef _sig_not

/* Some extensions for manipulating the low 32 signals in particular.  */

#define sigmask(sig)	(LP64__1UL << ((sig) - 1))

static CLANG__ALWAYS_INLINE void sigaddsetmask(sigset_t *set, LP64__ULONG mask)
{
	set->sig[0] |= mask;
}

static CLANG__ALWAYS_INLINE void sigdelsetmask(sigset_t *set, LP64__ULONG mask)
{
	set->sig[0] &= ~mask;
}

static CLANG__ALWAYS_INLINE int sigtestsetmask(sigset_t *set, LP64__ULONG mask)
{
	return (set->sig[0] & mask) != 0;
}

static CLANG__ALWAYS_INLINE void siginitset(sigset_t *set, LP64__ULONG mask)
{
	set->sig[0] = mask;
    if (_NSIG_WORDS == 2) set->sig[1] = 0;
}

static CLANG__ALWAYS_INLINE void siginitsetinv(sigset_t *set, LP64__ULONG mask)
{
	set->sig[0] = ~mask;
	switch (_NSIG_WORDS) {
	default:
		memset(&set->sig[1], -1, sizeof(LP64__LONG)*(_NSIG_WORDS-1));
		break;
	case 2: set->sig[1] = -1;
		break;
	case 1: ;
	}
}

// info->action[signal].sa_handler = NULL;
// info->action[signal].sa_flags = 0;
// info->action[signal].sa_mask = signal;

// sends a signal to a specified thread
void thread_kill(HANDLE thread, int signal) {
    // a thread can only be monitored once
    // lock to prevent duplicate thread monitoring
    EnterCriticalSection(&thread_list.criticalSection);
    ThreadInfo * info = find_or_add_thread(thread);
    if (sigismember(&info->thread_mask_queued, signal)) {
        // we already have a pending signal
        LeaveCriticalSection(&thread_list.criticalSection);
        return;
    }
    // we have no pending signal
    sigaddset(&info->thread_mask_queued, signal); // so we can quickly check signals without iterating the queue
    info->signal_queue.add((void*)signal, NULL); // so we can obtain the signal via sigwait
    SetEvent(info->has_pending_signals);
    LeaveCriticalSection(&thread_list.criticalSection);
}


// a signal which is blocked cannot be waited for by sigwait and friends
int sigprocmask(int how, const sigset_t * set, sigset_t* oldset) {
    EnterCriticalSection(&thread_list.criticalSection);

    ThreadInfo * info = find_or_add_thread(GetCurrentThread());
    if (oldset != NULL) {
        *oldset = info->thread_mask_blocked;
    }

    sigset_t newset;
    if (set != NULL) {
        switch (how) {
            case SIG_BLOCK:
                sigorset(&newset, &info->thread_mask_blocked, set);
                break;
            case SIG_UNBLOCK:
                sigandnset(&newset, &info->thread_mask_blocked, set);
                break;
            case SIG_SETMASK:
                newset = *set;
                break;
            default:
                return -EINVAL;
        }
    }

    info->thread_mask_blocked = newset;

    LeaveCriticalSection(&thread_list.criticalSection);
    return 0;
}

// a thread can manually add a signal
// that signal is only removed when it is accepted by a thread
//
// signals are per-thread but NOT thread local

// if timeout is NULL then infinite timeout
int sigtimedwait(const sigset_t * set, struct siginfo* info, const timespec_N_bit * timeout) {
    // a thread can only be monitored once
    //
    // lock to prevent duplicate thread monitoring
    //
    
    EnterCriticalSection(&thread_list.criticalSection);

    ThreadInfo * info_ = find_or_add_thread(GetCurrentThread());

    sigset_t mask = *set;

    /*
	 * Invert the set of allowed signals to get those we want to block.
	 */
	sigdelsetmask(&mask, sigmask(SIGKILL) | sigmask(SIGSTOP)); // remove these from mask
    signotset(&mask); // inverts mask

    int sig;

    // wait for a signal to be sent

    // attempts to wait for a signal
    // if multiple threads are waiting for the same signal, only ONE of them of them will recieve a PROCESS-WIDE signal (if sent)
    // if one of the signals in *set* is already pending then we return immediately
    // we return the signal number

    // compute the timing now so we dont recompute every time we obtain a signal that does not match
    LP64__LONG period_milliseconds = 0;
    if (timeout != NULL) {
        LP64__LONG period_nanosec = timeout->tv_nsec;
        time_64_t period_sec = timeout->tv_sec;

        if (!(period_nanosec == 0 && period_sec == 0)) {

            // add sec to millisecond
            period_milliseconds = period_sec * 1000; // sec -> sec,mill_mill_mill
            
            // truncate to millisecond precision for windows period
            // for each X we add 0 to add and divide
            // mill_mill_mill,micr_micr_micr,ns_ns_ns -> mill_mill_mill (truncate by 6, add 500000 then divide 1000000) // 1th nano -> 1th millisecond
            period_nanosec += 500000;
            period_nanosec /= 1000000;
            period_milliseconds += period_nanosec;
        }
    }

    // we jump to here every time we obtain a signal
    CHECK_QUEUE:
    sigset_t r;
    sigandset(&r, &mask, &info_->thread_mask_queued);
    if (sigisemptyset(&r) == 0) {
        // mask contains a signal in set, iterate signal queue to find the signal, since mask contains a signal we know the queue must not be empty
        auto iterator = info_->signal_queue.iterator();
        while(iterator.has_next()) {
            auto holder = iterator.next();
            if (sigismember(&r, (int)holder->data)) {

                // we have a signal

                // save signal
                sig = (int)holder->data;

                // remove signal from queue
                info_->signal_queue.remove(holder->index, false);

                // also remove from masked queue
                sigdelset(&info_->thread_mask_queued, sig);

                // we have removed the signal from the queue
                if (sigisemptyset(&info_->thread_mask_queued) == 1) {

                    // thread queue is empty, remove the thread from the thread list
                    // do not invoke destroy callback since we still hold a lock
                    thread_list.remove(info_->thread_list__index, false);
                }

                LeaveCriticalSection(&thread_list.criticalSection);

                delete info_;
                
                memset(info, 0, sizeof(struct siginfo));
                info->si_signo = sig;
                info->si_code = SI_USER;
                return sig;
            }
            // signal in queue did not match, find next signal and check if it matches
        }
    }
    // mask contains no signals in set, wait for a signal to be queued

    // dont block other threads that want to queue other signals
    //
    // it is safe to unlock here because we are the manipulating per-thread signals
    // other threads can send us more signals but only WE can remove signals
    LeaveCriticalSection(&thread_list.criticalSection);

    // wait for a thread to send us a signal if timeout is not zero, zero
    // NULL specifies INFINITE timeout

    if (timeout != NULL && period_milliseconds == 0) {
        // we have already polled above, and we had no signals which matched set, return
        goto FAIL;
    }

    // pass FALSE to signal we should not be woken by alertable actions
    DWORD ret = WaitForSingleObjectEx(info_->has_pending_signals, timeout == NULL ? INFINITE : (period_milliseconds > INFINITE ? INFINITE : period_milliseconds), FALSE);
    // we either obtained a signal (in which, lock and check), or we timed out (failed to obtain a signal in time)
    if (ret == WAIT_TIMEOUT) {
        goto FAIL;
    }
    EnterCriticalSection(&thread_list.criticalSection);
    goto CHECK_QUEUE;
FAIL:
    errno = EAGAIN;
    return -1;
}

int sigwaitinfo(const struct siginfo* set, struct siginfo* info) {
    return sigtimedwait(set, info, NULL);
}

// suspends execution until any signal is caught
int pause() {
    ThreadInfo * info = beginSignalWait();

    doSignalWait(info);

    // we have obtained a signal


    endSignalWait(info);
}

// suspends execution until a specified signal is caught
void sigsuspend(int signal) {
    EnterCriticalSection(&thread_list.criticalSection);
    ThreadInfo * info = find_or_add_thread(GetCurrentThread());
    auto old = info->sigmask;
    info->sigmask = signal;
    LeaveCriticalSection(&thread_list.criticalSection);

    WaitForSingleObjectEx(info->has_pending_signals, INFINITE, TRUE);

    info->sigmask = old;
    EnterCriticalSection(&thread_list.criticalSection);
    thread_list.remove(info->index);
    LeaveCriticalSection(&thread_list.criticalSection);
}



// we can install a signal handler via syscalls
static DWORD thread3(DWORD data)
{
    puts("thread 2 enter");
    int * d = (int*)data;
    if (*d == 0) {
        while(1) {
            printf("0");
            Sleep(100);
        }
    } else {
        while(1) {
            printf("1");
            Sleep(100);
        }
    }
}
/*
TEST(signal, thread_jump2) {
    for (int i = 0; i < 3; i++) {
        int data = 0;
        unsigned dwThreadId;

        HANDLE hThread = (HANDLE) _beginthreadex(NULL, 0, thread2, &data, 0, &dwThreadId);
        Sleep(2000);
        puts("emulating signal");
        data = 1;
        CONTEXT context;
        context.ContextFlags = CONTEXT_FULL;

        puts("suspending thread");
        SuspendThread(hThread);
        puts("suspended thread");
        if (!GetThreadContext(hThread, &context)) {
            puts("FAILED TO GET CURRENT CONTEXT");
            puts("resuming thread");
            ResumeThread(hThread);
            puts("resumed thread");
            return;
        }
        puts("GET CURRENT CONTEXT");
        puts("set pc to thread2");
        context.Rip = (DWORD64) thread2;
        if (!SetThreadContext(hThread, &context)) {
            puts("FAILED TO SET CURRENT CONTEXT");
            puts("resuming thread");
            ResumeThread(hThread);
            puts("resumed thread");
            return;
        }
        puts("SET CURRENT CONTEXT");
        puts("resuming thread");
        ResumeThread(hThread);
        puts("resumed thread");
        Sleep(2000);
        puts("done");
        TerminateThread(hThread, 0);
    }
}
*/
