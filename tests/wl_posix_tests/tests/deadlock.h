#include <gtest/gtest.h>
#include <wl_posix.h>
class TerminateOnDeadlockGuard final
{
public:
    using Clock = std::chrono::system_clock;
    using Duration = Clock::duration;

    explicit TerminateOnDeadlockGuard(Duration timeout, int exit_code);

    ~TerminateOnDeadlockGuard();

private:
    void waitForCompletion();

private:
    const int exit_code;
    const Duration m_timeout;
    std::mutex m_mutex;
    std::condition_variable m_wasCompleted;
    std::condition_variable m_guardStarted;
    std::thread m_guardThread;
};
//---------
TerminateOnDeadlockGuard::TerminateOnDeadlockGuard(Duration timeout, int exit_code) : exit_code(exit_code), m_timeout{timeout}
{
    m_guardThread = std::thread(&TerminateOnDeadlockGuard::waitForCompletion, this);
    std::unique_lock<std::mutex> lock{m_mutex};
    m_guardStarted.wait(lock);
}

TerminateOnDeadlockGuard::~TerminateOnDeadlockGuard()
{
    m_wasCompleted.notify_one();
    m_guardThread.join();
}

void TerminateOnDeadlockGuard::waitForCompletion()
{
    std::unique_lock<std::mutex> lock{m_mutex};
    m_guardStarted.notify_one();
    if (std::cv_status::timeout == m_wasCompleted.wait_for(lock, m_timeout))
    {
        std::exit(exit_code); // you can use `abort` here to create crash log.
    }
    std::exit(exit_code); // you can use `abort` here to create crash log.
}
